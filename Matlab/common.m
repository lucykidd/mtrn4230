% Contains functions that could be called by more than one module.


% Chocolates is an array 5*N, [x,y,theta,flavour,top,reachable,pickable]
function possible = isOrderFeasible(Order,Chocolates)
% order is a vector of length 4, containing values 0 to 6. Order is
% [nbrMilk, nbrDark, nbrOrange, nbrMint]

    milk = 1;
    dark = 2;
    orange = 3;
    mint = 4;
 
    % keeping only the reachable ones. If it's not pickable, it'll probably
    % become pickable later on
    II = find(Chocolates(:,6) == 1);
    Chocolates = Chocolates(II,:);

    % looking for milk
    II = find(Chocolates(:,4) == 1);
    if length(II) < Order(1)
        % Sorry bud, can't do that for you
        possible = 0;
        return;
    end

    % dark...
    II = find(Chocolates(:,4) == 2);
    if length(II) < Order(2)
        % Sorry bud, can't do that for you
        possible = 0;
        return;
    end

    % orange
    II = find(Chocolates(:,4) == 3);
    if length(II) < Order(3)
        % Sorry bud, can't do that for you
        possible = 0;
        return;
    end

    % mint
    II = find(Chocolates(:,4) == 4);
    if length(II) < Order(4)
        % Sorry bud, can't do that for you
        possible = 0;
        return;
    end

    % if we didn't quit by here, it's possible
    possible = 1;

return

function BPos = getBoxPositions(x,y,theta)
% x,y centre of the box
% theta angle of the main axis
% BPos is 4*3, with BPos(1,:) = [x,y,theta] of the position B1

% the box is 214*119mm on inside, so we give split that equally in 4.
% 214/4 = 53.5mm per chocolate, a chocolate is 53mm.
    SideCenter = [x-107*cos(theta),y-107*sin(theta)];

    SideCenter 
    for i=1:4
        % center left + half a chocolate for the first one, center left + a
        % chocolate + half a chocolate for the next one etc.
        BPos(i,:) = [SideCenter(1)+26.75*cos(theta)+53.5*(i-1)*cos(theta),SideCenter(1)+26.75*sin(theta)+53.5*(i-1)*sin(theta),theta+pi/2];
    end
return

function Pos = conCam2Robot(PosCon)
    % same as tableCam2Robot
    % PosCon = [x,y]
    % Pos = [x,y]
    % we know that xRob = -xcoeff*Xcam + xOffset
    % and yRob = yCoeff*Ycam + yOffset
    
    yCoeff = 0.7075;
    xCoeff = 0.7245;
    xOffset = 572.72;
    yOffset = 108;

    Pos(1) = -xCoeff*PosCon(1)+xOffset;
    Pos(2) = yCoeff*PosCon(2)+yOffset;
return


function Pos = tableCam2Robot(PosTable)
    % obvious function, returns coordinates of PosTable in robot frame
    % PosTable = [x,y]
    % Pos = [x,y]
    pix2mm =  0.6477;
    xOffset =  -800;
    yOffset = 140;
    % offset
    PosTable(1) = PosTable(1) + xOffset;
    PosTable(2) = PosTable(2) + yOffset;

    % to mm
    Pos = pix2mm*PosTable;

    % rotation
    temp = Pos(1); 
    Pos(1) = Pos(2);
    Pos(2) = temp;
return

% not even sorry for the long name ha has
function putChocolateFromTableInSlot(x,y,z,theta,flavour)
% x,y in robot frame, theta given with relation to positive X of robot
    stacks = [0,0,0,0]; % this should be persistent, but initialised to 0 and maybe user settable in case of weird stuff hapenning?, size of stack [milk,dark,orange,mint]
    
    chocX = 200;
    
    milkY = 100;
    darkY = 200;
    orangeY = 300;
    mintY = 400;
    
    tableZ = 152;
    
    chocThick = 7.5; % in mm, thickness
    
    switch flavour
        case 1 % we got milk
            this.movecommsHandle.moveChocolate([x,y,z],[chocX,milkY,tableZ+chockThick*stacks(1)],theta);
            stacks(1) = stacks(1) + 1;
            this.log('moving a milk chocolate in',x,' ', y,' ',theta,'stack size = ', stacks(1)); 
        case 2
            this.movecommsHandle.moveChocolate([x,y,z],[chocX,darkY,tableZ+chockThick*stacks(2)],theta);
            stacks(2) = stacks(2) + 1;
            this.log('moving a dark chocolate in',x,' ', y,' ',theta,'stack size = ', stacks(2)); 
        case 3
            this.movecommsHandle.moveChocolate([x,y,z],[chocX,orangeY,tableZ+chockThick*stacks(3)],theta);
            stacks(3) = stacks(3) + 1;
            this.log('moving an orange chocolate in',x,' ', y,' ',theta,'stack size = ', stacks(3)); 
        case 4
            this.movecommsHandle.moveChocolate([x,y,z],[chocX,mintY,tableZ+chockThick*stacks(4)],theta);
            stacks(4) = stacks(4) + 1;
            this.log('moving a mint chocolate in',x,' ', y,' ',theta,'stack size = ', stacks(4)); 
        otherwise
            this.log('dafuq do you want me to do with flavour ', flavour, ' ?');
           
    end     
return


function unloadChocolatesFromBox(x,y,theta)
% x,y = center of box
% theta = orientation of box

B = getBoxPositions(x,y,theta);


% now we should find a way to empty the box...
% suggestion: assume the box is full, empty each possible spot?
% not sure if we can assume the table is empty, should probably make a
% massive array of places to put the chocolates and tick out the ones that
% are in use


return


