% Path planner for MTRN4230
function main()
    Chocolates = [];
    for i=1:25
        Chocolates(1,i) = 25-i;
        Chocolates(2,i) = 25-i;
        Chocolates(3,i) = mod(i,5);
    end
    
    refX = 0;
    refY = 0;
    
    Chocolates
    Distances = zeros(1,length(Chocolates(1,:)));
    for i=1:length(Distances)
        Distances(i) = (Chocolates(1,i)- refX)^2 + (Chocolates(2,i) - refY)^2;
    end
    
    
    [a,b,c,d,e,f] = sortChocolates(Chocolates,0,0)
    

    


return


% Chocolates is an array 5*N, [x,y,theta,flavour,top, reachable,pickable]
function [milk,dark,mint,orange,bottom,all] = sortChocolates(Chocolates,refX,refY)

    milkFlavour = 1;
    mintFlavour = 4;
    darkFlavour = 2;
    orangeFlavour = 3;
    bottomFlavour = 0; % not a flavour, but consistency...
    
    % building a vector with all the distances
    Distances = zeros(1,length(Chocolates(1,:)));
    for i=1:length(Distances)
        Distances(i) = (Chocolates(1,i)- refX)^2 + (Chocolates(2,i) - refY)^2;
    end
    
    % sort the vector with distances
    [Distances, IX] = sort(Distances);
    
    % sort all chocolates by distance
    SortedChocolates = Chocolates(:,IX);
    all = SortedChocolates;
    
    milkII = find(SortedChocolates(4,:) == milkFlavour);
    milk = SortedChocolates(:,milkII);
    
    mintII = find(SortedChocolates(4,:) == mintFlavour);
    mint = SortedChocolates(:,mintII);
    
    darkII = find(SortedChocolates(4,:) == darkFlavour);
    dark = SortedChocolates(:,darkII);
    
    orangeII = find(SortedChocolates(4,:) == orangeFlavour);
    orange = SortedChocolates(:,orangeII);
    
    bottomII = find(SortedChocolates(4,:) == bottomFlavour);
    bottom = SortedChocolates(:,bottomII);
    
return