function main()
 %   clear all; 
 %   close all; 
%    clc; % Take care to clear variables or else elements of L may not be overwritten correctly - maybe a Matlab optimiser problem.
    tic;




    global irb_120;    
    k = pi/180;
    
    irb_120 = buildRobot();

            % on pickup
%    T = [-1,0,0,x;
 %        0,1,0,y;
  %      0,0,-1,z;
   %     0,0,0,1];

   x_flip = 0;
   y_flip = -0.200;
   z_flip = 0.190;
    % in flip pos
    T = [0,0,-1,x_flip;
        0,1,0,y_flip;
        1,0,0,z_flip;
        0,0,0,1];
    q0 = [-pi/2,pi/6,0,0,pi/2,0];
    
    q2 = k*[90,41,51,-7,-25,0];
    irb_120.fkine(q2);
    x_box = -0.050;
    y_box = 0.300;
    z_box = 0.022;
    theta_box = pi/4;
    T = [-1,0,0,0;
         0,1,0,0.3;
         0,0,-1,0.2;
        0,0,0,1]
    T = T*trotz(theta_box)
    T=T*trotz(theta_box)*troty(-pi/4) % should give what we want
    % then add a rotation of theta of -theta on joint 6...
    q = Quaternion(T)
   test = 12;
 %   Q1 = Q1*180/pi
pause();

end





% this is clearly a controller on 'roids.
function TAU = irb120_pd_controller(robot, T, Q, QD, varargin)
% Basic Proportional controller for an IRB-120, treating each axis independently.
% Mark Whitty
% 150511
% UNSW Mechatronics
% Q and QD are the manipulator joint coordinate and velocity state 
% respectively, and T is the current time. TAU is the output torque.
% Edit desired_q and desired_qd as a function of time for trajectory
% following.
error_q = varargin{1} - Q;
error_qd = varargin{2} - QD;
% Ku P = [70,30,30,30,30,3]; % Adjust the proportional constant independently 
% P = [500,200,200,100,100,100]; gives good speed...

P = [3500,2800,1400,550,300,80];
D = [480,380,190,75,40,10];


% Assume Ka (transconductance) is 1 for the amplifier and Kt (Km) is 0.2.
kt = 0.2;
TAU = kt*((P.*error_q) + (D.*error_qd));
TAU = TAU + robot.rne(varargin{1},varargin{2},[0,0,0,0,0,0]); % assumes 0 acceleration at the end

% We are ignoring any effects due to the use of a discrete time controller,
% coupling between axes, encoder measurements and much more. 
% This is a really basic controller!
end
