classdef Planner < PlannerInterface
    % Planner : This class is used to handle determining how to move the
    % robot from a current position to desired position, in addition to
    % figuring out other high level planning operations...
    % USED FOR FUTURE, not implemented in ass2
    % Author : Lucy & Francis
    % Date : 24/03/2015
    
    %% Properties
    properties (Access = public)
        % Everyone can see and modify these properties
        robController;
    end
    
    properties (Access = protected)
        % Only sublclasses can access these properties
    end
    
    properties (Access = private)
        % Only this class can access these properties
         tableHomePose;
         conveyorHomePose;
    end
    
    
    %% Methods
    methods (Access = public)
        % Everyone can see and use these methods for this class..
        
        function this = Planner()
            %constructor: constructs our planner object...
            % Usage : it should be called whenever the default constructor is
            % called
            
            % home poses 50 mm off table, in the middle of the table and conveyorbelt 
            this.tableHomePose = struct('position', [409  0 197], 'rotation',...
                [0 180 0]); 
            this.conveyorHomePose = struct('position', [0  409 197], 'rotation',...
                [0 180 90]);
          
        end
        
        function addHandles (this, robControllerHandle)
            %addHandles : adds the rob controller object handle. 
            this.robController = robControllerHandle;
        end
        
        function out = reachablePoint(this, point)
            % reachablePoint: checks if a given point is reachable. Returns 0 if 
            % it is not,1 if it is. 
            % input: point - a vector that stores (x, y, z)
            
            out = 0;
            if(sqrt(point(1)^2 + point(2)^2 + point(3)^2) <= 580)
                out = 1;
            end
        end
        
        function moveToPoint(this, goal)
            % moveToPoint: moves the robot from current position to the 
            % given goal. Goal is a vector x, y, z 
            
            if(overTheConveyor() == 1 && positionOverConveyor() == 1)
                movecomms.moveCartPos(movecomms, goal(1), goal(2), goal(3), 1);
                this.log('Successfully moved from over conveyor to goal over the conveyor\n');
            else if(overTheConveyor() == 1 && positionOverTable() == 1)
                    conveyorHomePosition();
                    tableHomePosition();
                    movecomms.moveCartPos(movecomms, goal(1), goal(2), goal(3), 1);
                    this.log('Successfully moved from over conveyor to goal over the table\n');
                else if (overTheTable() == 1 && positionOverConveyor(goal) == 1 ...
                            && overRHSOfTable() ==1)
                        tableHomePosition();
                        conveyorHomePosition();
                        movecomms.moveCartPos(goal(1), goal(2), goal(3), 1);
                        this.log('Successfully moved from over RHS of table to goal over the conveyor\n');
                    else if (overTheTable() == 1 && positionOverConveyor(goal) == 1 ...
                            && overRHSOfTable() == 0)
                            conveyorHomePosition();
                            movecomms.moveCartPos(goal(1), goal(2), goal(3), 1);
                            this.log('Successfully moved from over LHS of table to goal over the conveyor\n');
                        else if(overTheTable() == 1 && positionOverTable(goal) == 1)
                                movecomms.moveCartPos(goal(1), goal(2), goal(3), 1);
                                this.log('Successfully moved from over table to goal over the table\n'); 
                            else if(overTheTable() == 0 && overTheConveyor() == 0)
                                    this.log('Stop Mucking around\n'); %%!! do this later
                                end
                        
                            end
                        end
                    end
                end
            end
            
        end
        
        function moveTableToConveyor(this)
        % moveTableToConveyor: moves the robot from the table home position to
        % the conveyor home position
            if(overTheTable() == 1 && overRHSOfTable() == 1)
                tableHomePosition();
                %!! this could be done more efficiently
                conveyorHomePosition();
            else if (overTheTable() == 1 && overRHSOfTable() == 0)
                    conveyorHomePosition();
                else if (overTheTable() == 0)
                        this.log('moveTableToConveyor: End Effector is not over Table\n');
                    end
                end
            end
        end
        
        function moveConveyorToTable(this)
        % moveConveyorToTable: moves from conveyor home position to table home
        % position
            if (overTheConveyor() == 1)
                conveyorHomePosition();
                tableHomePosition();
            else if (overTheConveyor() ==0)
                    this.log('moveConveyorToTable: end effector is not over the conveyor\n');
                end
            end
        
        end
        
        function tableHomePosition(this)
        % tableHomePosition: moves robot from current pose to table home position 
            movecomms.moveCartPos(this.tableHomePose.position(1),...
                this.tableHomePose.position(2), this.tableHomePose.position(3),...
                1);
            this.log('successfully moved to table home position bitches\n');
        end
        
        function conveyorHomePosition(this)
        %conveyorHomePosition: moves robot from current pose to conveyor home
        %position
            movecomms.moveCartPos(this.conveyorHomePose.position(1),...
                this.conveyorHomePose.position(2), this.conveyorHomePose.position(3),...
                1);
            this.log('successfully moved to conveyor home position bitches\n');
        end
    
        function initialise(this)
        %initialise: move from the robots initial pose to our table home
        %position
            movecomms.moveJPos(0, 0, 0, 0, 90, 0);
            tableHomePosition();
            this.log('successfully initialised robot to home position');
        end
        
        function out = overTheTable(this)
        %overTheTable: checks if the robot end effector is over the table
            out = 0;
            prevPose = this.robController.buffer(size(this.robController.buffer));
            if(pose.position(2) > 0) 
                out = 1;
            end
        end
        
        function out = overTheConveyor(this) 
        %overTheConveyor: checks if the end effector is over the conveyor
            out = 0;
            pose = this.robController.getPose(this.robController);
            if(pose.position(1) < 129 && pose.position(2) > 129) 
                out = 1;
            end
        end
        
        function out = positionOverConveyor(position)
        % positionOverConveyor: checks if a given position vector is over
        % the conveyorbelt
            out = 0;
            if(position(1) < 129 && position(2) > 129) 
                out = 1;
            end
        end
        
        function out = positionOverTable(position)
        % positionOverTable: checks if a given position vector is over the 
        % table
            out = 0;
            if(position(2) > 0) 
                out = 1;
            end
        end
        
        function out = overRHSOfTable(this)
        % overRHSOfTable: checks if the end effector is in negative y (Robots RHS)
            out = 0;
            pose = this.robController.getPose(this.robController);
            if(pose.position(2) < 0)
                out = 1;
            end
        end

    end
    
    methods (Access = protected)
        % Only Subclasses can see and use these methods...
    end
    
    methods (Access = private)
        % Only this class can seee and use these methods...
    end
    
    
end

