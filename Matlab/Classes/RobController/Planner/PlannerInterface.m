classdef PlannerInterface < Base
    % PlannerInterface : The Planner class is used to handle determining how to move the
    % robot from a current position to desired position, in addition to
    % figuring out other high level planning operations... It is held by
    % the robot controller class...
    %
    % Usage : TODO
    %
    % Author : Lindsay
    % Date : 24/03/2015
    
    properties
    end
    
    methods (Abstract)
        
    moveToPoint(position);
    % moveToPoint: moves the robot from current position to the given position. Position is a vector x, y, z 
    
    reachablePoint(point);
    % reachablePoint: checks if a given point is reachable. Returns 0 if 
    % it is not,1 if it is
    
    moveTableToConveyor();
    % moveTableToConveyor: moves the robot from the table home position to
    % the conveyor home position
    
    moveConveyorToTable();
    % moveConveyorToTable: moves from conveyor home position to table home
    % position
    
    tableHomePosition();
    % tableHomePosition: moves robot from current pose to table home position 
    
    conveyorHomePosition();
    %conveyorHomePosition: moves robot from current pose to conveyor home
    %position
    
    initialise();
    %initialise: move from the robots initial pose to our table home
    %position
    
    overTheTable();
    %overTheTable: checks if the robot end effector is over the table
    
    overTheConveyor(); 
    %overTheConveyor: checks if the end effector is over the conveyor
    
    end
    
    % !!!! Please do not add any concrete methods here... do that in the
end

