classdef RobController < RobControllerInterface
    %RobController : This class holds the state of the robot (from the
    %robot) , the current mode of the robot (automatic or manual) and the
    %desired state of the robot (from the gui --> i.e if we are
    %automatically moving the robot then it might hold the desired joint
    %positions. It also contains another object called planner which
    %determines how the robot moves from a desired to alternate position
    %and how it handles higher level actions)
    %
    % Author : Lucy, Francis, Patrick and Lindsay
    % Date : 24/03/2015
    
    %% Properties
    properties (Access = public)
        % Everyone can see and modify these properties
    end
    
    properties (Constant)
        maxJointSpeed = 400; %degrees per second
        maxLinearSpeed = 400; %mm per second
        defaultLinearSpeed = 350;
        defaultJointSpeed = 350;
    end
    
    properties (Access = private)
        % Only this class can access these properties
        mode; % 1 for manual and 0 for automatic??
        planner;
        robotPose;
        vision;
        
        tableChocolates;
        chocMatrix; % current matrix like in assignment 1 that stores our chocolate locations...
        overTable; % 1 if robot over table, 0 if robot over conveyor...
        
        conveyorBox;
        commsState;
        motionMode; % tells us if the robot is moving = 1 or stopped/pause = 0
        buffer;
        stateVector; % bypass by patrick
        outOfRange; % tells us if the robot can reach its target
        newOutOfRange;
        linearMode;
        conStat; % boolean to know the status of conveyor, couldn't find it anywhere. Pat
        EStop; % boolean to know if the robot is in Estop or not.
        Ready; % boolean to know if robot is ready for a new movement or not. Does not include IO.
        ReadyCheck; % counter to figure out how many times the robot has been ready to avoid errors due to communications not in sync
    end
    
    
    %% Methods
    methods (Access = public)
        
        function this = RobController()
            % Constructor : constructs our robot object
            % Usage : it should be called whenever the default constructor is
            % called
            % Author : Lucy and Francis Date: 21/04/2015
            this.overTable=0;
            
            this.name='RobController';
            this.mode=1; % manual mode to start with --> NB: this is just a dummy...
            this.planner=Planner(); % give this object a planner object...
            this.motionMode=2; % initially the robot is not moving
            
            this.robotPose = struct('jointAngles', [0 0 0 0 0 0], 'position',...
                [439 0 630], 'quaternions', [0 90 0 0], 'iostates', [0, 0, 0, 0], ...
                'desiredPosition', [430 0 630], 'desiredJAngles', [0 0 0 0 0 0]);
            this.tableChocolates = struct('locations', [], 'orientations',...
                [], 'types', []);
            this.conveyorBox = struct('boxLocations', [], 'boxRotations', [], ...
                'chocLocations', [], 'chocOrientations', []);
            this.buffer = struct('mode', [], 'goal', [], 'speed', []);
            this.log('Rob Controller Constructed\n');
            this.stateVector = [0,0,0,0];
            this.EStop = 0;
            this.conStat = 0;
            this.Ready = 0;
            this.ReadyCheck = 0;
        end
        
        function addHandles (this,guiHandle,siocomms,movecomms,vision)
            % addHandles : adds the gui object handle, siocomms handle and
            % movecomms handle...
            % Author : Lucy and Francis ??? I wrote this..Lindsay, Date: 21/04/2015
            this.guiHandle=guiHandle;
            this.siocommsHandle = siocomms;
            this.movecommsHandle = movecomms;
            this.vision=vision;
        end
        
        function out = getRobotPose(this)
            %getRobotPose: returns the pose of the robot
            % Author : Lucy and Francis, Date: 21/04/2015
            out = this.robotPose;
            return
        end
        
        % distance can be positive (towards robot) or negative (away from
        % robot)
        function moveConveyorDistance(this,distance)
            % Determine the time the conveyor should be active
            % conveyor moves at 75 mm/s
            % Author: Partick & Lindsay, Date: 31/05/2015
            time = distance/75;
            this.movecommsHandle.moveConveyor(time);
            return
        end;
        
        function joints = getCurJoints(this)
            %getCurrJoints: Returns an array holding the angle for each
            %joint of the robot
            % Author: Patrick & Lindsay, Date: 31/05/2015
            joints = this.robotPose.jointAngles;
            return
        end
        
        function ready = isReady(this)
            % isReady: Checks if the robot is ready for the next command to
            % be sent to it. Returns 0 for false, 1 for true
            % Author: Patrick & Lindsay, Date: 31/05/2015
            ready = this.Ready;
            return
        end;
        
        function Boxes = getBoxes(this)
            % getBoxes: returns a struct with box locations, rotations and
            % orientations
            % Author: Patrick & Lindsay, Date: 31/05/2015
            Boxes = this.conveyorBox;
            return
        end;
        %% Functions you should implement...
        
        %% Robot State Update Functions
        % These functions update the current state of the robot (here)
        % and also the gui representation of the robot by calling appropriate
        % functions
        % These functions will be called by our comms objects
        
        function changeMode (this,mode)
            %changeMode: Will change the robot from inactive (0) to  manual control mode (1) to
            %automatic control mode (2)
            %Must be called by the gui to enable the robot at startup...
            %Usage: this.changeMode(1);
            %Author Lindsay , Date : 24/03/2015
            
            this.mode=mode;
            this.guiHandle.updateRobotMode(this.mode);
        end
        
        function changeMotionMode(this, motionMode)
            %changeMotionMode: Will change the motion mode of the robot from
            %stopped/paused(0) and active(1)
            %Author : Lucy and Francis, Date: 21/04/2015
            
            this.motionMode = motionMode;
        end
        
        function updateGUIDesired(this)
            % updateGuiDesired: when called by the robotController will
            % update the respective fields in the gui to reflect the new
            % desired pose
            % Author: Partick & Lindsay, Date:31/05/2015
            this.guiHandle.updateRobotDesired(this.robotPose.desiredPosition, ...
                this.robotPose.desiredJAngles, [0 0 0]);
        end
        
        function out = updateRobotPose(this, poseVector)
            %updateRobotPose: Will update the stored pose of the robot.
            %poseVector is a vector holding joint angles, position of end
            %effector and rotation of end effector in that order.
            %poseVector = [x, y, z, q1, q2, q3, q4, j1, j2, j3, j4, j5, j6,
            %io1, io2, io3, io4, dx, dy, dz, dj1, dj2, dj3, dj4, dj5, dj6,
            %outOfRange,conStat,EStop]
            %Author: Lucy, Date: 31/03/2015
            
            outOfRangePosInVect = 27; % positions of OOR, conStat and EStop
            conStatPosInVect = 28;
            EStopPosInVect = 29;
            ReadyPosInVect = 30;
            
            try
                if(length(poseVector)==30)
                    for i = 1:6
                        if(i <=3)
                            this.robotPose.position(i) = poseVector(i);
                            this.robotPose.desiredPosition(i) = poseVector(i+17);
                        end
                        
                        if (i<= 4)
                            this.robotPose.quaternions(i) = poseVector(i+3);
                            this.robotPose.iostates(i) = poseVector(i+13);
                        end
                        
                        this.robotPose.jointAngles(i) = poseVector(i+7);
                        this.robotPose.desiredJAngles(i)= poseVector(i+20);
                    end
                    
                    this.outOfRange = poseVector(outOfRangePosInVect);
                    this.conStat = poseVector(conStatPosInVect);
                    this.EStop = poseVector(EStopPosInVect);
                    this.ReadyCheck = this.ReadyCheck + poseVector(ReadyPosInVect);
                    if(poseVector(ReadyPosInVect) == 0) % robot considers himself not ready
                        this.Ready = 0;
                        this.ReadyCheck = 0;
                    end
                    if(this.ReadyCheck == 3) % triple check
                        this.Ready = 1;
                    end
                    
                    if((this.outOfRange > 0) & (this.newOutOfRange == 1)) % newOutOfReach as to not spam the console....
                        this.error('Error, robot cannot get there!');
                        this.log('Error, robot cannot get there!');
                        this.newOutOfRange = 0;
                    end
                    
                    if this.outOfRange == 0
                        this.newOutOfRange = 1; % if we're in range, then an error would necessarily be new
                    end
                    
                    this.log('ready ', this.Ready);
                    this.log('ready check ', this.ReadyCheck);
                    % Lets change the parts in the gui...
                    this.guiHandle.updateRobotIOState(this.robotPose.iostates);
                    this.guiHandle.conveyorState(this.conStat);
                    
                    %change quaternions to euler angles for gui
                    q = this.robotPose.quaternions;
                    roll = atan2(2*(q(1)*q(2) + q(3)*q(4)),(1-2*(q(2)^2 + q(3)^2)));
                    pitch = asin(2*(q(1)*q(3) - q(4)*q(2)));
                    yaw = atan2(2*(q(1)*q(4) + q(2)*q(3)), 1-2*(q(3)^2 + q(4)^2));
                    
                    this.guiHandle.updateRobotPose(this.robotPose.position, ...
                        this.robotPose.jointAngles, [roll pitch yaw]);
                    
                    this.checkClearScene();
                else
                    this.log('pose vector is the wrong length');
                end
                
            catch e
                this.error(strcat('Something went wrong in updateRobotPose : ',getReport(e)));
            end
        end
        
        
        %% Chocolate State Update
        function updateChocMatrix(this,c)
            % updateChocMatrix: updates the chocolate matrix with the most
            % current chocolate details
            % Author: Lindsay, Date: 21/04/2015
            this.chocMatrix=c;
            this.log('Update our chocolate c matrix');
        end
        
        function out = updateTableChocolates(this,chocolateLocations,...
                chocolateOrientations,chocolateRotations, chocolateTypes)
            %updateTableChocolates: Updates position, orientation, rotation and
            % type of each chocolate on the table.
            % Input: chocolateLocations - vector storing x, y values of chocolate
            %                             centre respectively
            %        chocolateOrientations - vector storing 'upside down' (-1) or
            %                                'upside up' (+1)
            %        chocolateRotations - vector storing a theta value for each
            %                             chocolate
            %        chocolateTypes - vector storing chocolate type as: {} !!(whatever mark has specified)
            %Author : Lucy and Francis, Date: 21/04/2015
            
            % Lindsay --> Pat : are you using this at all.... I have changed it
            % to just store the matrix that i get when i scan for chocolate...
            
            this.tableChocolates.locations = chocolateLocations;
            this.tableChocolates.orientations = chocolateOrientations;
            this.tableChocolates.types = chocolateTypes;
            this.tableChocolates.rotations = chocolateRotations;
            
            %set positive out value and log the updated locations
            out=1;
            this.log('Updated Table Chocolates');
            for i = 1:rotations.size();
                this.log('Location %d = [%f %f %f]', chocolateLocations(1),...
                    chocolateLocations(2), chocolateLocations(3));
            end
            
        end
        
        function out = updateConveyorBox(this,boxLocations,boxRotations,...
                chocolateLocations,chocolateOrientations, chocolateRotations)
            %updateConveyorBox: Updates position and rotation of box and position,
            %orientation and rotation of chocolates on conveyor.
            % Input : boxLocations - vector holding location of the centroid of boxes
            %         boxRotationss- vector holding rotation of boxes
            %         chocolate* - same an in UpdateTableChocolates
            %Author : Lucy and Francis, Date: 21/04/2015
            
            this.conveyorBox.boxLocations = boxLocations;
            this.conveyorBox.boxRotations = boxRotations;
            this.conveyorBox.chocLocations = chocolateLocations; % Not used??
            this.conveyorBox.chocOrientation = chocolateOrientations; % Not used??
            this.conveyorBox.chocRotatios = chocolateRotations; % Not used...
            
            %set positive out value and log the updated values
            out=1;
            this.log('updateConveyorbox : ');
            
        end
        
        %% Manual and Automatic Movement Commands to be called by GUI
        function out = manualRobotMove(this,movementType,joystick,speed)
            % manualRobotMove : move the robot in either cartesian or joint
            % mode given variables from the gui
            %Inputs: movementType - 0 (joint mode) or 1 (cartesian mode)
            %        joystick - if joint mode: vector holding Joint number
            %                   and positive(=1) or Negative(-1)
            %                 - if cartesian mode: vector holding x, y, z
            %                   values (positive(=1), negative(=-1), no
            %                   movement(=0))
            %        speed - desired speed of movement
            %Author : Lucy and Francis, Date: 21/04/2015
            
            if this.motionMode >=0
                this.motionMode = 1; % set robot to be moving
                this.mode=1; % set robot to manual mode
            else
            end
            
            this.clearBuffer();
            
            % treating the speed
            this.setSpeed(speed);
            this.log('set speed to', speed);
            %JOINT MODE: Set target to max positive or negative angle for the joint.
            if(movementType == 0)
                
                
                % make vectors holding max positive and negative angles fo reach joint
                maxPositive = [165, 110, 70, 160, 120, 400];
                maxNegative = [-165, -110, -110, -160, -120, -400];
                
                moveVec = this.robotPose.jointAngles; % fixed by Pat 2015/04/19, was [0,0,0,0,0,0] originally
                
                joystick(1) = -joystick(1); % for left-right use
                for i = 1:6
                    if(joystick(i) > 0)
                        moveVec(i) = maxPositive(i);
                    else if (joystick(i) < 0)
                            moveVec(i) = maxNegative(i);
                        end
                    end
                end
                
                this.movecommsHandle.moveJPos(moveVec(1),moveVec(2),moveVec(3),moveVec(4),moveVec(5),moveVec(6));
                this.movecommsHandle.setLinearMode(0);
                this.movecommsHandle.setXYZMode(0);
                this.movecommsHandle.resume();
                this.Ready = 0; % robot not ready anymore
                this.ReadyCheck = 0; % reset counter
                this.log('Move in joint mode... with joint goals:',moveVec);
                %  this.fillBuffer(0, moveVec, [this.defaultLinearSpeed
                %  speed]); %For future assignment
                
                out=1;
                
                %CARTESIAN MODE
            else
                % set target to max distance robot can move in the
                % direction of the 'joystick' vector
                goal = this.robotPose.position;
                maxPositive = [450 300 300]; % in robot frame
                maxNegative = [200 -300 157];
                
                % capping, might introduce "strange" movements at the
                % beginning
                for i = 1:3
                    if goal(i) < maxNegative(i)
                        goal(i) = maxNegative(i);
                    end
                    if goal(i) > maxPositive(i)
                        goal(i) = maxPositive(i);
                    end
                end
                
                % treating inputs
                joystick = joystick(1:3);
                
                temp = joystick(1); % swapping X-Y coordinates
                joystick(1) = joystick(2);
                joystick(2) = temp;
                joystick = -joystick; % somehow fully inverted w.r.t common sense.
                this.log('inputs for movement ', joystick);
                II = joystick > 0;
                if(~isempty(joystick(II)))
                    goal(II) = maxPositive(II);
                else
                    II = joystick < 0;
                    if(~isempty(joystick(II)))
                        goal(II) = maxNegative(II);
                    end
                end
                
                % coordinates have to be set before setting the XYZMode
                this.movecommsHandle.setLinearMode(1);
                this.movecommsHandle.moveCartPos(goal(1),goal(2),goal(3),1);
                this.movecommsHandle.setXYZMode(1);
                this.movecommsHandle.resume();
                this.Ready = 0;
                this.ReadyCheck = 0;
                %     this.fillBuffer(3, goal, [speed this.defaultJointSpeed]); Not working, bypassed. Pat
                this.log('Manual Robot Cartesian Move:',goal);
                out=0;
            end
            this.updateGUIDesired(); % update our gui's desired pose representation...
        end
        
        
        function out = moveToConveyorImage(this,pixCoords)
            % moves the robot to the coordinates found in the picture of the
            % conveyor
            % pixCoords = [x,y] with relation to the top left of the
            % picture
            % temp function for assignment 2
            % Author: Patrick
            % date: 2015-04-20
            this.log('conveyor input: ',pixCoords);
            pixels2mm = 0.7202; % ratio for conversion in mm
            XYZ = [0,0,32]; % z of the conveyor is at 22.
            
            XYZ(1) = -pixCoords(1) + 800; % offset + turning round
            XYZ(2) = (pixCoords(2) + 100); % offset
            XYZ(1:2) = pixels2mm*XYZ(1:2); % conversion
            
            % avoid singularities
            this.movecommsHandle.setLinearMode(this.linearMode);
            this.moveCartPos(XYZ); % sending
            this.movecommsHandle.setXYZMode(1);
            this.movecommsHandle.resume();
            this.Ready = 0; % robot is not ready anymore
            this.ReadyCheck = 0;
            this.log('Robot coordinates: ', XYZ(1:2));
            this.updateGUIDesired(); % update our gui's desired pose representation...
        end
        
        
        function out = moveToTableImage(this,pixCoords)
            % Moves the robot to the coordinates found in the picture of the
            % table
            % pixCoords = [x,y] with relation to top left corner of
            % pictures
            % temp function for assignment 2
            % Author: Patrick
            % date: 2015-04-20
            this.log('table input: ',pixCoords);
            pixels2mm = 0.6477; % ratio for pixels/mm conversion
            XYZ = [0,0,157];  % offset for the table
            pixCoords(1) = pixCoords(1) - 800;  % getting correct frame
            pixCoords(2) = pixCoords(2) + 140;
            XYZ(1:2) = pixels2mm*pixCoords; % setting proportionality
            
            temp = XYZ(1); % swap
            XYZ(1)= XYZ(2);
            XYZ(2) = temp;
            
            % avoid singularities
            this.movecommsHandle.setLinearMode(this.linearMode);
            this.moveCartPos(XYZ); % Setting the goal before allowing for movement
            this.movecommsHandle.setXYZMode(1); % setting movement in XYZ mode
            this.movecommsHandle.resume();
            this.log('Robot coordinates for mouse click: ', XYZ(1:2));
            this.updateGUIDesired(); % update our gui's desired pose representation...
        end
        
        
        function out = moveCartPos(this, XYZ)
            % Moves the robot to the desired XYZ coordinates if possible.
            % temp function for assignment 2
            % Author: Patrick
            % date: 2015-04-20
            
            % Sending to XYZ coordinates, removing linear mode to avoid
            % singularities
            
            this.movecommsHandle.moveCartPos(XYZ(1),XYZ(2),XYZ(3),1);
            this.movecommsHandle.setLinearMode(this.linearMode);
            this.movecommsHandle.setXYZMode(1);
            this.movecommsHandle.resume();
            this.updateGUIDesired(); % update our gui's desired pose representation...
            this.Ready = 0;
            this.ReadyCheck = 0;
            this.log('set Ready to 0');
        end
        
        function out = moveCartPosNoOrient(this, XYZ)
            % Moves the robot to the desired XYZ coordinates if possible.
            % keeps current orientation
            % Author: Patrick
            % date: 2015-04-20
            
            % Sending to XYZ coordinates, removing linear mode to avoid
            % singularities
            %   this.movecommsHandle.stop();
            this.movecommsHandle.moveCartPos(XYZ(1),XYZ(2),XYZ(3),0);
            this.movecommsHandle.setLinearMode(this.linearMode);
            this.movecommsHandle.setXYZMode(1);
            this.movecommsHandle.resume();
            this.updateGUIDesired(); % update our gui's desired pose representation...
            this.Ready = 0;
            this.ReadyCheck = 0;
            this.log('set Ready to 0');
        end
        
        function out = setSpeed(this, Speed)
            % Sets the speed of the robot, tests the values given then sends
            % through to the MoveComms handle.
            % Speed = [v_tcp, v_ori, v_leax, v_reax]
            % temp function for assignment 2
            % Author: Patrick
            % date: 2015-04-19
            
            
            maxSpeed = [this.maxLinearSpeed, this.maxJointSpeed, this.maxLinearSpeed, this.maxJointSpeed];
            minSpeed = [20,20,20,20];
            II = Speed > maxSpeed;
            Speed(II) = maxSpeed(II);
            II = Speed < minSpeed;
            Speed(II) = minSpeed(II);
            
            this.movecommsHandle.setSpeed(Speed);
        end
        
        function moveCartQuatPos(this,x,y,z, Quaternions)
            % Move the robot in the cartesian position
            % x,y,z coordinates w.r.t the base of the robot
            % Quaternions is [q1, q2, q3, q4] with norm 1.
            % Author: Patrick
            
            this.movecommsHandle.moveCartQuatPos(x,y,z,Quaternions);
            this.Ready = 0;
            this.ReadyCheck = 0;
            this.log('set Ready to 0');
        end
        
        function out = moveJPos(this, Joints)
            % Sets the position of the robot, tests the values given and sends
            % to the MoveComms handle
            % Joints = [J1, J2, J3, J4, J5, J6]
            % temp function for assignment 2
            % Author: Patrick
            % date: 2015-04-19
            
            minJPos = [-165, -110, -110, -160, -120, -400];
            maxJPos = [165,   110,   70,  160,  120,  400];
            % checking the values
            Original = Joints;
            Joints = Joints(1:6); % making sure there's only 6 values
            II = Joints > maxJPos;
            for i=1:length(II)
                if II(i) == 1
                    message = strcat('value for joint ', num2str(i), ' exceeded, ', num2str(Original(i)),' defaulted to ', num2str(maxJPos(i)));
                    this.guiHandle.commandWindow(message);
                end
            end
            Joints(II) = maxJPos(II);
            
            II = Joints < minJPos;
            for i=1:length(II)
                if II(i) == 1
                    message = strcat('value for joint ', num2str(i), ' exceeded (negative), ', num2str(Original(i)),' defaulted to ', num2str(minJPos(i)));
                    this.guiHandle.commandWindow(message);
                end
            end
            Joints(II) = minJPos(II);
            
            % setting target, then allowing for movement
            this.movecommsHandle.moveJPos(Joints(1),Joints(2),Joints(3), Joints(4), Joints(5), Joints(6));
            this.movecommsHandle.setXYZMode(0);
            this.movecommsHandle.resume();
            this.updateGUIDesired(); % update our gui's desired pose representation...
            this.Ready = 0;
            this.ReadyCheck = 0;
            this.log('set Ready to 0');
        end
        
        function out = setAutoDesiredLocation (this,coordinateSystem, location,speed)
            % setAutoDesiredLocation : sets a location for the robot to go to if in automatic mode...
            % called by gui on mouse click
            % Input: coordinateSystem - 0 for table camera
            %                         - 1 for conveyor camera
            %                         - 2 for numbers typed into gui
            %Author : Lucy and Francis
            
            this.clearBuffer();
            
            if(coordinateSystem == 0)
                %translate coord system: move to middle of screen and
                %rotate 90
                location(1) = location(1) - 800;
                temp = location(1);
                location(1) = location(2);
                location(2) = temp;
                
                % convert from pixels.
                pixelFactor = 0.6477;
                location = location*pixelFactor;
                
                %send location to planner and move there.
                this.setNewSpeed([speed this.maxJointSpeed])
                this.planner.moveToPoint(location);
                
                
            else if (coordinateSystem == 1)
                    %!!over the conveyor. Do this!
                    pixelRatio = 0.7202;
                    
                else if (coordinateSystem == 2)
                        this.setNewSpeed([speed this.maxJointSpeed]);
                        this.planner.moveToPoint(location);
                    end
                end
            end
            
            out=0;
            this.log('set Auto Desired Location');
            this.updateGUIDesired(); % update our gui's desired pose representation...
        end
        
        %% Robot Command Functions to be called by the GUI
        function out = reachablePoint(this, point)
            %reachablePoint: checks if a point is reachable. Returns 1 if
            %it is, zero if not
            %Input: point - (x, y, z)
            %Author : Lucy & Francis, Date: 31/05/2015
            
            out = this.planner.reachablePoint(point);
        end
        
        function out = setLinearMode(this,newMode)
            % checks and sets linearMode of the robot
            % input bool. true = linear motion, false = joint motion
            % author: Patrick
            % date: 2015-04-21
            if newMode
                this.linearMode = 1;
                this.movecommsHandle.setLinearMode(1);
            else
                this.linearMode = 0;
                this.movecommsHandle.setLinearMode(0);
            end
        end
        
        function out = pauseRobot(this)
            % pauseRobot: Tell robot studio to pause movement
            % Author: Lucy & Francis, Date: 31/05/2015
            
            % update our motionState
            this.motionMode=0; % not moving
            
            % send stop command to robot...
            this.movecommsHandle.stop();
            
            out=0;
            this.log('Stop the Robot\n');
        end
        
        function out = stopRobot(this)
            %stopRobot: will stop the robot and clear the buffer
            % Author: Lucy & Francis, Date: 31/05/2015
            this.motionMode = 0;
            this.movecomms.stop();
            this.clearBuffer();
            
            this.log('Robot has been stopped');
            out = 1;
        end
        
        function out = restartRobot(this)
            % restartRobot: Send comms to robotstudio to restart robot motions
            % Author: Lucy & Francis, Date: 31/05/2015
            
            % update our motion mode...
            this.motionMode=1; % moving again
            
            % send resume command
            this.movecommsHandle.resume();
            
            out=0;
            this.log('Restart Robot\n');
        end
        
        function out = enableVacuumPump(this)
            % enableVaccumPump: Send comms to robotstudio to enable vaccum
            % pump
            % Author: Lucy & Francis, Date: 31/05/2015
            this.log('try to enable the vacuum pump');
            
            % update vacuum desired state
            states = this.stateVector;
            this.log('previous state : ', states);
            states(4)=1;
            this.log('new state: ', states);
            this.stateVector = states;
            this.movecommsHandle.setIOState(states);
            out=1;
            
        end
        
        function out = disableVacuumPump(this)
            % disableVaccumPump: send comms to robot studio to disable vac
            % pump
            % Author: Lucy & Francis, Date: 31/05/2015
            this.log('try to disable the vacuum pump');
            
            states = this.stateVector;
            this.log('previous state : ', states);
            states(4) = 0;
            this.log('new state : ', states);
            this.stateVector = states;
            this.movecommsHandle.setIOState(states);
            out=1;
        end
        
        function out = enableSuction(this)
            % enableSuction: send comms to robot studio to enable suction
            % Author: Lucy & Francis, Date: 31/05/2015
            this.log('try to enable pump suction');
            states = this.stateVector;
            this.log('previous state : ', states);
            states(3)=1;
            this.log('new state : ', states)
            this.stateVector = states;
            this.movecommsHandle.setIOState(states);
            out=1;
            
        end
        
        function out = disableSuction(this)
            %disableSuction: send comms to robot studio to disable suction
            % Author: Lucy & Francis, Date: 31/05/2015
            this.log('try to disable pump suction');
            states = this.stateVector;
            this.log('previous state : ', states);
            states(3)=0;
            this.log('new state : ', states)
            this.stateVector = states;
            this.movecommsHandle.setIOState(states);
            out=1;
            
        end
        
        function out = enableConveyor(this)
            % enableConveyor: sends comms to robot studio to enable
            % conveyor
            % Author: Lucy & Francis, Date: 31/05/2015
            this.log('try to enable the conveyor belt');
            states = this.stateVector;
            this.log('previous state : ', states);
            states(1) = 1;
            this.log('new state : ', states)
            this.stateVector = states;
            this.movecommsHandle.setIOState(states);
            out = 1;
            
        end
        
        function out = disableConveyor(this)
            % disableConveyor: sends comms to robot studio to enable the
            % conveyor.
            % Author: Lucy & Francis, Date: 31/05/2015
            this.log('try to disable the conveyor the vacuum pump');
            states = this.stateVector;
            this.log('previous state : ', states);
            states(1)=0;
            this.log('new state : ', states)
            this.stateVector = states;
            this.movecommsHandle.setIOState(states);
            out=1;
            
        end
        
        function out = convDir(this,direction)
            % convDir: sends comms to robot studio to change the conveyor
            % direction: 1 = forward, 0 = backward
            % Author: Lucy & Francis, Date: 31/05/2015
            this.log('Change the direction of the conveyor : ',direction);
            states = this.stateVector;
            this.log('previous state : ', states);
            states(2)= direction;
            this.log('new state : ', states)
            this.stateVector = states;
            this.movecommsHandle.setIOState(states);
            out=1;
            
        end
    end
    
    methods (Access = private)
        % These are only called by the robot controller
        
        function checkPose(this)
            % checkPose: checks if robot is about to reach its current
            % goal. If so send it the next point in buffer. This will be
            % called everytime the robot state is updated
            % Author: Lucy & Francis Date: 21/04/2015
            
            if(size(this.buffer.mode) > 0)
                if((max(this.robotPose.desiredPosition - this.robotPose.position) < 10)||...
                        (max(this.robotPose.desiredJAngles - this.robotPose.jointAngles) < 5))
                    %check mode of next element in buffer
                    if(this.buffer.mode(1) == 0)
                        this.movecommsHandle.moveJPos(this.buffer.goal(1, 1),...
                            this.buffer.goal(1, 2),this.buffer.goal(1, 3), ...
                            this.buffer.goal(1, 4), this.buffer.goal(1, 5), ...
                            this.buffer.goal(1, 6));
                        this.movecommsHandle.setLinearMode(0);
                        this.movecommsHandle.setXYZMode(0);
                        this.setNewSpeed([this.buffer.speed(1) this.buffer.speed(2)])
                    else if(this.buffer.mode(1) == 1)
                            this.movecommsHandle.moveCartPos(this.buffer.goal(1,1),...
                                this.buffer.goal(1,2), this.buffer.goal(1,3), 1);
                            this.movecommsHandle.setLinearMode(0);
                            this.movecommsHandle.setXYZMode(1);
                            this.setNewSpeed([this.buffer.speed(1) this.buffer.speed(2)])
                        else if(this.buffer.mode(1) == 2)
                                this.movecommsHandle.moveJPos(this.buffer.goal(1,1),...
                                    this.buffer.goal(1,2), this.buffer.goal(1,3),...
                                    this.buffer.goal(1,4), this.buffer.goal(1,5),...
                                    this.buffer.goal(1,6));
                                this.movecommsHandle.setLinearMode(1);
                                this.movecommsHandle.setXYZMode(0);
                                this.setNewSpeed([this.buffer.speed(1,1) this.buffer.speed(1,2)])
                            else if(this.buffer(1, 1) == 3)
                                    this.movecommsHandle.moveCartPos(this.buffer.goal(1,1),...
                                        this.buffer.goal(1,2), this.buffer.goal(1,3), 1);
                                    this.movecommsHanlde.setLinearMode(1);
                                    this.movecommsHandle.setXYZMode(1);
                                    this.setNewSpeed([this.buffer.speed(1) this.buffer.speed(2)])
                                end
                            end
                        end
                    end
                    
                    this.log('checkPose: Sent next point. mode: ', this.buffer.mode(1));
                    this.log('\t goal: ', this.buffer.goal(1));
                    this.log('\t speed: ', this.buffer.speed(1))
                    
                    %delete the first line of buffer
                    this.buffer.mode = this.buffer.mode(2:end);
                    this.buffer.goal = this.buffer.goal(2:end);
                    this.buffer.speed = this.buffer.speed(2:end);
                end
            end
        end
        
        function out = fillBuffer(this, mode, goal, speed)
            % fillBuffer: updates buffer with a given mode, speed and goal
            % INPUT: mode: 0 - move to joint angle targets via joint mode
            %              1 - move to cart. targets via joint mode
            %              2 - move to joint angle targets via linear mode
            %              3 - move to cart. targets via linear  mode
            %        goal: if goal is cartesian (x, y, z, 0, 0, 0)
            %              if goal is joint angles (j1, j2, j3, j4, j5, j6)
            %        speed: integer specifying desired speed
            % Author: Lucy & Francis Date: 21/04/2015
            
            %check if mode is valid
            if(mode < 4 && mode >= 0)
                this.buffer.mode = [this.buffer.mode; mode];
                %if cartesian mode, check point is reachable
                if(mode == 1 || mode == 3)
                    if(this.planner.reachablePoint(goal(1:3)))
                        this.buffer.goal = [this.buffer.goal; goal];
                        this.buffer.speed = [this.buffer.speed; speed];
                        
                        this.log('updated buffer with: mode = ', mode);
                        this.log('\t goal = ', goal);
                        this.log('\t speed = ', speed);
                        this.log('\n');
                    else
                        this.log('fillBuffer: cart. goal out of range: ', goal);
                        this.buffer.mode = this.buffer.mode(1 : size(this.buffer.mode(-1)));
                    end
                    % if in joint mode, check all angles are in range
                else
                    maxPositive = [165, 110, 70, 160, 120, 400];
                    maxNegative = [-165, -110, -110, -160, -120, -400];
                    
                    if((min(maxPositive - goal) >= 0) &&...
                            (max(maxNegative - goal) <= 0))
                        this.buffer.goal = [this.buffer.goal; goal];
                        this.buffer.speed = [this.buffer.speed; speed];
                        
                        this.log('updated buffer with: mode = ', mode);
                        this.log('\t goal = ', goal);
                        this.log('\t speed = ', speed);
                        this.log('\n');
                    else
                        this.log('fillBuffer: Joints goal out of range: ', goal);
                        this.buffer.mode = this.buffer.mode(1 : size(this.buffer.mode(-1)));
                    end
                    
                end
            else
                this.log('fillbuffer: mode value is out of range');
            end
            
            out = 1;
        end
        
        function clearBuffer(this)
            % clearBuffer: clear the buffer. Will use this during manual
            % movement or if stop is called.
            % Author: Lucy & Francis Date: 21/04/2015
            this.buffer.mode = [];
            this.buffer.goal = [];
            this.buffer.speed = [];
        end
        
        function setNewSpeed(this, speed)
            % setNewSpeed: sets the wanted speed of the robot.
            % Author: Lucy & Francis Date: 21/04/2015
            if((speed(1) > 0) && (speed(2) > 0))
                if(speed(1) > this.maxLinearSpeed)
                    speed(1) = this.maxLinearSpeed;
                end
                if(speed(2) > this.maxJointSpeed)
                    speed(2) = this.maxJointSpeed;
                end
                
                this.movecommsHandle.setSpeed(speed(1), speed(2), speed(1), speed(2));
                this.log('setNewSpeed: linear ', speed(1));
                this.log('\t angular', speed(2));
                this.log('\n');
            end
        end
        
        function checkClearScene (this)
            % checkForImCap : this will determine whether the table or the
            % conveyor image is clear
            % Author: Patrick Bobbink, Date: 31/05/2015
            
            joints=this.robotPose.jointAngles;
            tableThresh = 85; %any angle greater than 85 degrees in joint 1 means table is clear
            
            if joints(1) > tableThresh && this.overTable==1
                this.overTable=0;
                this.vision.triggerProcessTableIm();
            elseif joints(1) <= tableThresh && this.overTable==0
                this.overTable=1;
                this.vision.triggerProcessConvIm();
            end;
        end
    end
    
    
end

