classdef (Abstract) RobControllerInterface < Process
    %RobControllerInterface :  The RobController class holds the state of the robot (from the
    %robot) , the current mode of the robot (automatic or manual) and the
    %desired state of the robot (from the gui --> i.e if we are
    %automatically moving the robot then it might hold the desired joint
    %positions. It also contains another object called planner which
    %determines how the robot moves from a desired to alternate position
    %and how it handles higher level actions)
    %
    % Author : Lindsay, Patrick, Francis, Lucy
    % Date : 31/05/2015
    
    properties (Access = protected)
        guiHandle;
        siocommsHandle;
        movecommsHandle;
    end
    
    methods (Abstract)

    %% Functions you should implement...
    
    %% Robot State Update Functions
    % These functions must update the current state of the robot
    % and also the gui representation of the robot by calling appropriate
    % functions.
    % These functions will be called by our comms objects

    changeMode (this,mode)
    % changeMode: Will change the robot from inactive (0) to  manual control mode (1) to
    % automatic control mode (2)
    % Must be called by the gui to enable the robot at startup...
    % Usage: this.changeMode(1);
    
    changeMotionMode(this, motionMode)
    %changeMotionMode: Will change the motion mode of the robot from
    %stopped/paused(0) and active(1)
    %Author : Lucy and Francis, Date: 21/04/2015
    
    updateGUIDesired(this)
    % updateGuiDesired: when called by the robotController will
    % update the respective fields in the gui to reflect the new
    % desired pose
    % Author: Partick & Lindsay, Date:31/05/2015
    
    updateRobotPose(this,poseVector)
    %updateRobotPose: Will update the stored pose of the robot.
    %poseVector is a vector holding joint angles, position of end
    %effector and rotation of end effector in that order.
    %poseVector = [x, y, z, q1, q2, q3, q4, j1, j2, j3, j4, j5, j6,
    %io1, io2, io3, io4, dx, dy, dz, dj1, dj2, dj3, dj4, dj5, dj6,
    %outOfRange,conStat,EStop]
    %Author: Lucy, Date: 31/03/2015
    
    
    %% Chocolate State Update
    updateTableChocolates(this,chocolateLocations,chocolateOrientations, ...
        chocolateRotations ,chocolateTypes)
    %updateTableChocolates: Updates position, orientation, rotation and 
    % type of each chocolate on the table. 
    % Input: chocolateLocations - vector storing x, y values of chocolate
    %                             centre respectively
    %        chocolateOrientations - vector storing 'upside down' (-1) or 
    %                                'upside up' (+1)
    %        chocolateRotations - vector storing a theta value for each
    %                             chocolate
    %        chocolateTypes - vector storing chocolate type as: {} !!(whatever mark has specified)
    
    updateConveyorBox(this,boxLocations,boxRotations,chocolateLocations,...
        chocolateOrientations,chocolateRotations)
    %updateConveyorBox: Updates position and rotation of box and position,
    %orientation and rotation of chocolates on conveyor. 
    % Input : boxLocations - vector holding location of the centroid of boxes
    %         boxRotationss- vector holding rotation of boxes
    %         chocolate* - same an in UpdateTableChocolates
    %Author : Lucy and Francis, Date: 21/04/2015
    
    %% Manual and Automatic Movement Commands to be called by GUI

    manualRobotMove(this,movementType,joystick,speed)
    % manualRobotMove : move the robot in either cartesian or joint 
    % mode given variables from the gui 
    %Inputs: movementType - 0 (joint mode) or 1 (cartesian mode)
    %        joystick - if joint mode: vector holding Joint number
    %                   and positive(=1) or Negative(-1)
    %                 - if cartesian mode: vector holding x, y, z
    %                   values (positive(=1), negative(=-1), no
    %                   movement(=0))
    %        speed - desired speed of movement
    %Author : Lucy and Francis, Date: 21/04/2015
    
    moveToConveyorImage(this,pixCoords)
    % moves the robot to the coordinates found in the picture of the
    % conveyor
    % pixCoords = [x,y] with relation to the top left of the
    % picture
    % temp function for assignment 2
    % Author: Patrick, Date: 21/04/2015
    
    moveToTableImage(this,pixCoords)
    % Moves the robot to the coordinates found in the picture of the
    % table
    % pixCoords = [x,y] with relation to top left corner of
    % pictures
    % temp function for assignment 2
    % Author: Patrick, Date: 20/04/2015
    
    moveCartPos(this, XYZ)
    % Moves the robot to the desired XYZ coordinates if possible.
    % temp function for assignment 2
    % Author: Patrick, Date: 20/04/2015
    
    moveCartPosNoOrient(this, XYZ)
    % Moves the robot to the desired XYZ coordinates if possible.
    % keeps current orientation
    % Author: Patrick, Date: 2015-04-20
    
    setSpeed(this, Speed)
    % Sets the speed of the robot, tests the values given then sends
    % through to the MoveComms handle.
    % Speed = [v_tcp, v_ori, v_leax, v_reax]
    % temp function for assignment 2
    % Author: Patrick, Date: 19/04/2015
    
    moveCartQuatPos(this,x,y,z, Quaternions)
    % Move the robot in the cartesian position
    % x,y,z coordinates w.r.t the base of the robot
    % Quaternions is [q1, q2, q3, q4] with norm 1.
    % Author: Patrick, Date: 19/04/2015
    
    moveJPos(this, Joints)
    % Sets the position of the robot, tests the values given and sends
    % to the MoveComms handle
    % Joints = [J1, J2, J3, J4, J5, J6]
    % temp function for assignment 2
    % Author: Patrick, Date: 19/04/2015

    setAutoDesiredLocation (this,coordinateSystem, location,speed)
    % setAutoDesiredLocation : sets a location for the robot to go to if in automatic mode...
    % called by gui on mouse click
    % Input: coordinateSystem - 0 for table camera
    %                         - 1 for conveyor camera
    %                         - 2 for numbers typed into gui
    %Author : Lucy and Francis
    
    %% Robot Command Functions to be called by the GUI
    
    reachablePoint(this, point)
    %reachablePoint: checks if a point is reachable. Returns 1 if
    %it is, zero if not
    %Input: point - (x, y, z)
    %Author : Author: Lucy & Francis, Date: 31/05/2015
    
    setLinearMode(this,newMode)
    % checks and sets linearMode of the robot
    % input bool. true = linear motion, false = joint motion
    % author: Patrick, Date: 21/04/2015
    pauseRobot(this)
    % pauseRobot: Tell robot studio to pause movement
    % Author: Lucy & Francis, Date: 31/05/2015
    
    stopRobot(this)
    %stopRobot: will stop the robot and clear the buffer
    % Author: Lucy & Francis, Date: 31/05/2015

    restartRobot(this)
    % restartRobot: Send comms to robotstudio to restart robot motions
    % Author: Lucy & Francis, Date: 31/05/2015
    
    enableVacuumPump(this)
    % enableVaccumPump: Send comms to robotstudio to enable vaccum
    % pump
    % Author: Lucy & Francis, Date: 31/05/2015
    
    disableVacuumPump(this)
     % disableVaccumPump: send comms to robot studio to disable vac
     % pump
     % Author: Lucy & Francis, Date: 31/05/2015
    
     enableSuction(this)
    % enableSuction: send comms to robot studio to enable suction
    % Author: Lucy & Francis, Date: 31/05/2015
    
    disableSuction(this)
    % disableSuction: send comms to robot studio to disable suction
    % Author: Lucy & Francis, Date: 31/05/2015
    
    enableConveyor(this)
    % enableConveyor: sends comms to robot studio to enable
    % conveyor
    % Author: Lucy & Francis, Date: 31/05/2015
    
    disableConveyor(this)
    % disableConveyor: sends comms to robot studio to enable the
    % conveyor.
    % Author: Lucy & Francis, Date: 31/05/2015
    
    convDir(this,direction)
    % convDir: sends comms to robot studio to change the conveyor
    % direction. 1 = forward, 0 = backward
    % Author: Lucy & Francis, Date: 31/05/2015
   
            
    %%  %%    
    addHandles (this,guiHandle,siocomms,movecomms)
    % addHandles : adds the gui object handle, siocomms handle and
    % movecomms handle...
    % Usage : robcontroller.addHandles(guiHandle,siocommshandle,movecommshandle);
    
    end
    
end

