classdef (Abstract) CommsInterface < Process
    % CommsInterface : The comms class is a generic class that handles the common comms
    % procedures. I.e the creation of a tcp socket, sending data from a tcp
    % socket, recieving data from a tcp socket and also error handling
    % concerned with tcp sockets.
    %
    % Usage : Implement abstract methods here that are needed by this
    % class.
    %
    % Author : Lindsay
    % Date : 24/03/2015
    
    properties (Access = protected) % change to external read but no write
        robContHandle; % Handle to the robotControllerObject
        tcpSocket;
        socketState; % 
        robotPort;
        robotIpAddress;
        readMode;
        commsMode;
        dropped;
        maxDropped;
        sendOutState; 
    end
    
    methods (Abstract)
        % Note : All Methods here are just definitions
        % Do not use a function...end block to define an abstract method, use only the method signature.
        
        %%  Functions you should use
        setCommsMode(this,mode)
        % setCommsMode : this sets the comms object to recieve data from
        % robot if mode=0 , robotStudio if mode =1 and a dummy
        % server/client if mode=2
        % Usage : movecomms.setCommsMode(mode);
        % Author : Lindsay
        % Date : 24/03/2015
        
        initConnection (this,varargin)
        % initConnection : this tries to establish a tcp connection to the
        % current ipAddress and port if no arguments are supplied. Else if an ipaddress
        % and port us provided it will try to init comms to those...
        % It will report an error if unable and
        % return -1. It will return 1 if connection successful..
        %
        % Usage : movecomms.initConnection(ipAddress,port);
        % Author : Lindsay
        % Date : 24/03/2015
        
        closeConnection(this)
        % This function attempts to close the tcp socket for this class..    
        % Lets just try and close regardless if it is defined or not...
        % Usage : movecomms.initConnection(ipAddress,port);
        % Author : Lindsay
        % Date : 24/03/2015
        
        setReadMode(this,mode)
        % changeMode : this changes the mode of our comms object. mode = 0
        % means we only consider the latest message (even if there are older
        % messages in the buffer), mode = 1 means that we consider all
        % messages...
        %
        % Usage : movecomms.initConnection(ipAddress,port);
        % Author : Lindsay
        % Date : 24/03/2015
        
        sendMessage (this, message)
        % sendMessage : this tries to send a message(string) to a server. Message can
        % be either a command or a sIO ..
        %
        % Usage :this.sendMessage(message), wherein message is a string.
        % Author : Lindsay
        % Date : 24/03/2015
        
        readMessages (this)
        % readMessage: this tries to read a message from the server. It will
        % return each read message as a vector and the number of read messages... TODO - expand on this when better
        % known...
        %
        % Usage : [data,message] = this.readMessages()
        % Author : Lindsay
        % Date : 24/03/2015
        
        %% Functions you probably shouldnt touch....
        addHandles (this,robControllerHandle)
        % addHandles : adds the  robot controller object
        % handle to this object such that we can call methods in those
        % objects.
        % Usage : movecomms.addHandles(guiHandle,robControllerHandle);
        % Author : Lindsay
        % Date : 24/03/2015
        
        
    end
    
    % !!!! Please do not add any concrete methods here... do that in the
end

