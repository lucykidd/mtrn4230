classdef (Abstract) Comms < CommsInterface
    % Comms : This abstract class is a generic class that handles the common comms
    % procedures. I.e the creation of a tcp socket, sending data from a tcp
    % socket, recieving data from a tcp socket and also error handling
    % concerned with tcp sockets...
    %
    % Usage : Extend this class with a specific communications handler to
    % implement a specific communications method...
    %
    % Author : Lindsay
    % Date : 24/03/2015
    
    %% Properties
    properties (Access = public)
        % Everyone can see and modify these properties
        % NB: there is a way so that people can read properties but not write
    end
    
    properties (Access = protected)
        % Only sublclasses can access these properties
    end
    
    methods (Access = public)
        
        %% Initialisation methods and constructors
        
        function this = Comms
            this.socketState=0;
            this.componentState=0; % 0 for not initialised...
            this.robotPort=1025; % default value
            this.robotIpAddress='127.0.0.1';%default value..
            this.tcpSocket=-1; % say the socket is not valid..
            this.readMode=0; % default, only read the latest message and discard the rest...
            this.sendOutState=1;
            this.dropped=0; % number of dropped messages...
            this.maxDropped=14; % maximum number of dropped messages in a row before classed as loss..
            this.commsMode=1; % 0 is dummy sockets (i.e server and client are both running on this machine) , 1
            % 1 is for robot studio mode (should communicate with robot studio
            % by default)
            % 2 is for lab mode (should communicate with robot in lab...)
        end
        
        function delete(this)
            % delete : Comms Basic destructor .This should handle closing of the
            % socket...good to ensure we don't lose our socket...
            % Purpose : close comms gracefully when this object is
            % destroyed
            % Author : Lindsay
            % Date : 25/03/15
            
            % Lets just try and close regardless if it is defined or not...
            this.closeConnection();
            
        end
        
        function closeConnection(this)
            % closeConnection : This function attempts to close the tcp socket for this class..
            % Lets just try and close regardless if it is defined or not...
            % Usage : this.closeConnection() will close this current
            % connection
            % Author : Lindsay
            % Date : 25/04/15
            this.log('Try to close the TCP Socket ....');
            try
                fclose(this.tcpSocket);
                this.log('TCP Socket closed successfully');
                this.tcpSocket=-1;
                this.socketState=-1;
            catch e
                this.error('Unable to close the tcpSocket???');
            end
        end
        
        %% Init connections...
        
        function out = initConnection (this,varargin)
            % initConnection : this tries to establish a tcp connection to the
            % supplied ipAddress and port if supplied, otherwise open the default... It will report an error if unable and
            % return -1. It will return 1 if connection successful..
            % Usage : this.initConnection(num) if length varargin > 0 then
            % init connection to the robot...
            % Author : Lindsay , Date : 24/03/2015
            
            out=-1; % default value is unsuccessful..
            
            % Check that we are not currenttl active or connected..
            if this.socketState > 0
                this.consoleLog('Socket is already open and active...do not restart..');
                out=0; % nothing happened..
                return;
            end
            
            % 1. try to create the socket
            this.consoleLog(strcat('Try to initialise a TCP Socket to :',this.robotIpAddress,' at port:',this.robotPort));
            if nargin > 2
                % TODO - handle new ip address input...
            end
            
            try
                this.tcpSocket = tcpip(this.robotIpAddress, this.robotPort);
                set(this.tcpSocket, 'ReadAsyncMode', 'continuous'); % we don't want to block the command line...
                fopen(this.tcpSocket);
            catch err
                this.error(strcat('Unable to open TCP connection to ', this.robotIpAddress, ' on port ', num2str(this.robotPort)));
                % TODO . close the socket...?
                %this.error(getReport(err));
                this.socketState=-1;
                this.setErrorState(-1); % this component is now in a non functioning state...
                return;
            end
            
            % 2. Check if the connection is valid.
            if isvalid(this.tcpSocket) && ~isequal(get(this.tcpSocket, 'Status'), 'open')
                warning(['Could not open TCP connection to ', this.robotIpAddress, ' on port ', this.robotPort]);
                this.error(strcat('Could not open TCP connection to ', this.robotIpAddress, ' on port ', num2str(this.robotPort)));
                this.socketState=-1; % tODO - cleanup/remove me later
                
                % Sets the error state of this module to -1 : socket unable
                % to communicate with server...
                this.setErrorState(-1); % this component is now in a non functioning state... let manager know..
                return;
            else
                this.socketState=1; % Socket is good and active...
                this.consoleLog(strcat(this.name,'Socket initialised successfully!'));
                this.setErrorState(1); % set my component state to 1 to indicate it is good...
            end
            out = this.socketState;
            this.dropped=0;% start with a clean slate...
        end
        
        %% Main Methods
        
        function setReadMode(this,mode)
            % changeMode : this changes the mode of our comms object. mode = 0
            % means we only consider the latest message (even if there are older
            % messages in the buffer), mode = 1 means that we consider all
            % messages...
            % Author : Lindsay , Date : 24/03/2015
            this.readMode=mode;
        end
        
        function sendMessage (this, message)
            % sendMessage : this tries to send a message to a server. Message can
            % be either a command or a sIO ..
            % Usage : this.sendMessage (message..)
            % Author : Lindsay , Date : 24/03/2015
            t=cputime();
            if this.socketState ~= 1 % socket has not been initialised properly
                this.error('Trying to send message from an uninitialised socket ...');
                return;
            end
            
            this.log(strcat('Try to send ',this.name, ' message : ' , message));
            try
                fwrite(this.tcpSocket,message); % TODO -  possible blocking here..
            catch e
                this.error('Socket sending error encounteres..');
                this.error(e.identifier);
            end
            % TODO - more error checking here...
            this.log(strcat('Message sent successfully...'));
            t=cputime()-t;
            this.log('Time to send a raw message',t);
        end
        
        function sendData(this,binaryData)
            % sendData : this tries to send a message to a server. Message can
            % be either a command or a sIO ..
            % Usage : this.sendData(vector), where vector is something like
            % [1 0 0 0.543 1] etc...
            % Author : Lindsay , Date : 24/03/2015
            
            if this.socketState ~= 1 % socket has not been initialised properly
                this.error('Trying to send message from an uninitialised socket ...');
                return;
            end
            
            try
                fwrite(this.tcpSocket,binaryData) % TODO -  possible blocking here..
            catch e
                this.error('Socket sending error encountered..');
            end
            this.log('Message sent successfully...');
        end
        
        function [data , numMessages] = readMessages (this)
            % readMessage: this tries to read a message from the server. It will
            % return each read message as a vector and the number of read messages... TODO - expand on this when better
            % known
            % Usage : [data , numMessages] = this.readMessages ()
            % Author : Lindsay , Date : 24/03/2015
            numMessages=0;
            data=[];
            if this.socketState ~= 1 % socket has not been initialised properly
                this.error('Trying to read message from an uninitialised socket ...');
                return;
            end
            
            % Try to read some stuff from the socket...
            this.log(strcat('Try to read ',this.name, ' message ... '));
            try
                
                if this.tcpSocket.BytesAvailable >=95
                    this.dropped=0; % reset our dropped count to 0...
                    
                    this.log('bytesAvailable!!');
                    bAvail = this.tcpSocket.BytesAvailable;
                    
                    data=uint8(zeros(bAvail,1));
                    data = fread(this.tcpSocket,size(data),'uint8');
                    
                    % Buffer would appear to need to be flushed....
                    numMessages=1; % only ever want to deal with one message...
                    
                    % NB: Might need flush the rest of the socket as we don't wan't to read any
                    % old state messages etc..
                    this.log(strcat('Successfully read ',this.name, ' '));
                else
                    this.dropped=this.dropped+1;
                    if this.dropped > this.maxDropped
                        this.error('15 dropped state messages....comms are lost!');
                        this.dropped=0;
                        
                        % 1. lets close this connection and report the error
                        this.closeConnection();
                        
                        % 2. Let the manager know...
                        this.managerHandle.commsLost();
                    end
                end
                
            catch err
                this.error('Problem reading from socket!!!');
            end
        end
        
        function  addHandles (this,robControllerHandle)
            % addHandles : adds the  robot controller object
            % handle to this object such that we can call methods in those
            % objects.
            % Usage : movecomms.addHandles(guiHandle,robControllerHandle);
            % Author : Lindsay
            % Date : 24/03/2015
            this.robContHandle=robControllerHandle;
        end
        
        %% Error Checking functions
        function checkSocketState (this)
            % Function is deprecated!!!..but left here for compatibility reasons.
        end;
        
    end
    
    methods (Access = protected)
        % Only Subclasses can see and use these methods...
    end
    
end

