%% This class is scoped for deprecation!!!
%% DO NOT USE!!!!

classdef dummyServer < Process
    % dummyServer : This class is simply used to create a basic dummyServer
    % which can be used to emulate a robot...to some extent...
    % 
    % Usage : TODO ...
    %
    % Author : Lindsay 
    % Date : 24/03/2015
    
    %% Properties
    properties (Access = public)
        % Everyone can see and modify these properties
        % NB: there is a way so that people can read properties but not
        % write to the properties
    end
    
    %% Methods
    methods (Access = public)
        % Implement Abstract MEthods here...
        
        function this = dummyServer(this,port)
           this.robotIpAddress = '0.0.0.0';
           this.robotPort=port;
        end
        
        function setCommsMode(this,mode)
            % do nothing
        end
        
        function this = dummyServer()
            % Constructor : constructs our vision object...
            % Usage : it should be called whenever the default constructor is
            % called
            this.name='SIOComms';
            
            % Try to initialise our SIO Comms task
            this.setRegularTask(1,1.2,0.2);
            % NB: the connection must be intialised....
            
            % Maybe not so goodto do it here but oh well......but lets try and initialise our
            % connection
            this.tcpSocket = tcpip('0.0.0.0', 30000, 'NetworkRole', 'server');
            fopen(this.tcpSocket);
            % TODO .. some error checking here perhaps...
        end
        
        function out = mainTask (this,timerObj,event,string_arg)
        % Main task.. just read a message and echo it back...for now...
            data = fread(this.tcpSocket,this.tcpSocket.BytesAvailable);
            fwrite(this.tcpSocket,data); % echo the data back ...
        end
        
    end
    
    methods (Access = protected)
        
    end
    
end

