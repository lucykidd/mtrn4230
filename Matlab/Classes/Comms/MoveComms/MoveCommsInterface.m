classdef (Abstract) MoveCommsInterface < Comms
    % SIOCommsInterface : This class is used to handle peridoically recieving state & IO messages from
    % the robot and also handles sending IO messages to the robot.
    %
    % Usage : TODO
    %
    % Author : Lindsay
    % Date : 24/03/2015
    % NB: This extends the handle superclass which therein enables 
    
    properties (Access = protected)
        robControllerHandle; % Handle to our robot controller object
        header;
    end
    
    methods (Abstract)
    % Note : All Methods here are just definitions
    % Do not use a function...end block to define an abstract method, use only the method signature.
         
        setCommsMode(this,mode)
         % Set the comms mode - todo expand on this...
         % Author : Lindsay
         % Date : 24/03/15
         
        moveJPos(this,joint1,joint2,joint3,joint4,joint5,joint6)
        % moveJPos : Move the robot to the specified joint position at the designated
        % speed.
        % Usage: moveJPos(j1,j2,j3,j4,j5,j6)
        % where joints are joint angles and speed is a double between 0 and
        % 1.0 (1.0 is fastest and 0 is slowest...)
        % Author : Lindsay
        % Date : 24/03/15
        
        moveCartPos(this,x,y,z,vertical)
        % Move the robot in the cartesian position
        % x,y,z coordinates w.r.t the base/robot coordinate system (confirm
        % this)
        % vertical is a boolean, if set to 1, the tool will be pointing
        % down
        % Author : Lindsay
        % Date : 24/03/15
        
        stop(this)
        % Stop the robot from moving
        % Author : Lindsay
        % Date : 24/03/15
        
        resume(this)
        % Resume any current movement that the robot was doing...
        % Author : Lindsay
        % Date : 24/03/15
       
        setSpeed(this,speed)
        % set the speed of the robot
        % input speed = [v_tcp, v_ori, v_leax, v_reax]
        % v_tcp is speed of tool mm/s , v_ori is angular speed of tool
        % degrees/s, v_leax is speed for linear external axes mm/s, v_reax
        % is angular speed for external axes degrees/s
        % Author : Patrick
        % Date : 24/03/15
    
        setLinearMode(this,LinearMode)
        % setLinearMode : Sets LinearMode on the robot to the value given
        % in LinearMode variable
        % Usage: setLinearMode(LinearMode)
        % where LinearMode is a boolean value, 1 for true and 0 for false
        % Author : Lindsay
        % Date : 24/03/15
        
        setXYZMode(this,XYZMode)
        % setXYZMode : Sets XYZMode on the robot to the value given in
        % XYZMode variable.
        % Usage: setXYZMode(XYZMode)
        % where XYZMode is a boolean value, 1 for true and 0 for false
        % Author : Lindsay
        % Date : 24/03/15
        
        moveCartQuatPos(this,x,y,z, Quaternions)
        % Move the robot in the cartesian position
        % x,y,z coordinates w.r.t the base of the robot and sets
        % quaternions.
        % Quaternions is [q1, q2, q3, q4] with norm 1.
        % Author: Patrick

        
    end

end

