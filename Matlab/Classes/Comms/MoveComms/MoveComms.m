% MOVECOMMS PROTOCOL...
% MOVECOMMS only ever sends to the robot...
% PROTOCOL is big endian..
% [ HEADER ID Data] --> where data is a set of floats..
% Stop & Resume : []
% moveJPos -- > data is : [joint1,joint2,joint3,joint4,joint5,joint6]
% setLinearMode --> [LinearMode]
% setXYZMode --> [XYZMode]
% setCommsMode -->  data is  [mode]
% moveCartPos [x,y,z,vertical]
% setIOState [IOVac,IOConRun,IOConDir,IOvacSol]
% setSpeed [speed]
% sendHeartBeat []

classdef MoveComms < MoveCommsInterface
    % MoveComms : This class is used to handle sending
    % movement commands to the robot (by periodically checking the robot
    % state and from initiation). It also handles recieving movement status
    % from the robot ??? (Bit vague here....)
    %
    % Usage : Create a movecomms object, connect this object to the
    % required
    %
    % Author : Lindsay
    % Date : 24/03/2015
    
    %% Properties
    properties (Access = public)
        % Everyone can see and modify these properties
        % NB: there is a way so that people can read properties but not write
    end
    
    properties (Access = private)
        % Only this class can access these properties
        counter; % a simple counter for the number of times that main has been called...
    end
    
    %% Public Methods
    methods (Access = public)
        % Implement Abstract methods here..
        
        function this = MoveComms()
            % Constructor : constructs our vision object...
            % Usage : it should be called whenever the default constructor is
            % called
            % Author : Lindsay
            % Date : 24/03/15
            
            this.robotPort=1026; % default value
            this.robotIpAddress='127.0.0.1';%default value..
            this.name='MoveComms';
            this.header = 12345.123; % Important..do not delete/modify...
            %this.setRegularTask(1,0.0,5.0); % simple task to try enable reconnection every 5 seconds if possible..
            this.setRegularTask(1,0.0,0.4); % 2.5 HZ heartbeat rate, 4 missed == paused motion, 12 missed == socket lost 5s socket reactivation rate..
            this.counter=11; % start counter at a high value to try and init the socket...
        end
        
        function setCommsMode(this,mode)
            % setCommsMode : this sets the comms object to recieve data from
            % robot if mode=1 , robotStudio if mode =0
            % server/client if mode=2
            % Usage: this.setCommsMode (mode)
            % Author : Lindsay
            % Date : 24/03/15
            
            this.commsMode=mode;
            if this.commsMode ==1 % Set to robot mode
                this.log('Set Comms mode to robot mode....');
                this.robotPort=1026; % default value
                this.robotIpAddress='192.168.2.1';%default value..
            else
                this.log('Set comms mode to simulation mode on localhost');
                this.robotPort=1026; % default value
                this.robotIpAddress='127.0.0.1';%default value..
            end
        end
        
        function moveConveyor(this,time)
            % moveConveyor: will move the conveyor belt a specified
            % time. If time is positive, it's towards the table, if it's negative, away from the table
            % Usage: movecomms.moveConveyor(5) , where 5 is 5 seconds...
            % Author : Lindsay & Pat
            % Date : 16/05/15
            
            % send the time that the conveyor is to be active for...
            % this time is presumed to be directly related to distance
            % travelled...
            byteVector = this.createMessage(this.header,14.0,[time]);
            this.sendData(byteVector);
            this.consoleLog('Send a new conveyor displacement message', time);
        end
        
        function moveJPos(this,joint1,joint2,joint3,joint4,joint5,joint6)
            % moveJPos : Move the robot to the specified joint position at the designated
            % speed.
            % Usage: moveJPos(j1,j2,j3,j4,j5,j6)
            % where joints are joint angles and speed is a double between 0 and
            % 1.0 (1.0 is fastest and 0 is slowest...)
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Create the message based on joints
            byteVector = this.createMessage(this.header,1.0,[joint1,joint2,joint3,joint4,joint5,joint6]);
            
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send New Joint Motion Command');
        end
        
        function setLinearMode(this,LinearMode)
            % setLinearMode : Sets LinearMode on the robot to the value given
            % in LinearMode variable
            % Usage: setLinearMode(LinearMode)
            % where LinearMode is a boolean value, 1 for true and 0 for false
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Create the message based on joints
            byteVector = this.createMessage(this.header,6.0,[LinearMode]);
            
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send New Linear Motion Command');
        end
        
        function setXYZMode(this,XYZMode)
            % setXYZMode : Sets XYZMode on the robot to the value given in
            % XYZMode variable.
            % Usage: setXYZMode(XYZMode)
            % where XYZMode is a boolean value, 1 for true and 0 for false
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Create the message based on joints
            byteVector = this.createMessage(this.header,7.0,[XYZMode]);
            
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send XYZMode Motion Command');
        end
        
        function moveCartPos(this,x,y,z,vertical)
            % moveCartPos : Move the robot in the cartesian position
            % x,y,z coordinates w.r.t the base of the robot
            % vertical defines whether the tool will be vertical (true) or
            % keeping the orientation defined in the current Quaternions
            % Usage : movecomms.moveCartPos(x,y,z,vertical)
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Create the message based on joints
            byteVector = this.createMessage(this.header,2.0,[x,y,z,vertical]);
            
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send Move Robot In Cartesian Command');
        end
        
        function moveCartQuatPos(this,x,y,z, Quaternions)
            % moveCartQuatPos : Move the robot in the cartesian position
            % x,y,z coordinates w.r.t the base of the robot
            % Quaternions is [q1, q2, q3, q4] with norm 1.
            % Usage: movecomms.moveCartQuatPos(x,y,z, Quaternions)
            % Author: Patrick
            % Date : 21/05/15
            
            % 1. Create the message based on joints
            byteVector = this.createMessage(this.header,9.0,[x,y,z, Quaternions(1:4)]);
            
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send Move Robot In Cartesian Command');
        end
        
        function stop(this)
            % Stop the robot from moving
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Create the message based on joints
            byteVector = this.createMessage(this.header,3.0,[]);
            
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send Stop Motion Command!');
        end
        
        function resume(this)
            % resume : resume any current movement that the robot was doing...
            % NB: a new movement command will also resume motion
            % automatically..
            % Usage : this.resume()
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Create the message based on joints
            byteVector = this.createMessage(this.header,4.0,[]);
            
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send Resume Motion Command');
        end
        
        function setIOState (this,IOVector)
            % setIOState: Called to set the iostate of the robot (send an iostate
            % message to the robot....
            % IOVector = [ConRun, ConDir, VacSol, VacPump] 0 or 1 values in that
            % order
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Create the message based on joints
            byteVector = this.createMessage(this.header,5.0,IOVector);
            
            this.log('Set the IOState of robot',IOVector);
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send IOChange Command');
        end
        
        function setSpeed(this,speed)
            % set the speed of the robot
            % speed = [v_tcp, v_ori, v_leax, v_reax]
            % v_tcp is speed of tool mm/s , v_ori is angular speed of tool
            % degrees/s, v_leax is speed for linear external axes mm/s, v_reax
            % is angular speed for external axes degrees/s
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Create the message
            byteVector = this.createMessage(this.header,8.0,speed);
            
            % 2. Send the message to the server...
            this.sendData(byteVector);
            this.consoleLog('Send setSpeed Command ');
        end
        
        function sendHeartBeat (this)
            % This function will send a short heartbeat message to confirm to the robot that it is still connected...
            % Heartbeat is used to indicate to the robot if a socket has
            % gone stale...
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. create the message...
            byteVector = this.createMessage(this.header,12.0,[]);
            
            % 2. send the message to the robot..
            this.sendData(byteVector);
            this.log('Send a heartbeat message');
        end
        
        function moveChocolate(this,ChocPos, ChocTarget,theta)
            % This function sends the robot to get a chocolate in ChocPos
            % and brings it to ChocTarget).
            % ChocPos = [x,y,z],robot frame, z the height of table +
            % chocolate or conveyor + chocolate
            % ChocTarget = [x,y,z],robot frame, z the height of target +
            % chocolate. If this is not on a solid, the chocolate falls
            % down
            % theta is the rotation between position 1 and 2.
            % Author: Pat
            % Date : 21/05/15
            
            byteVector = this.createMessage(this.header,10.0, [ChocPos,ChocTarget,theta]);
            
            this.sendData(byteVector);
            this.log('sent to a chocolate');
        end
        
        function out = mainTask (this,timerObj,event,string_arg)
            % SIO Comms Main Task
            % This is where the periodic code for this object is executed..
            % I.e it is where we try to initialise our connection and send
            % heartbeat messages ...
            % Author : Lindsay
            % Date : 24/03/15
            
            try
                if this.counter > 12 && this.socketState ~= 1
                    % we want to try and intialise the socket again...
                    this.initConnection(); % try to initialise a connection...
                    this.counter=0;
                elseif this.socketState == 1
                    % we want to send a heartbeat message
                    this.sendHeartBeat();
                end
                this.counter=this.counter+1;
            catch e
                this.error('Something is throwing in our main task for', this.name);
                this.error(getReport(e));
            end
        end
    end
    
    methods (Access = protected)
        % Only Subclasses can see and use these methods...
        
        function byteVector = createMessage(this,header,ID,floatVector)
            % Will create an array of bytes (uint8) data to be sent
            % to the server...
            % Author : Lindsay
            % Date: 24/03/15
            
            % Make everything a single == float32
            header=single(header);
            ID=single(ID);
            floatVector=single(floatVector);
            
            % Fill in out byteVector
            byteVector = zeros(1,8+(4*(length(floatVector))));
            byteVector = uint8(byteVector); % make sure it is uint8!!
            byteVector(1:4) = typecast(header,'uint8');
            byteVector(5:8) = typecast(ID,'uint8');
            
            % fill in the rest of the vector with float data..
            j=1;
            for i = 9:4:length(byteVector)
                byteVector(i:i+3) = typecast(floatVector(j),'uint8');
                j=j+1;
            end
            
        end;
        
    end
    
    methods (Access = private)
        % Only this class can seee and use these methods...
    end
    
    
end

