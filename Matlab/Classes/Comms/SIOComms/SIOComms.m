
% COMMS PROTOCOL....
% USING TCP/IP PROTOCOl
% FOR Siocomms this is only a reading socket on matlab, and hance
% only recieves state updates from the robot. State protocol..
% [Header,X,Y,Z,q1,q2,q3,q4,j1,j2,j3,j4,j5,j6,IOVac,IOCon,IOSol
% ,IOCondir,desiredX,desiredY,desiredZ,desiredq1,desiredq2,desiredq3,desiredq4,desiredJ1,desiredJ2,desiredJ3,desiredJ4,desiredJ5,error,IConstat]
% NB: all values above are floats and are sent and processed as bytes...

classdef SIOComms < SIOCommsInterface
    % SIOComms : This class is used to handle peridoically recieving state & IO messages from
    % the robot and also handles sending IO messages to the robot.
    %
    % Usage : TODO ...
    %
    % Author : Lindsay
    % Date : 24/03/2015
    
    %% Properties
    properties (Access = public)
        % Everyone can see and modify these properties
        % NB: there is a way so that people can read properties but not
        % write to the properties
    end
    
    properties (Access = protected)
        % Only sublclasses can access these properties
    end
    
    properties (Access = private)
        % Only this class can access these properties
        commsLostCount;
        retryCount;
    end
    
    
    %% Methods
    methods (Access = public)
        % Implement Abstract MEthods here...
        
        function setCommsMode(this,mode)
            % setCommsMode : this sets the comms object to recieve data from
            % robot if mode=0 , robotStudio if mode =1 and a dummy
            % server/client if mode=2
            % Author : Lindsay
            % Date : 24/03/15
            
            this.commsMode=mode;
            % This needs to be overriden in SIOComms and MoveComms...
            
            if this.commsMode == 1 % Set to robot mode
                this.log('Set Comms mode to robot mode....');
                this.robotPort=1025; % default value
                this.robotIpAddress='192.168.2.1';%default value..
            else
                this.log('Set comms mode to simulation mode on localhost');
                this.robotPort=1025; % default value
                this.robotIpAddress='127.0.0.1';%default value..
            end
            
        end
        
        function setIOState (this,IOVector)
            % setIOState: Called to set the iostate of the robot (send an iostate
            % message to the robot....
            % IOVector = [ConRun, ConDir, VacSol, VacPump] 0 or 1 values in that
            % order
            % Author : Lindsay
            % Date : 24/03/15
            
            this.log('deprecated functionality...');
        end
        
        function this = SIOComms()
            % Constructor : constructs our vision object...
            % Usage : it should be called whenever the default constructor is
            % called
            % Author : Lindsay
            % Date : 24/03/15
            
            this.name='SIOComms';
            this.commsLostCount=0;
            this.retryCount=12;
            
            % Set our SIO Comms task
            this.setRegularTask(1,0.3,0.4);
            % NB: the connection must be still be intialised.... for task
            % to begin...
        end
        
        function out = mainTask (this,timerObj,event,string_arg)
            % SIO Comms Main Task... simple reading messages periodically...
            % this is where siocomms tries to periodically read messages
            % etc....
            % Author : Lindsay
            % Date : 24/03/15
            
            try
                
                if this.socketState ~= 1
                    % we want to try and intialise the socket again...
                    if this.commsLostCount > this.retryCount
                        this.initConnection(); % try to initialise a connection...
                        this.commsLostCount=0;
                    else
                        this.commsLostCount=this.commsLostCount+1;
                        % just increment our counter...
                    end
                else
                    % Lets read in a message from our socket
                    [data, numMessages] = this.readMessages(); % Just try and read messages...
                    
                    % Update robot state if valid message...
                    if numMessages > 0
                        vec = this.dataToVector(data);
                        %this.consoleLog('Recieved State Update Message');
                        this.log('State update message , Data:',vec);
                        this.robContHandle.updateRobotPose(vec); %update the robot pose...
                    end
                end;
                
            catch e
                this.error('Something is throwing in our main task for', this.name);
                this.error(getReport(e));
            end
        end
    end
    
    methods (Access = protected)
        
        function vector = dataToVector (this,data)
            % This function will take a binary data vector and then convert it to
            % a float vector to pass to the robot controller...
            % Author : Lindsay
            % Date : 24/03/15
            
            % 1. Find the message header
            vector=zeros(1,30);
            header1 = 203;%CB
            header2 = 248;%F8
            header3 = 121;%79
            header4 = 44;%2C
            found=0;
            
            numBytes=length(data);
            data = uint8(data);
            
            % 2. Find the data header...
            for i=1:numBytes-3
                if data(i) == header1 && data (i+1) == header2 && data(i+2) == header3 && data(i+3) == header4
                    found =i+4;
                    break;
                end;
            end
            
            % 3. Fill in the data vector...
            if found > 0 && (numBytes-found >= 0)% We have found a valid heading...
                count=1;
                for i=found:4:found+116 % number of bytes to be read
                    vector(count) = typecast(data(i:i+3),'single'); % append data to the vector...
                    count=count+1;
                end
            end
        end
        
    end
    
    methods (Access = private)
        % Only this class can seee and use these methods...
    end
    
end

