classdef SIOCommsInterface < Comms
    % SIOCommsInterface : This class is used to handle peridoically recieving state & IO messages from
    % the robot and also handles sending IO messages to the robot.
    %
    % Author : Lindsay
    % Date : 24/03/2015
    % NB: This extends the handle superclass which therein enables 
    
    properties (Access = protected)
        robControllerHandle; % Handle to our robot controller object
    end
    
    methods (Abstract)
    % Note : All Methods here are just definitions
    % Do not use a function...end block to define an abstract method, use only the method signature.
    
    setCommsMode(this,mode)
    % setCommsMode: set this comms object to either communicate with the
    % robot or the simulator....
    % Author : Lindsay
    % Date : 24/03/15
    
    setIOState (this,IOVector)
    % setIOState: Called to set the iostate of the robot (send an iostate
    % message to the robot....
    % IOVector = [ConRun, ConDir, VacSol, VacPump] 0 or 1 values in that
    % order
    % Author : Lindsay
    % Date : 24/03/15
    
    end

end

