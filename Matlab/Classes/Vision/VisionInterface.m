classdef (Abstract) VisionInterface < Process
    % VisionInterface : The vision class does all of our vision work. Basically it should
    % periodically retrieve images , do some processing and then update the
    % robot system state (with known chocolate locations etc..) and also
    % update the image in our gui. We can prompt this function or let it do
    % it by itself with a timer....
    %
    % Usage : TODO
    %
    % Author : Lindsay
    % Date : 24/03/2015
    % NB: This extends the handle superclass which therein enables 
    
    properties (Access = protected)
        guiHandle; % Handle to our gui object
        robControllerHandle; % Handle to our robot controller object
    end
    
    methods (Abstract)
    % Note : All Methods here are just definitions
    % Do not use a function...end block to define an abstract method, use only the method signature.
    
    addHandles (this,guiHandle,robControllerHandle)
    % addHandles : adds the gui object handle and robot controller object
    % handle to this vision object such that we can call methods in those
    % objects.
    % Usage : vision.addHandles(guiHandle,robControllerHandle);
    % Author : Lindsay
    % Date : 24/03/2015

    end
    
    % !!!! Please do not add any concrete methods here... do that in the
end

