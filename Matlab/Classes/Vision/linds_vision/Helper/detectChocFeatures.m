function [ chocPoints,chocFeatures ] = detectChocFeatures(  chocImage, crop)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
chocImage = rgb2gray(chocImage);
%chocPoints = detectSURFFeatures(chocImage,'MetricThreshold',700,'NumOctaves',30,'NumScaleLevels',60,'ROI',[1,250,299,434]);

% Try to determine the midway point on the chocolate...

if crop == 1 % do a cropping operation to get focus points...
    sz= size(chocImage)
    midpoint = 0.4* sz(1)
    width = sz(2) -1
    roi = [uint8(1),uint8(midpoint),uint8(width) , uint8(sz(1) - midpoint) ]
    chocPoints = detectSURFFeatures(chocImage,'MetricThreshold',700,'NumOctaves',30,'NumScaleLevels',60,'ROI',roi);
    
else
    chocPoints = detectSURFFeatures(chocImage,'MetricThreshold',700,'NumOctaves',30,'NumScaleLevels',60);
end


figure();
imshow(chocImage);
hold on;
plot(chocPoints.Location(:,1),chocPoints.Location(:,2),'r*');
[chocFeatures , chocPoints] = extractFeatures(chocImage,chocPoints,'SURFSize',64);
end

