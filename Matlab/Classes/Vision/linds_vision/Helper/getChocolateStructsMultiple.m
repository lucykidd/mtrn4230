% Simple Script to compute all feature structs...

uniqThresh2 = 210;
uniqThresh = 75;

% Read in our chocolate images...
%darkBack=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\medium_res_back_dark_3.jpg');
milkBack=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\medium_res_back_blue.jpg');

% compute our chocolate structs
milkStruct =  compCombinedFeatureStructs('milk_front','png','linds_set_marker.txt',4,uniqThresh)
mintStruct =  compCombinedFeatureStructs('mint_front','png','linds_set_marker.txt',4,uniqThresh)
darkStruct =  compCombinedFeatureStructs('dark_front','png','linds_set_marker.txt',5,uniqThresh)
orangeStruct =  compCombinedFeatureStructs('orange_front','png','linds_set_marker.txt',4,uniqThresh)

darkBackStruct =  compCombinedFeatureStructs('dark_back','png','linds_set_marker.txt',10,uniqThresh)
%darkBackStruct = compFeatureStructs(darkBack,uniqThresh2);
milkBackStruct = compFeatureStructs(milkBack,uniqThresh2);
%darkStruct = compFeatureStructs(dark,uniqThresh2);

% save the chocolate structs...
save('linds_features.mat','milkStruct','mintStruct','darkStruct','orangeStruct','darkBackStruct','milkBackStruct');

