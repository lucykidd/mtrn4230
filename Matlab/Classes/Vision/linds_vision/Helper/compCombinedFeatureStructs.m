function [ featureStruct ] = compCombinedFeatureStructs(folderName,type,fileMarker,numChocs,uniqThresh)
% A Function that is run once, and computes a detailed feature struct for
% a template image based on several templates...

% chocolates : an array of chocolate images
% sizes : a list of [x,y] sizes of the chocolate in this image.... (right
% now assumed to be the same...)

% find the path name
path = which(fileMarker)
path = strrep(path,fileMarker,'');
chocolate = imread(strcat(path,folderName,'\1.',type));
chocolate=rgb2gray(chocolate);
chocolateRef = chocolate;

% Common variables
numScaleLevels=100;
metricThreshold = 150; % 400 is pretty good...
strongestNum = 20;
numOctaves = 50;

% get the first chocolate...
[all_choc ,uniq_choc,f_all_choc, f_uniq_choc] = detectAndPlot(chocolate,numScaleLevels,metricThreshold,numOctaves,strongestNum,'chocolate points',uniqThresh);
featureStruct.allPoints=all_choc;
featureStruct.uniqPoints = uniq_choc;
featureStruct.allFeatures = f_all_choc;
featureStruct.uniqFeatures = f_uniq_choc;
featureStruct.image=chocolate;

for i=2:numChocs

    chocolate = imread(strcat(path,folderName,'\',num2str(i),'.',type));
    chocolate=rgb2gray(chocolate);
    
    % 1. find all the points...
    [all_choc ,uniq_choc,f_all_choc, f_uniq_choc] = detectAndPlot(chocolate,numScaleLevels,metricThreshold,numOctaves,strongestNum,'chocolate points',uniqThresh);
    
    % 2. Match the features to the chocPoints and features...
    %[matchesUniq,matchMetricUnique]= matchFeatures(f_uniq_choc,featureStruct.uniqFeatures,'MatchThreshold',11.00,'MaxRatio',0.6,'Unique',true); % normal chocolate detection...
    %[matchesAll,matchMetricAll]= matchFeatures(f_all_choc,featureStruct.allFeatures,'MatchThreshold',11.00,'MaxRatio',0.6,'Unique',true); % normal chocolate detection...
    
    % 3. remove these matched features from our points...
    % TODO...
    
    % 4. Append/augment the featurepoint and features vector...
    % with the new stuff...
    %fprintf('Number of matches from %d found is %d\n',i,length(matchMetricAll));
    featureStruct.allPoints=vertcat(featureStruct.allPoints, all_choc);
    featureStruct.uniqPoints = vertcat(featureStruct.uniqPoints ,uniq_choc);
    featureStruct.allFeatures = [ featureStruct.allFeatures; f_all_choc];
    featureStruct.uniqFeatures = [ featureStruct.uniqFeatures; f_uniq_choc];
    
end

% show all of our features...
     strongest=selectStrongest(featureStruct.allPoints,strongestNum);    
     figure();
     imshow(chocolateRef);
     hold on;
     plot(featureStruct.allPoints.Location(:,1),featureStruct.allPoints.Location(:,2),'b*');
     
     k=size(chocolateRef);
     plot([0,k(1)-1],[uniqThresh,uniqThresh],'g');
     plot(featureStruct.uniqPoints.Location(:,1),featureStruct.uniqPoints.Location(:,2),'go');
     
     plot(strongest.Location(:,1),strongest.Location(:,2),'rx');
     title('All Chocolates...');
     hold off;
end

function [points,uniqPoints,features,uniqFeatures ] = detectAndPlot(image,numScaleLevels,metricThreshold,numOctaves,strongestNum,description,uniqThresh)
    
    % Detect Surf Features and plot stuff...
    %points = detectSURFFeatures(image,'NumScaleLevels',numScaleLevels,'MetricThreshold',metricThreshold,'NumOctaves',numOctaves);
    
    points = detectSURFFeatures(image,'NumScaleLevels',numScaleLevels,'MetricThreshold',metricThreshold,'NumOctaves',numOctaves);
    %points = detectHarrisFeatures(image);
    
    % 1. Determine the unique points Naively state that unique points are those constrained to the
    % chocolate emblem only...
    uniqPoints=points;
    i=1;
    while i <= length(uniqPoints)
        if uniqPoints.Location(i,2) > uniqThresh
            uniqPoints(i,:)=[];
        else
            i=i+1;
        end
    end
    
    % Show some stuff...
%     strongest=selectStrongest(points,strongestNum);
%     figure();
%     imshow(image);
%     hold on;
%     %plot(selectStrongest(milkPoints,20));
%     plot(points.Location(:,1),points.Location(:,2),'b*');
%     
%     k=size(image);
%     plot([0,k(1)-1],[uniqThresh,uniqThresh],'g');
%     plot(uniqPoints.Location(:,1),uniqPoints.Location(:,2),'go');
%     
%     plot(strongest.Location(:,1),strongest.Location(:,2),'rx');
%     title(description);
%     hold off;
%     
    [features points] = extractFeatures(image,points);%,'SURFSize',128);
    [uniqFeatures uniqPoints] = extractFeatures(image,uniqPoints);%,'SURFSize',128);
    
end


