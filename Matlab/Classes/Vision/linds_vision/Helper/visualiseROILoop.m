function [mask,ROI] = visualiseROILoop(mask,ROI,name)
    imshow(mask);
    hold on;
    title(name);
    plotROIPoints(ROI,'b');
    hold off;
end
