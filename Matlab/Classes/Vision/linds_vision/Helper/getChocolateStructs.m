% Simple Script to compute all feature structs...

uniqThresh2 = 210;
uniqThresh = 75;

% Read in our chocolate images...
%milk=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\High\milk_300.png');
%mint=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\High\mint_300.png');
%dark=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\High\dark_300.png');
%orange=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\High\orange_300.png');

milk=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\choc_blue_low_1.jpg');
mint=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\Choc_mint_1.png');
dark=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\Choc_dark_1.png');
orange=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\Choc_orange_3.png');
darkBack=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\medium_res_back_dark_3.jpg');
milkBack=imread('student_submissions\z3288764_MTRN4230_ASST1\ChocolateSamples\medium_res_back_blue.jpg');

% compute our chocolate structs
milkStruct = compFeatureStructs(milk,uniqThresh);
mintStruct = compFeatureStructs(mint,uniqThresh);
darkStruct = compFeatureStructs(dark,uniqThresh);
orangeStruct = compFeatureStructs(orange,uniqThresh);
darkBackStruct = compFeatureStructs(darkBack,uniqThresh2);
milkBackStruct = compFeatureStructs(milkBack,uniqThresh2);
%darkStruct = compFeatureStructs(dark,uniqThresh2);

% save the chocolate structs...
save('linds_features.mat','milkStruct','mintStruct','darkStruct','orangeStruct','darkBackStruct','milkBackStruct');

