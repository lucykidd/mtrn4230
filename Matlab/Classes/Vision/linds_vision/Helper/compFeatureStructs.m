function [ featureStruct ] = compFeatureStructs(chocolate,uniqThresh)
% A Function that is run once, and computes a detailed feature struct for
% a template image based on several templates...

% chocolates : an array of chocolate images
% sizes : a list of [x,y] sizes of the chocolate in this image.... (right
% now assumed to be the same...)

chocolate=rgb2gray(chocolate);

% Common variables
numScaleLevels=100;
metricThreshold = 750;
strongestNum = 20;
numOctaves = 50;
%uniqThresh = 75;

% This function aims to determine the maximum amount of features and surf
% points from each individual sample chocolate image by playing with the
% number of scales and other paramters. 

% Lastly it aims to take several slightly different images of the sample
% chocolates, perform a basic alignment and from that convolute all feature
% points to build up a very robust and good descriptor for a given
% chocolate. Hence it can handle multiple images taken at different
% locations and different exposures etc...


% 1. use template matching to align various sample images
% TODO later

% 3. Detect Surf Feature Points and plot some data...
[all_choc ,uniq_choc,f_all_choc, f_uniq_choc] = detectAndPlot(chocolate,numScaleLevels,metricThreshold,numOctaves,strongestNum,'chocolate points',uniqThresh);

% 4. Match each Chocolates feature points...
%TODO if needed
featureStruct.allPoints=all_choc;
featureStruct.uniqPoints = uniq_choc;
featureStruct.allFeatures = f_all_choc;
featureStruct.uniqFeatures = f_uniq_choc;
featureStruct.image=chocolate;

% 5. Show Correlations...
% TODO if needed...

% 6. Compute unique chocolate surfpoint vectors and corresponding feature
% points...

end

function [points,uniqPoints,features,uniqFeatures ] = detectAndPlot(image,numScaleLevels,metricThreshold,numOctaves,strongestNum,description,uniqThresh)
    
    % Detect Surf Features and plot stuff...
    %points = detectSURFFeatures(image,'NumScaleLevels',numScaleLevels,'MetricThreshold',metricThreshold,'NumOctaves',numOctaves);
    
    points = detectSURFFeatures(image,'NumScaleLevels',numScaleLevels,'MetricThreshold',metricThreshold,'NumOctaves',numOctaves);
    %points = detectHarrisFeatures(image);
    
    % 1. Determine the unique points Naively state that unique points are those constrained to the
    % chocolate emblem only...
    uniqPoints=points;
    i=1;
    while i <= length(uniqPoints)
        if uniqPoints.Location(i,2) > uniqThresh
            uniqPoints(i,:)=[];
        else
            i=i+1;
        end
    end
    
    % Show some stuff...
    strongest=selectStrongest(points,strongestNum);
    figure();
    imshow(image);
    hold on;
    %plot(selectStrongest(milkPoints,20));
    plot(points.Location(:,1),points.Location(:,2),'b*');
    
    k=size(image);
    plot([0,k(1)-1],[uniqThresh,uniqThresh],'g');
    plot(uniqPoints.Location(:,1),uniqPoints.Location(:,2),'go');
    
    plot(strongest.Location(:,1),strongest.Location(:,2),'rx');
    title(description);
    hold off;
    
    [features points] = extractFeatures(image,points);%,'SURFSize',128);
    [uniqFeatures uniqPoints] = extractFeatures(image,uniqPoints);%,'SURFSize',128);
    
end


