% Simple script to read in all required images into this workspace...
clear all hidden;
close all hidden;

% Scene Images....
s1_mixed = imread('Images\IMG_005.jpg');
s2_mixed = imread('Images\IMG_009.jpg');
s3_mixed = imread('Images\IMG_013.jpg');
s4_mixed = imread('Images\IMG_051.jpg');
s5_mixed = imread('Images\IMG_021.jpg');
s6_mixed = imread('Images\IMG_025.jpg');
s7_mixed = imread('Images\IMG_091.jpg');

s1_blue = imread('Images\IMG_011.jpg');

s1_back = imread('Images\IMG_015.jpg');
s2_back = imread('Images\IMG_041.jpg');

s1_occ = imread('Images\IMG_087.jpg');
s2_occ = imread('Images\IMG_095.jpg');
s3_occ = imread('Images\IMG_119.jpg');
s4_occ = imread('Images\IMG_151.jpg');

% Chocolate images
blue_low_1 = imread('ChocolateSamples\choc_blue_low_1.jpg');
orange_low_1 = imread('ChocolateSamples\Choc_orange_1.png');
mint_low_1 = imread('ChocolateSamples\Choc_mint_1.png');
dark_low_1 = imread('ChocolateSamples\Choc_dark_1.png');

dark_back_1 = imread('ChocolateSamples\choc_dark_back_low_1.jpg');
blue_back_1 = imread('ChocolateSamples\choc_blue_back_low_1.jpg');
blue_back_medium = imread('chocolateSamples\medium_res_back_blue.jpg');
dark_back_medium = imread('chocolateSamples\medium_res_back_dark_3.jpg');
s2_blue_back = imread('Images\IMG_079.jpg');

% mixed front and back
mixed_back_blue = imread('Images\IMG_095.jpg');
back_single_back = imread('Images\back_black_single.jpg');