function [ all_milk,uniq_milk,f_all_milk, f_uniq_milk,all_orange,uniq_orange,f_all_orange, f_uniq_orange,all_dark,uniq_dark,f_all_dark, f_uniq_dark,all_mint,uniq_mint,f_all_mint, f_uniq_mint] = ...
    computeCorrelation(milk_choc,orange_choc,dark_choc,mint_choc)
all_milk=[]; uniq_milk=[]; 
all_orange=[];uniq_orange=[];
all_dark=[];uniq_dark=[];
all_mint=[];uniq_mint=[];

% Common variables
numScaleLevels=50;
metricThreshold = 500.00;
strongestNum = 10;
numOctaves = 50;

% This function aims to determine the maximum amount of features and surf
% points from each individual sample chocolate image by playing with the
% number of scales and other paramters. 

% It also aims to determine completely unique features and common 
% features between each chocolate

% Lastly it aims to take several slightly different images of the sample
% chocolates, perform a basic alignment and from that convolute all feature
% points to build up a very robust and good descriptor for a given
% chocolate. Hence it can handle multiple images taken at different
% locations and different exposures etc...

% 1. use template matching to align various sample images
% TODO later

% 2. Compute and Show feature points for each chocolate
milkGray = rgb2gray(milk_choc);
orangeGray = rgb2gray(orange_choc);
darkGray = rgb2gray(dark_choc);
mintGray = rgb2gray(mint_choc);

% 3. Detect Surf Feature Points and plot some data...
[all_milk ,uniq_milk,f_all_milk, f_uniq_milk] = detectAndPlot(milkGray,numScaleLevels,metricThreshold,numOctaves,strongestNum,'milk choc points');
[all_orange ,uniq_orange,f_all_orange, f_uniq_orange] = detectAndPlot(orangeGray,numScaleLevels,metricThreshold,numOctaves,strongestNum,'orange choc points');
[all_dark ,uniq_dark,f_all_dark, f_uniq_dark] = detectAndPlot(darkGray,numScaleLevels,metricThreshold,numOctaves,strongestNum,'dark choc points');
[all_mint ,uniq_mint,f_all_mint, f_uniq_mint] = detectAndPlot(mintGray,numScaleLevels,metricThreshold,numOctaves,strongestNum,'mint choc points');

% 4. Match each Chocolates feature points...
%TODO if needed
% 5. Show Correlations...
% TODO if needed...

% 6. Compute unique chocolate surfpoint vectors and corresponding feature
% points...

end

function [points,uniqPoints,features,uniqFeatures ] = detectAndPlot(image,numScaleLevels,metricThreshold,numOctaves,strongestNum,description)
    % Detect Surf Features and plot stuff...
    points = detectSURFFeatures(image,'NumScaleLevels',numScaleLevels,'MetricThreshold',metricThreshold,'NumOctaves',numOctaves);
    
    % 1. Determine the unique points Naively state that unique points are those constrained to the
    % chocolate emblem only...
    uniqPoints=points;
    i=1;
    uniqThresh = 74;
    while i <= length(uniqPoints)
        if uniqPoints.Location(i,2) > uniqThresh
            uniqPoints(i,:)=[];
        else
            i=i+1;
        end
    end
    
    % Show some stuff...
    strongest=selectStrongest(points,strongestNum);
    figure();
    imshow(image);
    hold on;
    %plot(selectStrongest(milkPoints,20));
    plot(points.Location(:,1),points.Location(:,2),'b*');
    plot(strongest.Location(:,1),strongest.Location(:,2),'rx');
    k=size(image);
    plot([0,k(1)-1],[uniqThresh,uniqThresh],'g');
    plot(uniqPoints.Location(:,1),uniqPoints.Location(:,2),'go');
    title(description);
    hold off;
    
    [features points] = extractFeatures(image,points,'SURFSize',64);
    [uniqFeatures uniqPoints] = extractFeatures(image,uniqPoints,'SURFSize',64);
end


