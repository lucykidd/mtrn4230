classdef debugDetection < handle
    %Simple class to debug our ROI detection as
    % we go by having figures
    properties
        sceneImage;
        chocType;
        sceneMask;
        figScene;
        figMask;
        figChoc,
        chocImage;
        ROI; % our original ROI...
        chocPoints;
        
        % Scene handle lists
        scenePic;
        cBMask ;
        cSMask;
        cBcene;
        cScene;
        iPScene;
        iPMask;
        pScene;
        pMask;
        iPCMask;
        iPCScene;
        interestPoints;
        interestPoints2;
        
        sceneSURF;
        matchedScene;
        sceneSURF2;
        matchedScene2;

        % boxes...
        boxScene;
        boxMask;
        objectCount;
    end
    
    
    methods
        function this = debugDetection(sceneImage,chocImage,chocBack,sceneMask,chocPoints,chocPointsBack,ROI,chocType)
            this.sceneImage=sceneImage;
            this.chocImage = chocImage;
            this.sceneMask=sceneMask;
            this.ROI=ROI;
            this.chocType=chocType;
            this.chocPoints=chocPoints;
            
            % Create the figures
            this.figScene = figure();
            this.scenePic = imshow(sceneImage);
            hold on;
            this.plotROIPoints(ROI,'b');
            this.cBcene=plot(0,0,'b.');
            this.cScene=plot(0,0,'b.');
            this.pScene=plot(0,0,'b*');
            this.iPScene = plot(0,0,'g*');
            this.iPCScene = plot(0,0,'go');
            this.boxScene = this.initBoxes();
            this.sceneSURF = plot(0,0,'y*');
            this.matchedScene = plot(0,0,'ro');
            this.sceneSURF2 = plot(0,0,'g+');
            this.matchedScene2 = plot(0,0,'yo');
            title(strcat('Scene Image for :',chocType));
            hold off;
            
            this.figMask = figure();
            imshow(sceneMask);
            hold on;
            this.plotROIPoints(ROI,'b');
            this.cBMask=plot(0,0,'b.');
            this.cSMask=plot(0,0,'b.');
            this.pMask = plot(0,0,'b*');
            this.iPMask = plot(0,0,'g*');
            this.iPCMask= plot(0,0,'go');
            this.boxMask = this.initBoxes();
            title(strcat('Scene Mask for :',chocType));
            hold off;
            
            this.figChoc = figure();
            imshow(chocImage);
            hold on;
            title(strcat('Choc Image for :',chocType));
            plot(chocPoints.Location(:,1),chocPoints.Location(:,2),'y*');
            this.interestPoints = plot(0,0,'r*');
            hold off;
            
            % create the back scene image...
            this.figChoc = figure();
            imshow(chocBack);
            hold on;
            title(strcat('Choc Back Image for :',chocType));
            plot(chocPointsBack.Location(:,1),chocPointsBack.Location(:,2),'y*');
            this.interestPoints2 = plot(0,0,'r*');
            hold off;
    
            this.objectCount=0;
        end
        
        function handles = initBoxes(this)
           handles=[];
            for i=1:40
              handles(end+1) = line([0 0 0 0],[ 0 0 0 0],'Color','g'); 
            end
        end
        
        function this = updateROI (this,interestPoint,ROI)
           % Simple method to update our ROI plots in our plots...
           
           %Get circle locations and update
           [xMatrix, yMatrix , xMatrix2,yMatrix2] = this.getCircles(ROI);
           
           set(this.cBcene,'xdata',xMatrix,'ydata',yMatrix);
           set(this.cScene,'xdata',xMatrix2,'ydata',yMatrix2);
           set(this.cBMask,'xdata',xMatrix,'ydata',yMatrix);
           set(this.cSMask,'xdata',xMatrix2,'ydata',yMatrix2);
           
           % update our current points
           set(this.pScene,'xdata',ROI(:,2),'ydata',ROI(:,3));
           set(this.pMask,'xdata',ROI(:,2),'ydata',ROI(:,3));
           
           % update our current interest point
           set(this.iPScene,'xdata',interestPoint(1),'ydata',interestPoint(2));
           set(this.iPMask,'xdata',interestPoint(1),'ydata',interestPoint(2));
           [xMatrix, yMatrix , xMatrix2,yMatrix2] = this.getCircles([0,interestPoint(1),interestPoint(2) ]);
           set(this.iPCScene,'xdata',xMatrix,'ydata',yMatrix);
           set(this.iPCMask,'xdata',xMatrix,'ydata',yMatrix);
        end
        
        function this=updateSceneImage(this,newScene)
           % Will update the displayed scene image...
           set(this.scenePic,'CData',newScene);
        end
        
        function this = drawBox (this,box)
            box(:,1)
            box(:,2)
            disp('Draw the scene box..');
            this.objectCount=this.objectCount+1;
            set(this.boxScene(this.objectCount),'xdata',box(:,1),'ydata',box(:,2));
            set(this.boxMask(this.objectCount),'xdata',box(:,1),'ydata',box(:,2));
        end
        
        function surfPoints(this,surfScene,matchedScene,matchedChoc)
            set(this.interestPoints,'xdata',matchedChoc.Location(:,1),'ydata',matchedChoc.Location(:,2));
            set(this.sceneSURF,'xdata',surfScene.Location(:,1),'ydata',surfScene.Location(:,2));
            set(this.matchedScene,'xdata',matchedScene.Location(:,1),'ydata',matchedScene.Location(:,2));
        end
        
        function surfPoints2(this,surfScene,matchedScene,matchedChoc)
            set(this.interestPoints2,'xdata',matchedChoc.Location(:,1),'ydata',matchedChoc.Location(:,2));
            set(this.sceneSURF2,'xdata',surfScene.Location(:,1),'ydata',surfScene.Location(:,2));
            set(this.matchedScene2,'xdata',matchedScene.Location(:,1),'ydata',matchedScene.Location(:,2));
        end
        
        function [xMatrix, yMatrix , xMatrix2,yMatrix2] = getCircles(this,ROI)
            xMatrix=[];
            yMatrix=[];
            xMatrix2=[];
            yMatrix2=[];
            
            for i=1:length(ROI(:,1))
               [x y] = this.circleParams(ROI(i,2),ROI(i,3),150);
               [x2 y2 ] = this.circleParams(ROI(i,2),ROI(i,3),50);
               xMatrix=[xMatrix;x'];
               yMatrix=[yMatrix;y'];
               xMatrix2=[xMatrix2;x2'];
               yMatrix2=[yMatrix2;y2'];
            end
        end
        
        function plotROIPoints(this,ROI,color)

            for i=1:length(ROI(:,1))
               this.circle(ROI(i,2),ROI(i,3),150,'r');
               this.circle(ROI(i,2),ROI(i,3),50,'r');
               plot(ROI(i,2),ROI(i,3),'r*');
            end
        end
        
        function [xunit,yunit] = circleParams(this,x,y,r)
            th = 0:pi/50:2*pi;
            xunit = r * cos(th) + x;
            yunit = r * sin(th) + y;
        end
        
        function [ h,xunit,yunit] = circle(this,x,y,r,color)
            th = 0:pi/50:2*pi;
            xunit = r * cos(th) + x;
            yunit = r * sin(th) + y;
            h = plot(xunit, yunit,color);
        end
    end
    
end

