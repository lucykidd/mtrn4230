function plotROIPoints(scene,ROI,color)

for i=1:length(ROI)
    
    % plot a rough bounding circle...
    circle(ROI(i).Centroid(1),ROI(i).Centroid(2),150,color);
    circle(ROI(i).Centroid(1),ROI(i).Centroid(2),50,color);
    
    % plot the centroid
    plot(ROI(i).Centroid(1),ROI(i).Centroid(2),strcat(color,'*'));
    
    % plot the convex hull
    plot(ROI(i).ConvexHull(:,1),ROI(i).ConvexHull(:,2),'y');
    
    % plot the x and y major and minor axis...
    % assume they are at the centroid....
    
    xMajorMax = ROI(i).Centroid(1) + cos(ROI(i).Orientation(1)*(pi/180))*ROI(i).MajorAxisLength(1); % NB check does not extend of image..
    xMajorMin = max(ROI(i).Centroid(1) - cos(ROI(i).Orientation(1)*(pi/180))*ROI(i).MajorAxisLength(1),0);
    
    yMajorMax = ROI(i).Centroid(2) + sin(ROI(i).Orientation(1)*(pi/180))*ROI(i).MajorAxisLength(1); % NB check does not extend of image..
    yMajorMin = max(ROI(i).Centroid(2) - sin(ROI(i).Orientation(1)*(pi/180))*ROI(i).MajorAxisLength(1),0);
    
    xMinorMax = ROI(i).Centroid(1) + sin(ROI(i).Orientation(1)*(pi/180))*ROI(i).MinorAxisLength(1); % NB check does not extend of image..
    xMinorMin = max(ROI(i).Centroid(1) - sin(ROI(i).Orientation(1)*(pi/180))*ROI(i).MinorAxisLength(1),0);
    
    yMinorMax = ROI(i).Centroid(2) + cos(ROI(i).Orientation(1)*(pi/180))*ROI(i).MinorAxisLength(1); % NB check does not extend of image..
    yMinorMin = max(ROI(i).Centroid(2) - cos(ROI(i).Orientation(1)*(pi/180))*ROI(i).MinorAxisLength(1),0);
    
    plot([xMinorMin, xMinorMax],[yMinorMin,yMinorMax],'g');
    plot([xMajorMin, xMajorMax],[yMajorMin,yMajorMax],'r');
    
    % Try to link regions together....(i.e big region with a small region
    % etc..)
    
    % Try and detect edges or corner points within the convex hull region...
    %corners = detectHarrisFeatures(image);
    
    %calculate distance of corner from roi...
    
end

end
function [ h,xunit,yunit] = circle(x,y,r,color)
th = 0:pi/50:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;
h = plot(xunit, yunit,color);
end