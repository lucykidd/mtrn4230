function [BW] = darkMaskG(I)
%createMask  Threshold RGB image using auto-generated code from colorThresholder app.
%  [BW,MASKEDRGBIMAGE] = createMask(RGB) thresholds image RGB using
%  auto-generated code from the colorThresholder App. The colorspace and
%  minimum/maximum values for each channel of the colorspace were set in the
%  App and result in a binary mask BW and a composite image maskedRGBImage,
%  which shows the original RGB image values under the mask BW.

% Auto-generated by colorThresholder app on 09-Apr-2015
%------------------------------------------------------

% assume ycrcb color space...

% Define thresholds for channel 1 based on histogram settings
channel1Min = 7.000;
channel1Max = 112.000;

% Define thresholds for channel 2 based on histogram settings
channel2Min = 124.000;
channel2Max = 132.000;

% Define thresholds for channel 3 based on histogram settings
channel3Min = 125.000;
channel3Max = 147.000;

% Create mask based on chosen histogram thresholds
BW = (I(:,:,1) >= channel1Min ) & (I(:,:,1) <= channel1Max) & ...
    (I(:,:,2) >= channel2Min ) & (I(:,:,2) <= channel2Max) & ...
    (I(:,:,3) >= channel3Min ) & (I(:,:,3) <= channel3Max);

