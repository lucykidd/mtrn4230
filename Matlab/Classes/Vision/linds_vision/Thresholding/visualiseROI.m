function [mask,ROI] = visualiseROI(mask,ROI,name)

    figure();
    imshow(mask);
    hold on;
    title(name);
    plotROIPoints(mask,ROI,'b');
    hold off;
end
