 function [milkROI,milkM,mintROI,mintM,orangeROI,orangeM,blackROI,blackM] = compColorROI(image)
% This function takes an image and computes regions of interest for each
% type of chocolate

% NB : could replace with with background subtraction and colour histogram
% to speed it up potentially... (i.e only one step...) etc...

% 1. compute masks
% NB - could speed this up by combining the thresholding operations...
image = rgb2ycbcr(image);

milkM = milkMaskG(image);
mintM = mintMaskG(image);
orangeM = orangeMaskG(image);
blackM = darkMaskG(image);

% 2. Crop out irrelevant part of image...

% TODO

% 2. Convert masks to ROI...
[milkM,milkROI] = maskToROI(image,milkM,20,5); 
[orangeM,orangeROI] = maskToROI(image,~orangeM,35,20); 
%[mintM,mintROI] = maskToROI(image,mintM,20,20); % old values which
%resulted in incorrect region in milk chocolate being detected as mint..
[mintM,mintROI] = maskToROI(image,mintM,35,30); 
[blackM,blackROI] = maskToROI(image,blackM,50,20); 

end

