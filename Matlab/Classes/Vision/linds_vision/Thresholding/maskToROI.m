function [ mask , ROI ] = maskToROI( image,mask, numOpen,numClose)
%Takes a raw binary mask of the image, convolutes it such that each region
%is filled and noise is eliminated, then compute ROI based on separate regions...
ROI=[];

squareSize=2000;

% numOpen typically 20-30

% 1. Remove noise
mask=bwareaopen(mask,numOpen);

% 2. Fill any gaps
se = strel('square',numClose);
mask = imclose(mask,se);

% 3. detect separate blobs and approximate a centroid and corresponding
% radius etc...
CC = bwconncomp(mask);
ROI = regionprops(CC,'Centroid','Area');%,'convexHull','MajorAxisLength','MinorAxisLength','orientation','Eccentricity');

% 4. Lets try to extract some more information

% link said large region with a smaller region (centroid) and generate an
% estimate...

% improve the estimate...


% i. detect harris features surrounding 
end