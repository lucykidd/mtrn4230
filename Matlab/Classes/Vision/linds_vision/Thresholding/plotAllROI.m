function[milkROI,milkM,mintROI,mintM,orangeROI,orangeM,blackROI,blackM]= plotAllROI (image)
close all hidden;
% Just calls above and visualises the result
[milkROI,milkM,mintROI,mintM,orangeROI,orangeM,blackROI,blackM] = compColorROI(image);

visualiseROI(milkM,milkROI,'milk');
visualiseROI(mintM,mintROI,'mint');
visualiseROI(orangeM,orangeROI,'orange');
visualiseROI(blackM,blackROI,'black');

figure();
imshow(image);
hold on;
title('All ROI');
plotROIPoints(image,milkROI,'b');
plotROIPoints(image,mintROI,'g');
plotROIPoints(image,orangeROI,'r');
%plotROIPoints(blackROI,'k');
hold off;
end