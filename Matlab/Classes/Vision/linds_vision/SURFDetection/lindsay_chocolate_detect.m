% chocSurfDetect
% MTRN4230
% Lindsay Folkard
% Z3288764

%% Main Detection Script...
function [c,allBoxes] = lindsay_chocolate_detect( sceneImage,chocMilk,chocMint,chocOrange,chocDark,chocMilkBack,chocDarkBack)
% This function will detect all chocolates found in a scene of chocolates
% Usage : chocMilk etc.. are structs of precomputed SURF points...
close all hidden; % lets get rid of any figures...may want to remove this...

% determine original scene..
origScene=sceneImage;

% Local variables
sizeMin = 1600; % size of minimum blob for focus detection
sizeMax = 1000;
c=[0,0,0,0,0,0,0,0,0]; % dummy field just so we don't get concatenation errors..remove at end..
allBoxes = [ 0 0 0 0 0 0 0 0 0 0];

% 1. Find regions of Interest for milk,mint,orange and dark chocolates...
[milkROI,milkM,mintROI,mintM,orangeROI,orangeM,darkROI,darkM] = compColorROI(sceneImage);
sceneSize = size(sceneImage);
sceneMask = uint8(zeros(sceneSize(1),sceneSize(2))); % create a blank scene mask to be used below...

% 2. Initialise the scene mask... and the total useful mask
numCut=75; % empirically define part of image that is useful...
if sceneSize(1) == 1200
    numCut=235; % empirically define part of image that is useful...
end;

sceneMask(1:numCut,:)=1; % set this area to full...
sceneImage=uint8(sceneImage);%.*(~sceneMask));
% okay, let's try it in another way...
sceneImage(1:numCut,:,:) = 0; % set to 0

% a. Determine the total mask ...sum of all masks (except dark for now since it complicates things and is not unique)...
totalMask = milkM+mintM+orangeM;

% 3. Lets Process each region of interest...(mint --> orange --> milk -->
% dark)

% i. Do mint regions....they are very unique
if ~isempty(mintROI)
    [centroidOut,thetaOut,sideOut,reachableOut,pickableOut,boxesOut,sceneMask,sceneImage] = extractChocsFromROIs(sceneImage,sceneMask,totalMask,chocMint,chocDarkBack,mintM,mintROI,140,0);
    
    % update our c matrix...
    newC = [centroidOut,thetaOut,177.*ones(length(thetaOut),1),81.*ones(length(thetaOut),1),4.*sideOut,sideOut,reachableOut,pickableOut];
    c=[c;newC]; % careful here... if we don't find anything then c will be empty...
    
    if ~isempty(boxesOut)
        allBoxes = [allBoxes;boxesOut];
    end
else
end

% iii. Do Orange regions...can sometimes clash with blue..do blue first
% to counteract this
if ~isempty(orangeROI)
    [centroidOut,thetaOut,sideOut,reachableOut,pickableOut,boxesOut,sceneMask,sceneImage] = extractChocsFromROIs(sceneImage,sceneMask,totalMask,chocOrange,chocDarkBack,orangeM,orangeROI,200,0);
    
    % update our c matrix...
    newC = [centroidOut,thetaOut,177.*ones(length(thetaOut),1),81.*ones(length(thetaOut),1),3.*sideOut,sideOut,reachableOut,pickableOut];
    c=[c;newC]; % careful here... if we don't find anything then c will be empty...
    allBoxes = [allBoxes;boxesOut];
else
end

% ii. Do milk regions...they have issues with regions merging...
if ~isempty(milkROI)
    [centroidOut,thetaOut,sideOut,reachableOut,pickableOut,boxesOut,sceneMask,sceneImage] = extractChocsFromROIs(sceneImage,sceneMask,totalMask,chocMilk,chocMilkBack,milkM,milkROI,sizeMin,1);
    
    % update our c matrix...
    newC = [centroidOut,thetaOut,177.*ones(length(thetaOut),1),81.*ones(length(thetaOut),1),1.*sideOut,sideOut,reachableOut,pickableOut];
    c=[c;newC]; % careful here... if we don't find anything then c will be empty...
    allBoxes = [allBoxes;boxesOut];
else
end

% iv. Do Dark regions...all that is left now should be dark chocolates
% or rear of mint,orange or dark chocolates... Subject to same issue as milk regions...

% lets recompute the dark ROI by subtracting the sceneMask from the roi
% mask ...
darkM = darkMask(sceneImage);
darkM = darkM .* ~sceneMask;

% recompute our regions of interest...
[darkM,darkROI] = maskToROI(sceneImage,darkM,500,1);

if ~isempty(darkROI)
    darkM = darkM - mintM - orangeM - milkM; % also want to remove any
    [centroidOut,thetaOut,sideOut,reachableOut,pickableOut,boxesOut,sceneMask,sceneImage] = extractChocsFromROIs(sceneImage,sceneMask,totalMask,chocDark,chocDarkBack,darkM,darkROI,sizeMin,1);
    close all hidden;
    
    % update our c matrix...
    newC = [centroidOut,thetaOut,177.*ones(length(thetaOut),1),81.*ones(length(thetaOut),1),2.*sideOut,sideOut,reachableOut,pickableOut];
    c=[c;newC]; % careful here... if we don't find anything then c will be empty...
    allBoxes = [allBoxes;boxesOut];
else
end

% 4. PostProcess some data
% i. Determine what points are reachable
[c(:,8),xellipse,yellipse] = reachable(c);

% ii. Determine what points are pickable...anything not reachable is
% pickable...
c(:,9) = c(:,8).*c(:,9); % compute dot product...any pickable chocolate must also be reachable...

% iii. trim any garbage from here...
c(1,:)=[]; % remove the first dummy column placed to prevent concatenation errors
allBoxes(1,:)=[];

% Debug ... Display the annotated scene....to ascertain if it is
%plotResult(origScene,allBoxes,c,xellipse,yellipse);
% pause(1.6);
end

function h=plotResult(origScene,allBoxes,c,xellipse,yellipse)
h=figure();
imshow(origScene);
hold on;

% plot the ellipse
plot(xellipse,yellipse,'r');

% plot the mint boxes j =
idx=find(c(:,6)==4);
plotBoxes('g',allBoxes(idx,:),c(idx,1:2),c(idx,8));

% plot the orange boxes j =
idx=find(c(:,6)==3);
plotBoxes('r',allBoxes(idx,:),c(idx,1:2),c(idx,8));

% plot the blue boxes j =
idx=find(c(:,6)==1);
plotBoxes('b',allBoxes(idx,:),c(idx,1:2),c(idx,8));

% plot the dark boxes j =
idx=find(c(:,6)==2);
plotBoxes('y',allBoxes(idx,:),c(idx,1:2),c(idx,8));

% plot the unkown=
idx=find(c(:,6)==0);
plotBoxes('k',allBoxes(idx,:),c(idx,1:2),c(idx,8));

hold off;
end

function [in,x,y] = reachable(c)
% Function computes if the specified chocolate is able to be reached at its
% current point

% Robot is centered at 
x0=800;
y0=0;
r=846;

t=-pi:0.2:pi;
x=x0+r*cos(t);
y=y0+r*sin(t);

% test if each point is in polygon
centroidsX= c(:,1);
centroidsY= c(:,2);

in = ones(length(centroidsX),1);
for i=1:length(centroidsX)
    if sqrt((x0-centroidsX(i)).^2 + (y0-centroidsY(i)).^2) > 846
        in(i) = 0;
    end
end

end


function plotBoxes(color,boxes,centroid,in)
    % a simple function to plot boxes in a scene
    for i=1:length(boxes(:,1))
        line(boxes(i,1:5)' , boxes(i,6:10)', 'Color', color);
        hold on;
        plot(1600-centroid(i,1),centroid(i,2),strcat(color,'*'));
        if in(i)==1
            % inside the circle
            plot(1600-centroid(i,1),centroid(i,2),strcat(color,'o'));
        end
    end;
end

%% ExtractChocsFrom ROI's
function [centroidOut,thetaOut,sideOut,reachableOut,pickableOut,boxesOut,sceneMask,sceneImage] = extractChocsFromROIs(sceneImage,sceneMask,totalMask,chocFront,chocBack,chocMask,chocROI,sizeMin,enableBack)
% sceneImage,chocImage,sceneMask,totalMask,chocMask,chocROI,chocPoints,chocFeatures,chocUniqPoints,chocUniqFeatures,sizeMin,chocBack,chocBPoints,chocBFeatures,chocBUniqPoints,chocBUniqFeatures,enableBack
% This function extracts the locations of the chocolates from the ROIs and given chocolate...
% scene mask is the entire mask of the scene (cumulative)
% choc mask is the mask from this colour segmentation..

% While we have points in our ROI struct lets try and find a new
% chocolate..
coverThreshold = 0.7; % if 70% of one region is covered by another then it is deemed to be part of that area...
centroidOut=[0,0];
sideOut=[1];
thetaOut=[0];
reachableOut=[1];
pickableOut=[1];
boxesOut=[1,2,3,4,5,6,7,8,9,10];
count=1;
side=1;

% 1. Convert ROI to cell array and then matrix and lastly lets sort them in
% descending order....
chocROI = struct2cell(chocROI);
chocROI = cell2mat(chocROI');
chocROI = sortrows(chocROI,-1);

% 2. Scan through our ROI's
while ~isempty(chocROI)
    
    % i. Naively pick the largest chocROI
    % This ignores multiple joined regions, which we might decide to do
    % later..
    side=1;
    poi = chocROI(1,2:3);
    count=count+1;
    outcome=0; % are we successful in detecting a valid chocolate...
       
    % ii. If region above size threshold, Perform/use SURF Detection on focus area
    % NB: perhaps we need to consider the masking by other chocolates or
    % surfpoints here...
    try
        
        if chocROI(1,1) > sizeMin
            side=1;
            [outcome,inlierChoc,inlierScene, scenePoints ,centroid,theta,box] = findROIMatches(sceneImage,chocFront.image,poi,50,chocFront.uniqPoints,chocFront.uniqFeatures);
            
            if outcome == 0
                [outcome,inlierChoc,inlierScene, scenePoints , centroid,theta,box] = findROIMatches(sceneImage,chocFront.image,poi,160,chocFront.allPoints,chocFront.allFeatures);
            end
        else % If smaller, or insufficient matches found , Perform SURF Detection on entire region matching against both front and back
            %disp('Region is too small to be a focus ROI...do entire area of chocolate...');
            [outcome,inlierChoc,inlierScene, scenePoints ,centroid,theta,box] = findROIMatches(sceneImage,chocFront.image,poi,160,chocFront.allPoints,chocFront.allFeatures);
            
            if enableBack == 1
                % we want to also find the features for the back of the chocolate...
                [outcome2,inlierChoc2,inlierScene2, scenePoints2 ,centroid2,theta2,box2] = findROIMatches(sceneImage,chocBack.image,poi,150,chocBack.allPoints,chocBack.allFeatures);
                side=0;
                % naively lets just base this on number of matched
                % inlierscene points
                
                % If shape does not make sense..outcome should be zero...
                    if outcome2==1 &&  outcome == 1
                        %disp('Both give valid transformations');

                        if length(inlierChoc2) >= length(inlierChoc)
                            %disp('I think that this is the back of the chocolate...');
                            outcome=outcome2;
                            inlierChoc=inlierChoc2;
                            inlierScene=inlierScene2;
                            scenePoints=scenePoints2;
                            centroid=centroid2;
                            theta=theta2;
                            box=box2;
                        else
                            %disp('I think that this is the top side of the chocolate');
                            side=1;
                        end
                        % we are pretty certain that
                    elseif outcome2 == 1
                        %disp('Only the back of the chocolate matches...');
                        outcome=outcome2;
                        inlierChoc=inlierChoc2;
                        inlierScene=inlierScene2;
                        scenePoints=scenePoints2;
                        centroid=centroid2;
                        theta=theta2;
                        box=box2;
                    else
                        %disp('Only the front of the chocolate matches...');
                        side=1;
                    end
                
            end
            
        end
        
        if outcome == 1
            
            % 5. Check that basically the entire region we have just looked at in
            % the mask has been covered by the new box / mask ... if it has not
            % then maybe we have two chocolates here and thus must repeat the
            % process .. TODO
            
            % 6. Delete all regions from ROI that are more or less completely covered by this newly detected chocolate...(only really applicable for milk and dark chocolate)
            [chocROI,chocMask] = reshapeROI(poi,centroid,theta,box,chocMask,chocROI);
            
            % 7. Look for obstructions...
            obstructed=detectObstructions(chocMask,totalMask,box); % Should always be zero now as this feature is turned off...
            if obstructed == 0 % no obstruction, we can clear the area without worry...
                [sceneMask,sceneImage ] = updateSceneMask(sceneImage,sceneMask,box); % clear the area and update the scene image (for now just masking out chocolate...hopefully that works..)
            else
                % we do not want to eliminate any of this region....
            end
            
            % we never want to say if something is obstructed...
            obstructed=0; % quick fix...
            
            % 8. Append the new data to our outVectors...
            centroidOut=[centroidOut;centroid];
            sideOut=[sideOut;side];
            thetaOut=[thetaOut;theta];
            reachableOut=[reachableOut;1]; % TODO...
            pickableOut=[pickableOut;~obstructed];
            box= [box(:,1)',box(:,2)'];
            boxesOut=[boxesOut;box];
            
        else
            % 4. If nothing found, leave it and mark it... TODO
            chocROI(1,:)=[]; % delete this point for now...
            % TODO - add this point to an unidentified list...
        end
        
    catch e
        % should never happen, but in unforeseen situations do not want a
        % single error to cause the rest of the detection to fail...
        %e
        %disp('Exception thrown outer');
        try
            chocROI(1,:)=[]; % delete this roi as it has caused a problem
        catch e
            %disp('Exception thrown inner..');
            break;
        end
    end
    
   % disp('Finished Processing region....');
   %waitforbuttonpress;
end

% Lastly, remove the single placeholder from the rest of the arrays...
            centroidOut(1,:)=[];
            sideOut(1,:)=[];
            thetaOut(1,:)=[];
            reachableOut(1,:)=[]; 
            pickableOut(1,:)=[];
            boxesOut(1,:)=[]; 
end

%% reshapeROI
function [chocROI,sceneMask] = reshapeROI(poi,centroid,theta,box,chocMask,chocROI)
% Function will take the point of interest, chocROI and the newly computed
% box and attempt to
sceneMask=chocMask; % TODO - update me later maybe...

% If not entire region covered satisfactorily... then we want to recompute
% an ROI for it... (handle case of two chocolates...?) since it is likely
% we have a double up chocolate image..

% TODO - change this to compute the area left over for the chocROI of
% interest .... (i.e if two blue or brown right next to each other then we might
% need to separate the two... and generate a new ROI...)
chocROI(1,:)=[]; % Delete the poi from our image...

[in,on] = inpolygon(chocROI(:,2),chocROI(:,3),box(:,1),box(:,2));

if length(in) > 0
    %disp('Found another point inside our blocked out region...delete it..');
    chocROI(in,:)=[];
end


end

%% findROIMatches - match only in a given location...
function [outcome,inlierChoc,inlierScene, scenePoints ,centroid,theta,box] = findROIMatches(sceneImage,chocImage,location,regionSize,chocPoints,chocFeatures)%,metricThresh,matchThresh,ratio)
% Takes a given location and then uses this location to try and find the
% matches -- >now just the best match...

% 1. Compute the SURF point and descriptors of interest in the scene image
% NB: change this in the future to just extract relevant data from the
% surfpoints struct and feature vectors....
sceneImage=rgb2gray(sceneImage);

% Old detection parameters that work ... %scenePoints = detectSURFFeatures(sceneImage,'MetricThreshold',1000,'NumOctaves',7,'NumScaleLevels',20,'ROI',[int32(max(1,location(1)-regionSize)),int32(max(1,location(2)-regionSize)),int32(2*regionSize),int32(2*regionSize)]);
% 250 sweetspot...
scenePoints = detectSURFFeatures(sceneImage,'MetricThreshold',240,'NumOctaves',8,'NumScaleLevels',8,'ROI',[int32(max(1,location(1)-regionSize)),int32(max(1,location(2)-regionSize)),int32(2*regionSize),int32(2*regionSize)]);

[sceneFeatures,validScenePoints] = extractFeatures(sceneImage,scenePoints);

% 2. Match the features to the chocPoints and features...
% 12 is okay...lets bump it up to 14.00 sweetspot....
[matches,matchMetric]= matchFeatures(chocFeatures,sceneFeatures,'MatchThreshold',14.00,'MaxRatio',0.8,'Unique',true); % normal chocolate detection...

%fprintf('Number of matches found is %d\n',length(matchMetric));

% 3. Try to compute a valid homography/transformation...
%[outcome,inlierChoc,inlierScene,centroid,theta,box] = compValidHomography(matches,matchMetric,chocPoints,scenePoints,chocImage);
[outcome,inlierChoc,inlierScene,centroid,theta,box] = compValidHomography(matches,matchMetric,chocPoints,validScenePoints,chocImage);
end

%% compValid Homography...
function [outcome,inlierChoc,inlierScene,centroid,theta,box] = compValidHomography(matches,matchMetric,chocPoints,scenePoints,chocImage)
% Will try to compute the geometric transform, using prior knowledge about the
% point relations in the chocImage .... -> TODO...

% Constant Variables..
MaxNumTrials = 1000; % default 1000

Confidence = 98 ; % default 99
MaxDistance = 2.0; % default 1.5

% Declare out variables in case of failure...
outcome=1; % Successful transformation
tform=0;
inlierChoc = [];
inlierScene=[];
centroid=[];
theta=[];
box=[];

% 1. Try to compute a transformation...
if length(matchMetric) < 2
    %disp('Not enough matches...can not comput homography');
    outcome=0;
    return;
end

try
    %Try to create a geometric transform with the points...
    valid=0;
    
    while ~valid
        
        matchedChocPoints = chocPoints(matches(:,1),:);
        matchedScenePoints = scenePoints(matches(:,2),:);
        
        [tform, inlierChoc, inlierScene] = estimateGeometricTransform(matchedChocPoints, matchedScenePoints, 'similarity','MaxNumTrials',MaxNumTrials,'Confidence',Confidence,'MaxDistance',MaxDistance);
        boxPolygon = [1, 1;size(chocImage, 2), 1;size(chocImage, 2), size(chocImage, 1);1, size(chocImage, 1); 1, 1];
        newBox = transformPointsForward(tform, boxPolygon);

        %lets make sure that the box is correctly sized....
        if ~validBox(newBox,53,105,140,205)
            %disp('Box is invalid..it does not meet size constraints...');
            outcome=0;
            valid=0;
            %return;
            % lets cull some feature points...
            if length(matchMetric) > 5
                
                % sort our matches...
                help = [matchMetric,matches];
                help = sort(help,1);
                matchMetric = help(:,1);
                matches = help(:,2:3);
                
                % half the number of matches...
                len=length(matchMetric);
                matches = matches(1:uint8(len/3),:);
                matchMetric = matches(1:uint8(len/3),:);
                
                % reduce the confidence and number of trials... (lets not
                % take for ages..)
                Confidence = 0.93;
                MaxNumTrials = 500;
                MaxDistance = 3.2;
                
            else
                outcome=0;
                return;
            end
            
        else
            valid=1;
            outcome=1;
            %disp('We have a valid box...');
        end;
    end
    
    % 3. From a valid polygon lets determine the orientation and the location...
    numCut=0; % TODO - change me later...
    [xi , yi ] = curveintersect([newBox(1,1) newBox(3,1)],[newBox(1,2) newBox(3,2)],[newBox(2,1) newBox(4,1)],[newBox(2,2) newBox(4,2)]);
    centroid = [1600 - xi, yi + numCut];
    
    theta = atan2(newBox(1,2)-newBox(4,2),newBox(1,1)-newBox(4,1));
    
    if theta < 0
        theta = -pi - theta;
    else
        theta = pi - theta;
    end;
       
    box=newBox;
    
catch e
    %disp('Transform unsuccessful - an exception has been thrown...');
    outcome=0; % transformation is unsuccessfull
end

end


function [sceneMask,sceneImage ] = updateSceneMask(sceneImage,sceneMask,box)
% Function will take a sceneImage and sceneMask and a box, and extract this
% box from the image by convoluting the scene mask...it will also update
% the sceneImage to remove this region...

% 1. convert the polygon to a mask...
sz=size(sceneMask);
newMask = uint8(poly2mask(double(box(:,1)),double(box(:,2)),sz(1),sz(2)));

% 2. reapply to the sceneMask and the sceneImage
sceneMask=sceneMask+newMask;
sceneImage = (sceneImage).*uint8(repmat(~sceneMask,[1,1,3]));
end

function [obstructed] = detectObstructions(thisMask,totalMask,box)
% detectObstructions : Attempts to detect if a box region of a certain
% image is obstructed or not

obstructed=0;
% thresh = 20 ; % threshold of number of pixels in region for it to be considered obstructed...
% sz=size(totalMask);
% boxMask = poly2mask(double(box(:,1)),double(box(:,2)),sz(1),sz(2));
% 
% % 1. If any of totalMask-thisMask is inside the box then it is
% % obstructed... (simple case...)
% myMask = totalMask - thisMask;
% roi = boxMask.*myMask;
% 
% % now if we have any of the other colours in this mask, then we have an
% % issue...an occlusion by the other colour...
% if sum(roi(:)) > thresh
%     %disp('We have an occlusion on this chocolate...');
%     obstructed=1;
% else
%     %disp('No occlusion is detected..');
% end

end

function isValid = validBox(box,shortMin,shortMax,longMin,longMax)
% Simple function lets us know if a given box is valid based purely on it's
% dimensions
% TODO . ... fill me out later...
isValid=1;% lets just say it is valid right now...

% how do we determine if a box is valid...base it roughly on size....
side12 = sqrt((box(1,1)-box(2,1)).^2 + (box(1,2) - box(2,2)).^2);%
side23 = sqrt((box(2,1)-box(3,1)).^2 + (box(2,2) - box(3,2)).^2);%

if side12 > shortMax || side12 < shortMin
    isValid=0;
    %disp('Short side is out of range..invalid box!');
elseif side23 > longMax || side23 < longMin
    isValid=0;
   %disp('Long side is out of range..invalix box!'); 
end

end
