function [BW] = createConveyorMask2(RGB)
%createMask  Threshold RGB image using auto-generated code from colorThresholder app.

% Convert RGB image to chosen color space
I = rgb2ycbcr(RGB);

% Define thresholds for channel 1 based on histogram settings
channel1Min = 24.000;
channel1Max = 123.000;

% Define thresholds for channel 2 based on histogram settings
channel2Min = 101.000;
channel2Max = 128.000;

% Define thresholds for channel 3 based on histogram settings
channel3Min = 127.000;
channel3Max = 160.000;

% Create mask based on chosen histogram thresholds
BW = (I(:,:,1) >= channel1Min ) & (I(:,:,1) <= channel1Max) & ...
    (I(:,:,2) >= channel2Min ) & (I(:,:,2) <= channel2Max) & ...
    (I(:,:,3) >= channel3Min ) & (I(:,:,3) <= channel3Max);

% Invert mask
BW = ~BW;

