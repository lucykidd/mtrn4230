function [ BW,stats,boxes] = isolateBox(im,xi,yi)
%Function will return the location of the box...
Threshold=600;

% 1. Colour Threshold to isolate box..
[BW] = createConveyorMask2(im);

% 2. Crop the image/scene using the  vertices specified in xi and yi
RegionMask = roipoly(im,xi,yi);
BW=BW&RegionMask; % AND the two binary masks...

% 3. Morphology transformations...should not be needed
BW=bwareaopen(BW,2000); %get rid of anything less than 2000 pixels..

% 4. Use region props to detect interesting regions...
stats = regionprops(BW,'Area','Centroid','MajorAxisLength','MinorAxisLength','orientation');

% because im lazy lets just draw a box based on orientation
boxes=[];
for i=1:length(stats)
    if stats(i).Area > Threshold
       % we have a valid box
       angle = (stats(i).Orientation * 180/pi);% - pi/2;
       xc = stats(i).Centroid(1);
       yc = stats(i).Centroid(2);
       lmax = stats(i).MajorAxisLength;
       lmin = stats(i).MinorAxisLength;
       vertX = [lmax/2,-lmax/2,-lmax/2,lmax/2];
       vertY = [lmin/2,lmin/2,-lmin/2,-lmin/2];
       vertices = [vertX;vertY];
       
       % generate our rotation matrix
       R = [cos(angle) , -sin(angle);sin(angle),cos(angle)];
       
       % Compute the transformation... rotation +translation
       newVertices = R*vertices + [xc,xc,xc,xc;yc,yc,yc,yc];
       boxes=[boxes; newVertices(1,:),newVertices(2,:)];
    end
end

% NB: todo .. this method is not very good... may want to improve it in the
% future...

end


