
function out=batchProcessImage (path,milkStruct,mintStruct,orangeStruct,darkStruct,milkBackStruct,darkBackStruct)
% This function is to be run in another process and used to process images
% as they need to be..it will run continuosly in another thread and only
% stop when told to do so...
out=0;
disp('begin our batch processing code...');

imageReady=0;
state=0;

while 1
    
   % 1. Lets read the .mat file containing our ready
   imageReady=0;
   try
        disp(strcat(path,'flag.mat'));
        load(strcat(path,'flag.mat'));
   catch e
       disp('Error unable to open file properly..');
       pause(0.3);
       continue;
   end
   
   % 2. check to see if processing is needed
   if imageReady == 1
       disp('Image is ready to begin processing....read the image file and do some processing');
       state=1; % currently processing the task...
       imageReady=0; % image has been taken
       
       try
       save(strcat(path,'flag.mat'),'imageReady','state');% tell the gui image is no longer ready..
       
       catch e
          e
          disp('Error: unable to save flag file...');
          pause(0.3);
          continue;
       end
       % Now ... lets do some processing....
       
       % 1. read the new image
       try
        disp('read the new image to process..');
        load(strcat(path,'image.mat'));
        [c,allBoxes] = lindsay_chocolate_detect(im,milkStruct,mintStruct,orangeStruct,darkStruct,milkBackStruct,darkBackStruct);
        
        % lets save our computation...
        disp('save our output image..');
        save(strcat(path,'output.mat'),'c','allBoxes');
        
        % lets update the flag mat file...
        state=0;
        disp('change the flag.mat file...');
        save(strcat(path,'flag.mat'),'imageReady','state');% tell the gui image is no longer ready..
        
       catch e
           e
           disp('Error: unable to process the new image..');
           pause(0.1);
           continue;
       end
           
   elseif imageReady == 0
       disp('Image is not ready to begin processing!!');
   else
       disp('Task must stop...');
       break;
   end
    
   pause(0.2); % pause for 0.3 seconds... 
end

disp('task stopped...');

end