classdef Vision < VisionInterface
    % Vision : This class does all of our vision work. Basically it should
    % periodically retrieve images , do some processing and then update the
    % robot system state (with known chocolate locations etc..) and also
    % update the image in our gui. We can prompt this function or let it do
    % it by itself with a timer....
    %
    % Usage : TODO ...
    %
    % Author : Lindsay
    % Date : 24/03/2015
    
    %% Properties
    properties (Access = public)
        
    end
    
    properties (Access = private)
        % Only this class can access these properties
        %timerObj;
        %running;
        mode; % 0 for dummy images, 1 for actual videoinput...
        vidTable;
        vidConveyor;
        
        % Chocolate structs...
        milkStruct;
        darkStruct;
        mintStruct;
        orangeStruct;
        
        darkBackStruct;
        milkBackStruct;
        
        cam;
        % dummy images
        dummyTable;
        dummyConveyor;
        lastTableIm;  % lsat table image caputerd...
        
        % Last good captured images..
        clearTableIm; % last clear table image...
        clearConveyorIm; % last clear conveyor image...
        
        % timer handle
        processTimer;
        
        % path
        path;
        enableNextImage; % when set to 1 we are able to tell the task to process a new image
        tableClear; % tell the task that the table is clear and ready for processing
        convClear;
        processingSpace; %
        processingJob; % a handle to the image processing separate job..
        % when set to 0 we cannot..
        
        count; % silly debug variable...
        visionErr; % variable to store if error in vision...
        enableMainTask; % setting this to zero disables the main task...
    end
    
    %% Methods
    methods (Access = public)
        
        function this = Vision(varargin)
            % Constructor : constructs our vision object...
            % Usage : it should be called whenever the default constructor is
            % called
            this.name='Vision';
            this.mode=0; % set to dummy to begin with...
            this.enableNextImage=1;
            this.tableClear=0; % don't process any images...yet...
            this.convClear=0;
            this.count=13;
            this.visionErr=0;
            
            if nargin > 0 && varargin{1} > 0
                % we can set this mode to be 1...
                % lets not use dummy images but the actual feed...
                this.mode=1;
                try
                    this.initVidTable(); % create our videoinput object for table camera...
  %                  this.initVidConveyor(); % create our vidoeinput object for conveyor camera...
                catch e
                    this.error(strcat('Error with setting up the videocapture object...', getReport(e)));
                end
            end
            
            try
                this.loadData(); % load in our chocolate detection structs...
                %                 this.functionHandle = @(input)lindsay_chocolate_detect(input,this.milkStruct,this.mintStruct,this.orangeStruct,this.darkStruct,this.milkBackStruct,this.darkBackStruct);
                %                 this.log('Successfully created a handle to our chocolate detection function...');
                %                 this.processTimer = timer('ExecutionMode','singleShot','startDelay',0.08); % create our process timer function
                %                 this.processTimer.TimerFcn = this.functionHandle;
                %                 this.log('Successfully created a timer function to handle asynchronous image processing execution..');
            catch e
                this.error('Unable to load chocolate detection structs properly');
            end
            
            % try to read in our dummy images..
            try
                this.dummyTable = imread(which('IMG_035.jpg'));
                this.dummyConveyor = imread(which('conveyor5.jpg'));
                %this.dummyTable = imread(which('dummyTable.jpg'));
                %this.dummyConveyor = imread(which('dummyConveyor.jpg'));                
            catch e
                this.error('Unable to read dummy images successfully into the program!!');
            end
            
            % Iinitialise our vision task
            this.setRegularTask(0,2.0,0.33); % sets parameters for this regular task
            
            this.path = which('linds_features_mtrn4230.mat'); % get our path...
            this.path = strrep(this.path,'linds_features_mtrn4230.mat',''); % remove the filename...
            
            % Start our background processing task...
            this.processingSpace = parcluster('local');
            
            this.log('Start a new batch job for handling image processing....');
            try
                % Initialise our .mat files required to run the program...
                imageReady=0;
                state=0;
                im = this.dummyTable;
                save(strcat(this.path,'flag.mat'),'imageReady','state'); % signal that we want to stop...
                save(strcat(this.path,'image.mat'),'im'); % Just a quick dummy image to im to prevent issues...
                
                c=parcluster('local');
                this.processingJob = batch(c,@batchProcessImage,1,{this.path,this.milkStruct,this.mintStruct,this.orangeStruct,this.darkStruct,this.milkBackStruct,this.darkBackStruct});
                this.consoleLog('Successfully started the batch job for image processing...');
            catch e
                e
                this.error ('Error: Unable to start a batch job!!!');
            end
        end
        
        function initVidTable(this)
            % Lets try and setup a videoinput object for both classes...
 %           this.cam = videoinput('winvideo');
 %           this.cam.FramesPerTrigger = 1;
 %           triggerconfig(this.cam,'Manual');
 %           start(this.cam);
  %          trigger(this.cam);
            % uncomment line 145,146,150,151
         %    this.vidTable = videoinput('winvideo',1,'RGB24_1600x1200');
            this.vidTable = videoinput('winvideo');
             this.vidTable.FramesPerTrigger = 1;
             this.vidTable.TriggerRepeat=Inf;
             this.vidTable.Timeout = 20; % set timeout to 20s
%             %this.vidTable.StopFcn = @this.restartTable; % check this
             triggerconfig(this.vidTable,'Manual');
             start(this.vidTable);
             trigger(this.vidTable);
        end;
        
        function initVidConveyor(this)
            % uncomment line 157,158,162,163
%            this.vidConveyor = videoinput('winvideo',2,'RGB24_1600x1200');
 %           this.vidConveyor.FramesPerTrigger = 1;
 %           this.vidConveyor.TriggerRepeat=Inf;
%            this.vidConveyor.Timeout = 20; % set timeout to 20 seconds..
            %this.vidConveyor.StopFcn = @this.restartConveyor; % check this
   %         triggerconfig(this.vidConveyor,'Manual');
    %        start(this.vidConveyor);
    %        trigger(this.vidConveyor);
        end;
        
        function delete(this)
            % Quick vision destructor...
            imageReady=-1;
            state=0;
            destroy(this.processingJob); % this should stop and destroy the processing job..
            save(strcat(this.path,'flag.mat'),'imageReady','state'); % signal that we want to stop...
            if this.mode == 1
                % we need to delete the video input objects..
                this.log('Destroy our video input objects...');
                delete(this.vidTable);
                delete(this.vidConveyor);
                delete(this.cam);
            end
            this.log('Finished destroying the vision object...');
        end;
        
        % Put your abstract methods defined in your
        function addHandles(this,guiHandle,robControllerHandle)
            this.guiHandle=guiHandle;
            this.robControllerHandle=robControllerHandle;
        end
        
        % Only this class can seee and use these methods...
        function out = mainTask(this,timerObj,event,string_arg)
            % this will periodically grab frames from the cameras and try to
            % display them...
            state=0;
            imageReady=0;
            c=[];
            allBoxes=[];
            
            % Disable main Task while we are blocking...
            if this.enableMainTask == 0
               return; 
            end
            
            try
                %this.count=this.count+1;
                
                %% 1. Acquire new conveyor and table images...
                [im,imConveyor] = this.acquireImages();
                this.lastTableIm = im; % save our last good table image for processing...
                
                
                
                %% 2. Lets check for an image update from our processing task...
                load(strcat(this.path,'flag.mat'));
                
                if this.enableNextImage==0 && imageReady==0 && state == 0
                    % task has finished processing the latest image...
                    this.enableNextImage=1; % we are able to process a new image...
                    
                    % read in the data from our background task...
                    this.log('Background task finished processing image');
                    load(strcat(this.path,'output.mat')); % read the output data...
                    
                    % Update the gui and the robot controller with the new
                    this.guiHandle.updateChocolateStatus(c,allBoxes); % update our gui...
                    this.robControllerHandle.updateChocMatrix(c);
                    
                elseif this.enableNextImage==0 && imageReady > 0
                    this.log('Background Task has not begun processing the image');
                elseif this.enableNextImage==0 && state == 1
                    this.log('Background task is processing the image....');
                end
                
                %% 3. Process a new table Image....
                if this.enableNextImage == 1 && this.tableClear==1
                    % we want to tell the image processor to process a new
                    % image...
                    % save the image in a data file...
                    this.clearTableIm=im;
                    save(strcat(this.path,'image.mat'),'im');
                    % set the flag..
                    imageReady=1;
                    state=-1;
                    save(strcat(this.path,'flag.mat'),'state','imageReady');
                    this.log('Tell our image processor to process a new image..');
                    this.tableClear=0; % don't want to continuosly process images...Check this...
                    this.enableNextImage=0; % want to say we cant get the next image now...
                end
                
                %% 4. Process a new conveyor image if required....
                if this.convClear == 1
                    this.clearConveyorIm = imConveyor;
                    this.processBox(imConveyor);
                    this.convClear=0; % rest the flag...
                end;
                
            catch er
                er
                disp(getReport(er,'extended'));
                this.error('Error with capturing the images :');
            end
            out=0; % junk output..
        end;
        
        function [im,imConveyor] = acquireImages(this)
            % acquireImages: will get images from the required image stream
            % and trigger a new image capture when ready....
            
            if this.mode == 1
                try
                    % uncomment 271,272,273,274,275
                    if ~isrunning(this.vidTable) % stop for good measure    
                        disp('table vision is not running...??');
                        start(this.vidTable);
                        trigger(this.vidTable); % lets get a new image...
                    end;
        %            if ~isrunning(this.cam) % stop for good measure    
      %                  disp('problem with cam');
      %                  start(this.cam);
     %                   trigger(this.cam); % lets get a new image...
      %              end;
                    % uncomment 282,283,284,285,286
         %           if ~isrunning(this.vidConveyor) % stop for good measure 
        %                this.log('table vision is not ruinngin');
       %                 start(this.vidConveyor);
      %                  trigger(this.vidConveyor); % lets get a new image...
     %               end;
                    this.log('try and get a frame');
                    


                    % uncomment 2 below, remove the silly ones
                   im = getdata(this.vidTable,1,'uint8');
         %           imConveyor = getdata(this.vidConveyor,1,'uint8');
            %        im = getdata(this.cam,1,'uint8');
                    imConveyor = im;
                    this.log('got frames');
         %           start(this.cam);
           %         trigger(this.cam);
                   
                    % uncomment all below
   %                 start(this.vidTable);
                  %  start(this.vidConveyor);
                    trigger(this.vidTable); % lets get a new image...
                %    trigger(this.vidConveyor); % lets get a new image...
                    this.guiHandle.updateTableImage(im,1,2,3,4);
                    this.guiHandle.updateConveyorImage(imConveyor,1,2,3,4);
                catch e
                    e
                    % uncomment maybe here
                    %if isvalid(this.vidTable)
                        % do nothing
                    %else
                       % need to recreate the table vision object
              %         this.error('Table vision is invalid..');
             %          delete(this.vidTable);
            %           this.initVidTable(); % reinitialise the video input object...
                    %end;
                    
                    %if isvalid(this.vidConveyor)
                       % do nothing... 
                    %else
             %          this.error('Conveyor vision object is invalid..');
              %         delete(this.vidConveyor);
               %        this.initVidConveyor(); % reinitialise the videoinput object
                    %end;
                    
                end;
            else
                % lets use only trhe example images...
                this.guiHandle.updateTableImage(this.dummyTable,[2],[1],[1],[1]);
                this.guiHandle.updateConveyorImage(this.dummyConveyor,3,1,1,1,1,1);
                im=this.dummyTable; % just to help out below...
                imConveyor=this.dummyConveyor; % just to help out below...
            end;
            
            this.visionErr=0;
            
            % Check if scene is clear or not..
            if this.tableClear ==1
                this.guiHandle.clearTableImage=im;
            elseif this.convClear==1
                this.guiHandle.clearConvImage=imConveyor;
            end;
            
        end
        
        function loadData(this)
            % this function shall attempt to load the required reference data into this class...
            
            try
                absPath = which('linds_features_mtrn4230.mat');
                load(absPath);
                
                this.milkStruct=milkStruct;
                this.mintStruct=mintStruct;
                this.orangeStruct=orangeStruct;
                this.darkStruct=darkStruct;
                this.darkBackStruct=darkBackStruct;
                this.milkBackStruct=milkBackStruct;
                this.log('successfully read in chocolate structs...');
                
            catch e
                e.stack
                e.cause
                e.identifier
                this.error('Unable to read in chocolate feature structs!!!');
            end
        end
        
        function triggerProcessTableIm(this)
            % triggerProcess: function will manually cause our background
            % task to process a new image captured from the table camera...
            this.tableClear=1;
            this.log('trigger process table image..');
        end
        
        function triggerProcessConvIm(this)
            % triggerProcessConvIm: function will manually cause our background
            % task to process a new image captured from the conveyor camera...
            this.convClear=1;
            this.log('trigger process conveyor image..');
        end
        
        function c = blockProcessTableIm(this)
            % blockProcessImage : this function will trigger image processing on the current image
            % but will block until the processing is complete...this is not
            % to be used anywhere except by pat...
            this.enableMainTask=0; % make certain the mainTask is doing nothing....
            c=[]; % the new chocolate matrix...
            try
                im = this.lastTableIm;
                this.clearTableIm=im;
                save(strcat(this.path,'image.mat'),'im');
                % set the flag..
                imageReady=1;
                state=-1;
                save(strcat(this.path,'flag.mat'),'state','imageReady');
                this.log('Tell our image processor to process a new image..');
                this.tableClear=0; % don't want to continuosly process images...Check this...
                this.enableNextImage=0; % want to say we cant get the next image now...

                % wait until complete..probably should put some kind of
                % timeout..
                i=1;
                imageReady=1;
                state=1;
                while i < 1000
                    load(strcat(this.path,'flag.mat'));
                    this.consoleLog('Blocking...')
                    if  imageReady==0 && state == 0
                        this.log('Background task finished processing image');
                        load(strcat(this.path,'output.mat')); % read the output data...
                        this.guiHandle.updateChocolateStatus(c,allBoxes); % update our gui...
                        this.robControllerHandle.updateChocMatrix(c);
                        i=200;
                        break;
                    end;

                    pause(0.05); % pause for 50ms
                    i=i+1;
                end;

            catch e
                e
                this.error('Block Process Table image is failing..... try again...indefinately...bad');
                c=this.blockProcessTableIm();
            end;
            this.enableMainTask=1; % reenable the main task again...
        end
       
        function updateTableIm(this,imName)
            % updateTableIm : This function takes an image, located in the
            % ..../Vision/Tests/ folder and sets it as our current table
            % image...
            
            path = which('Vision.m');
            path = strrep(path,'Vision.m','');
            path = strcat(path,'Test_Table\');
            
            try
                im = imread(strcat(path,imName));
                this.dummyTable=im;
                %sthis.clearTable=im; % to short circuit the clear table reaquisition
                
            catch e
                e
                this.error(getReport(e));
            end
            
        end
        
        function updateConvIm(this,imName)
            % updateConvIm : This function takes an image, located in the
            % ..../Vision/Test_Conv/ folder and sets it as our current table
            % image...
            
            path = which('Vision.m');
            path = strrep(path,'Vision.m','');
            path=strcat(path,'Test_Conv\');
            
            try
                im = imread(strcat(path,imName));
                this.dummyConveyor=im;
                %sthis.clearTable=im; % to short circuit the clear table reaquisition
                
            catch e
                e
                this.error(getReport(e));
            end
            
        end
        
        function restartTable(this)
            % restarts the table capture process
            try
            start(this.vidTable);
            trigger(this.vidTable);
            catch e
                e
                this.error('Unable to restart the table capture process');
            end;
        end;
        
        function restartConveyor(this)
            % restarts the conveyor capture process
            try
            start(this.vidConveyor);
            trigger(this.vidConveyor);
            catch e
               e
               this.error('Unable to restart the table conveyor capture process');
            end
        end;
        
    end
    
    methods (Access = private)
        
        function processBox(this,im)
            % Function takes as input a box image and attempts to process it...
            
            % Image crop coordinates...
            xi = 10^3 .*[0.6025;1.6000;1.6015;0.6070;0.6025];
            yi = [290.750;283.2500;902.7500;905.7500;290.7500];
            
            [BW,bMat,boxes] = this.isolateBox(im,xi,yi);
            
            % update the gui and robot controller
            this.guiHandle.updateBoxStatus (bMat(:,2:3),bMat(:,6),boxes);
            this.robControllerHandle.updateConveyorBox(bMat(:,2:3),bMat(:,6),0,0,0); % update rob controller boxes...
        end
        
        function [ BW,boxMatrix,boxes] = isolateBox(this,im,xi,yi)
            %Function will return the location of any boxes in the stats
            %object...
            Threshold=35000;
            
            % 1. Colour Threshold to isolate box..
            [BW] = this.createConveyorMask2(im);
            
            % 2. Crop the image/scene using the  vertices specified in xi and yi
            RegionMask = roipoly(im,xi,yi);
            BW=BW&RegionMask; % AND the two binary masks...
            
       %     figure();
        %    imshow(RegionMask);
            
         %   figure();
          %  imshow(BW);
            
            % 3. Morphology transformations...should not be needed
            BW=bwareaopen(BW,30000); %get rid of anything less than 30000 pixels..
            
            % 4. Use region props to detect interesting regions...
            stats = regionprops(BW,'Area','Centroid','MajorAxisLength','MinorAxisLength','Orientation');
            
            % because im lazy lets just draw a box based on orientation
            
            boxes=[];
            boxMatrix=[];
            for i=1:length(stats)
                if stats(i).Area > Threshold
                    % we have a valid box
                    angle = (stats(i).Orientation * pi/180);% - pi/2;
                    xc = stats(i).Centroid(1);
                    yc = stats(i).Centroid(2);
                    lmax = stats(i).MajorAxisLength;
                    lmin = stats(i).MinorAxisLength;
                    vertX = [lmax/2,-lmax/2,-lmax/2,lmax/2];
                    vertY = [lmin/2,lmin/2,-lmin/2,-lmin/2];
                    vertices = [vertX;vertY];
                    
                    % generate our rotation matrix
                    R = [cos(-angle) , -sin(-angle);sin(-angle),cos(-angle)];
                    
                    % Compute the transformation... rotation +translation
                    newVertices = R*vertices + [xc,xc,xc,xc;yc,yc,yc,yc];
                    boxes=[boxes; newVertices(1,:),newVertices(2,:)];
                    
                    % make our box matrix...
                    boxMatrix= [ boxMatrix ; stats(i).Area , stats(i).Centroid(1), stats(i).Centroid(2),stats(i).MajorAxisLength,stats(i).MinorAxisLength,stats(i).Orientation];
                end
                
            end
        end
        
        function [BW] = createConveyorMask2(this,RGB)
            %createMask  Threshold RGB image using auto-generated code from colorThresholder app.
            
            % Convert RGB image to chosen color space
            I = rgb2hsv(RGB);
            
            % Define thresholds for channel 1 based on histogram settings
            channel1Min = 0.928;
            channel1Max = 0.125;
            
            % Define thresholds for channel 2 based on histogram settings
            channel2Min = 0.132;
            channel2Max = 1.000;
            
            % Define thresholds for channel 3 based on histogram settings
            channel3Min = 0.035;
            channel3Max = 0.514;
            
            % Create mask based on chosen histogram thresholds
            BW = ( (I(:,:,1) >= channel1Min) | (I(:,:,1) <= channel1Max) ) & ...
                (I(:,:,2) >= channel2Min ) & (I(:,:,2) <= channel2Max) & ...
                (I(:,:,3) >= channel3Min ) & (I(:,:,3) <= channel3Max);
            
            % Invert mask
            BW = ~BW;
            
        end
        
        
    end
    
end

