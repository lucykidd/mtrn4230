classdef Base < BaseInterface
    % BASE : A base class from which every other class extends (inherits).
    % I.e every other class is a Base class as well and thus will have
    % access to any public and protected functions defined here. This class
    % is used to store common methods and procedures to all classes.
    %
    % Usage : If you need to use any of the methods in this class from your
    % class, just call 'this.methodName(...variables....);'
    %
    % Author : Lindsay
    % Date : 24/03/2015
    
    %% Properties
    properties (Access = public)
    end
    
    properties (Access = protected)
    end
    
    %% Public Methods
    methods (Access = public)
        
        function this = Base(this)
            % Base Class Constructor
            % Author : Lindsay Folkard
            % Purpose : construct the Base object from which everything is
            % derived.
            % Date : 25/04/15
            
            this.name='Base'; % override in your own class
            this.printState=0;
            this.outHandle=1;
            this.errorHandle=2;
            this.componentState=1; % Component is judged to be good at startup...
            this.managerHandle=[]; % no managerHandle as of yet...
            
            % Determine the relative paths for our new files
            absPath = which('infoMain.txt');
            if ~isempty(absPath)
                if ispc() % if windows paths are as follows
                    this.outPath = strrep(absPath,'infoMain.txt','Out\');
                    this.errorPath = strrep(absPath,'infoMain.txt','Error\');
                else % Assume mac or linux - file descriptions are inverete
                    this.outPath = strrep(absPath,'infoMain.txt','Out/');
                    this.errorPath = strrep(absPath,'infoMain.txt','Error/');
                end;
            else
                this.outPath = 'Out/';
                this.errorPath = 'Error/';
            end;
            
        end
        
        function setErrorState(this,state)
            % setErrorState
            % Author : Lindsay
            % Purpose : sets the error state of this class... this is used to
            % update the manager about whether this class has a problem...
            % Udage: state is 1 for good, 0 for error
            % Date : 25/04/15
            
            try
                this.componentState=state;
                % prompt the manager to update my state...
                this.log(strcat('Error state of',this.name,'has been changed..'));
                
                if ~isempty(this.managerHandle)
                    this.managerHandle.updateComponentState(this.name,state);
                    this.log('Updated manager error state successfully');
                end
                
            catch e
                this.error('Unable to update manager error state!!');
            end
        end;
        
        function this = setManagerHandle(this,managerHandle)
            % setManagerHandle
            % Purpose : Function will set the manager handle of this class..
            % this class can now talk back to the manager...useful but could be
            % abused...might need to limit access to the manager methods...
            % Usage : this.setManagerHandle(manager) , where manager is a handle
            % to our manager class...
            % Author : Lindsay
            
            this.managerHandle = managerHandle;
        end
        
        function delete(this)
            % Base Class Destructor
            % Purpose : Destroy the base class object..
            % Author : Lindsay Folkard
            % Date : 25/04/15
            
            this.log('Begin Destroying this Object.....');
            
            % 2. Clean up any memory...
            % TODO
            
            % 3. Close any used files...
            this.outState(1);
        end;
        
        function outState(this,state)
            % outState
            % Purpose : This sets the output of the current class to either
            % print to a log file, console or not at all.
            % Usage: outState : 1 means print to log file, 0 means use console... < 0=
            % don't print anywhere...
            % Author : Lindsay
            % Date : 25/04/15
            
            if this.printState <= 0 && state > 0
                % switch to log print state...
                % need to close current file handle and then
                try
                    oldOut=this.outHandle;
                    oldError=this.errorHandle;
                    
                    this.outHandle = fopen(strcat(this.outPath,this.name,'_out.log'),'w'); % append the file
                    this.errorHandle = fopen(strcat(this.errorPath,this.name,'_err.log'),'w');
                    
                    if state == 0
                        % previously in standard log mode...
                        fclose(oldOut);
                        fclose(oldError);
                    end
                    
                    if this.outHandle == -1 || this.errorHandle == -1
                        % Can't actually find the path...
                        % Hacky solution...
                        this.outHandle=1;
                        state=1;
                        this.errorHandle=2;
                        this.error('Unable to redirect output to files!!! Still Stdout & stderr');
                    else
                        disp(strcat('Redirecting ',this.name ,' Output & error to file : ',this.outPath));
                    end;
                catch stuff
                    this.outHandle=1;
                    this.errorHandle=2;
                    this.error('Unable to open log and error files',0);
                    return;
                end
            elseif this.printState < 0 && state == 0
                % we are entering the console log mode...from no logging or log
                try
                    fclose(this.outHandle);
                    fclose(this.errorHandle);
                catch err
                    disp('error closing files');
                    return;
                end
                this.outHandle=1;% stdout
                this.errorHandle=2;% stderr
            elseif state < 0 && this.printState >= 0
                % we want to disable logging
                try
                    fclose(this.outHandle);
                    fclose(this.errorHandle);
                catch err
                    
                end
            end;
            this.printState=state; % set our new print state to this..
        end
        
        function log (this,message,varargin)
            % log : prints a message (string) and any variables to console or file.
            % Usage : this.log(messageString,(nothing or vector or matrice)
            % Author : Lindsay
            % Date : 24/03/2015
            
            try
                this.printStuff(this.outHandle,'log',message,nargin-2,varargin);
            catch e
                disp(strcat('Problem with log message function for',this.name));
            end
        end
        
        function error (this,message,errorCode,varargin)
            % error : lets the system know that we have had an error, given by
            % a message and a code ..
            % Usage : this.error('Gui failed somehow',0)
            % Author : Lindsay , Date : 24/03/2015
            try
                this.printStuff(this.errorHandle,'error',message,nargin-3,varargin); % call our print function
                this.printStuff(this.outHandle,'log ERROR - ',message,nargin-2,varargin);
                
                % Send the message to manager which will then send it to the gui...
                if ~isempty(this.managerHandle)
                    message = sprintf('Error : %s : %s',this.name,message);
                    this.managerHandle.componentError(this.name,message);
                end
                
            catch e
                disp(strcat('Problem with error message function for',this.name));
                disp(strcat('err : ',getReport(e)));
            end
        end
        
        function consoleLog(this,message,varargin)
            % consoleLog : this prints important messages to our gui command console
            % Usage : this.consoleLog(message,variables..) where message is a
            % string and the variables are vectors...
            % Author : Lindsay
            % Date : 25/05/15
            
            % First Log the message
            this.log(strcat('Important :',message));
            
            try
                % Send the message to manager which will then send it to the gui...
                if ~isempty(this.managerHandle)
                    
                    % Append name to message:
                    message = sprintf('log : %s : %s',this.name,message);
                    this.managerHandle.componentLog(this.name,message);
                end
                
            catch e
                e
                this.error(strcat('Unable to send console message to console...',message));
            end
        end
        
    end
    
    %% Protected Methods..
    methods (Access = protected)
        
        function printStuff (this,fileHandle,type,message,num,varargin)
            % PrintStuff: Generic function used to print information to a
            % specific file
            % Usage : prints data in a message along with the time to a given
            % fileHandle....
            % Author : Lindsay
            % Date : 25/04/15
            
            if this.printState < 0 % don't print anything
                return;
            else
                t=clock();
                if num <= 0 % Just print a message, no arguments
                    fprintf(fileHandle,'%d:%d:%2.0f - ',t(4),t(5),t(6));
                    fprintf(fileHandle,strcat( this.name,' ',type,' : ',message,' \n'));
                else % print a message with the data
                    format = ' %3.2f'; % TODO - improve this to differentiate types
                    fprintf(fileHandle,'%d:%d:%2.0f - ',t(4),t(5),t(6));
                    fprintf(fileHandle,strcat( this.name,' ',type,' : ',message, ' = ['));
                    fprintf(fileHandle,format,cell2mat(varargin{1}));
                    fprintf(fileHandle,' ]\n');
                end;
            end;
        end
        
    end
    
    methods (Access = private)
        % Only this class can seee and use these methods...
    end
    
end

