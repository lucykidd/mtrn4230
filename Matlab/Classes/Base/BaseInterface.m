classdef (Abstract) BaseInterface < handle
    % BASEINTERFACE : an interface (like a header file in c++) that 
    % describes the basic methods which are available to every class that we have.
    %
    % Usage : Basically, any methods in the concrete class which will be 
    % public should be put in the methods(Abstract) area so that we can see  
    % how to call them quickly and easily. The actual code implementing 
    % these methods will be in the concrete class. 
    % This is technically an "abstract class" and so must be extended
    % by a concrete class like Base (Don't worry...this should already be
    % done--> hence classdef Base < BaseInterface )
    %
    % Author : Lindsay
    % Date : 24/03/2015
    % NB: This extends the handle superclass which therein enables 
    
    properties (Access = protected)
        name; % name of the object --> used for determining outfile
        outPath; %outPath for the logs
        outHandle;% handle to out file
        errorPath; % errorPath for the logs...
        errorHandle;% handle to error file
        printState; % < 0 = no logs, 0 = terminal/console, 1=log file..
        managerHandle; % handle to the manager class....
        componentState; % state of this component , 1 = good, -1 = error
    end
    
    methods (Abstract)
    % Note : All Methods here are just definitions
    % Do not use a function...end block to define an abstract method, use only the method signature.
        
        outState(this,state)
        % outState : changes were the logs and errors will be printed (console,file,no
        % Usage : this.logState(state)
        % this.outState(1) all output from log and error calls is put on the matlab console.
        % this.outState(0) all log & error output sent to the out and error files respectively. 
        % this.outState(-1) prevents any of the logs or errors being printed
        % Author : Lindsay ,  Date : 24/03/2015
        
        log (this,message,varargin)
        % log : prints a message (string) and any variables to console or file. 
        % Usage : multiple
        % this.log('Error in code here')
        % this.log('Chocolate positions',chocolatePositions)
        % Author : Lindsay  , Date : 24/03/2015
        
        error (this,message,errorCode,varargin)
        % error : lets the system know that we have had an error, given by
        % a message and a code ..
        % Usage : this.error('Gui failed somehow',0)
        % For now the code is just 0 (or any number youlike), in the future we may define proper
        % error codes for given classes...
        % Author : Lindsay , Date : 24/03/2015
        
    end
    
    % !!!! Please try not add any concrete methods here... do that in the
    % Base concrete class
end

