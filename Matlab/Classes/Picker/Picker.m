classdef Picker < Process
    % Picker : This class does all the work of deciding how we are to
    % pickup a bunch of chocolates, where we will place them and if they
    % are pickable. It is to communicate with the GUI,vision and with the robot
    % controller. Hence, it will recieve chocolate locations from the
    % vision module, it will then process these chocolate locations and
    % current input from the gui to decide if picking the chocolates is
    % feasible.. it will then execute a picking plan if needed.
    %
    % Due to the initial plan of making multiple threads, each function has
    % its own state. This allows the program to "stop and resume" from the
    % same position without blocking the rest of the program.
    %
    % Author : Pat & Lindsay
    % Date : 30/05/2015
    
    
    %% Properties
    properties (Access = public)
        % no public properties
    end
    
    properties (Access = private)
        % Handles to our respective objects...
        gui;
        vision;
        robController;
        
        % stacks of chocolates
        tableStacks;
        boxStacks;
        convStack; % number of chocolates on the conveyor
        
        % positions for the stacks, robot frame.
        stacksX;
        stacksY;
        convStackX;
        convStackY;
        
        % various needed heights
        chocThick;
        tableHeight;
        conHeight;
        
        % needed for flipping
        halfChocLength;
        
        %related to order
        sequence; % sequence to do
        sequencedOrder; % does customer want a sequence?
        order; % actual quantities
        
        % storing info we've been given...
        Chocs;
        
        
        %state variables
        orderMade;  % has an order been made
        execOrder; % do we have to execute it ?
        orderPossible; % can we fulfill ?
        mainState; % state of main function
        flipAllState; % state of flipAllChocolates function
        flipState; % state of flipOne function
        stackAllState; % same but stack all
        stackState;
        unloadAllState; % same but unload all
        unloadState;
        loadAllState; % to load chocolates
        loadState;
        toSafeState; % towards safe position
        toFlipState; % towards flip position
        totalChocToFlip; % stores how many inverted chocolates we have
        cleanStackState;
        moveChocState; % state of a simple movement function.
        
        currentMoveChoc; % to know which of the chocolates we're moving
        currentStackChoc; % to know what chocolate we're picking up
        currentFlipChoc; % to know what chocolate we're flipping
        currentBoxEmptyChoc; % to know what position in the box we're emptying
        currentBoxFillChoc; % to know what position in the box we're filling
        currentBox; % to know what box we're emptying...
        
        
        % values where we have to remove chocolates in (each is [min,max])
        xClean;
        yClean;
        
        % are we acquiring new chocolates? Added due to matlab issue with
        % timers
        chocFlag;
        
        movePositions; % positions where to move the chocolates to
        boxEmptyingPositions; % positions where to put the chocolates from the box
        
    end
    
    %% Methods
    methods (Access = public)
        
        
        %% Initialisation and Destruction Methods
        % Author: Lindsay & Patrick
        % 30-05-2015
        % initializes all variables.
        function this = Picker(varargin)
            % Class Constructor
            
            this.name = 'Picker';
            this.log('Create the picker ...');
            this.error('No error...');
            
            % assuming all stacks of chocolates are empty.
            this.tableStacks = [0,0,0,0];
            this.boxStacks = [0,0,0,0];
            
            % settings the positions of the stacks
            this.stacksX = 200;
            this.stacksY = [100,200,300,400];
            
            % setting heights
            this.chocThick = 7;
            this.tableHeight = 152;
            this.conHeight = 22;
            this.halfChocLength = 40;
            
            % assuming no order yet
            this.orderMade = 0;
            this.orderPossible = 0;
            this.sequencedOrder = 0;
            this.sequence = [];
            this.execOrder = 0;
            
            this.chocFlag = 0;
            
            % setting the machine in a ready state
            this.mainState = 0;
            this.flipAllState = 0;
            this.flipState = 0;
            this.stackAllState = 0;
            this.stackState = 0;
            this.unloadAllState = 0;
            this.unloadState = 0;
            this.loadAllState = 0;
            this.loadState = 0;
            this.toSafeState = 0;
            this.toFlipState = 0;
            this.Chocs = [];
            this.currentFlipChoc = 1;
            this.currentStackChoc = 1;
            this.currentBoxFillChoc = 1;
            this.currentBoxEmptyChoc = 4; % modify to 8 for asst 4
            this.cleanStackState = 0;
            this.moveChocState = 0;
            
            this.convStack = 0; % amount of chocolates on the conveyor stack
            this.convStackX = -450; % coordinates of the stack
            this.convStackY = 140;
            
            this.xClean = [20,350];
            this.yClean = [0,500];
            this.movePositions = [200, -50;
                400, -50;
                200, -200;
                400, -200;
                200, -350;
                350, -350];
            
            this.boxEmptyingPositions = [200, -50;
                200, -125;
                200, -200;
                200, -275;
                400, -50;
                400, -125;
                400, -200;
                400, -275]; % chocolates will be aligned with X axis.
            this.setRegularTask(0,0.5,0.33);
            this.run();
        end
        
        % Author: Lindsay
        % 30-05-2015
        % destructor, but not needed. Left as it might be needed later on.
        function delete(this)
            % do nothing
        end;
        
        % Author: Lindsay
        % 30-05-2015
        % adds required handles to other objects
        function addHandles(this,guiHandle,robControllerHandle,visionHandle)
            this.gui=guiHandle;
            this.robController=robControllerHandle;
            this.vision=visionHandle;
        end
        
        
        % Author: Patrick
        % 30-05-2015
        % runs periodically
        % tries to fulfill the order if possible, else signals it cannot be
        % done.
        function out = mainTask(this,timerObj,event,string_arg)
            
            if(this.orderMade) % if an order has been made
                if(this.robController.isReady() && this.execOrder && (this.chocFlag == 0)) % is the robot ready
                    this.robController.enableVacuumPump();
                    this.robController.setLinearMode(0);
                    
                    switch(this.mainState)
                        case -1 % just to go and get a picture of the box...
                            this.maxSpeed();
                            this.robController.moveJPos([0,0,0,0,0,0]);
                            this.mainState = 0;
                        case 0 % getting ready for a new picture
                            this.vision.triggerProcessConvIm();
                            this.gui.displayPickerState(6);
                            this.maxSpeed();
                            this.toSafePos();
                            if this.toSafeState == 0
                                this.mainState = 1;
                            end
                            if this.isOrderFeasibleFromStacks(this.order)
                                this.mainState = 14; % we somehow can do it ... go to emptying the box
                                this.gui.displayPickerState(7); % send to gui that it's possible
                            end
                        case 1 % new picture
                            this.log('first picture');
                            this.getNewChocs();
                            this.orderPossible = this.isOrderFeasible(this.Chocs,this.order);
                            % send correct value back to GUI here
                            if this.orderPossible == 1
                                this.gui.displayPickerState(7); % send to gui that we can do it
                            end
                            this.mainState = 2;
                            this.gui.displayPickerState(0);
                            
                        case 2 % cleaning stacking area
                            this.log('cleaning stacking zone');
                            this.cleanStackArea();
                            if(this.cleanStackState == 0)
                                this.mainState = 3;
                            end
                            
                        case 3 % going to safe position
                            this.gui.displayPickerState(6);
                            this.toSafePos();
                            if this.toSafeState == 0
                                this.mainState = 4;
                            end
                            
                        case 4 % just taking a picture
                            % there shouldn't be any new info from this
                            % picture, so we're not updating the GUI
                            this.log('second picture');
                            this.getNewChocs();
                            
                            % filtering out the stacks, not too sure why I
                            % make a new variable...
                            chocs = this.Chocs;
                            II = find(((this.Chocs(:,1) < this.xClean(1)) | (this.Chocs(:,1) > this.xClean(2))) | ((this.Chocs(:,2) < this.yClean(1)) | (this.Chocs(:,2) > this.yClean(2))));
                            this.Chocs = chocs(II,:);
                            
                            this.mainState = 5;
                            
                        case 5 % fill stacks
                            this.gui.displayPickerState(1);
                            this.log('filling stacks');
                            this.fillStacks(this.Chocs);
                            if this.stackAllState == 0
                                this.mainState = 6;
                                this.log('stacks: ', this.tableStacks);
                                this.log('order: ', this.order);
                                if this.isOrderFeasibleFromStacks(this.order)
                                    this.mainState = 14; % this is where we empty the box
                                end
                            end
                            
                        case 6 % picture
                            this.toSafePos();
                            if this.toSafeState == 0
                                this.mainState = 7;
                            end
                        case 7  % picture
                            this.gui.displayPickerState(6);
                            this.log('third picture');
                            this.getNewChocs(); % remove the ones that are stacked!
                            chocs = this.Chocs; % filtering out the stacks
                            % keeping the ones that have: x smaller than
                            % xmin or bigger than xmax
                            % AND y smaller than ymin or bigger than ymax
                            if(~isempty(chocs))
                                this.orderPossible = this.isOrderFeasible(this.Chocs,this.order);
                                if this.orderPossible == 1
                                    this.gui.displayPickerState(7); % send back to GUI
                                end
                                
                                II = find(((this.Chocs(:,1) < this.xClean(1)) | (this.Chocs(:,1) > this.xClean(2))) | ((this.Chocs(:,2) < this.yClean(1)) | (this.Chocs(:,2) > this.yClean(2))));
                                this.Chocs = chocs(II,:);
                            end
                            this.mainState = 8;
                        case 8
                            this.gui.displayPickerState(2);
                            this.log('trying to stack potential overlaps');
                            this.fillStacks(this.Chocs);
                            if this.stackAllState == 0
                                this.mainState = 9;
                                if this.isOrderFeasibleFromStacks(this.order)
                                    this.mainState = 14; % this is where we empty the box
                                end
                            end
                        case 9
                            this.gui.displayPickerState(6);
                            this.toSafePos();
                            if this.toSafeState == 0
                                this.mainState = 10;
                            end
                        case 10
                            this.log('fourth picture');
                            this.getNewChocs(); % remove the ones that are stacked!
                            chocs = this.Chocs; % filtering out the stacks
                            % keeping the ones that have: x smaller than
                            % xmin or bigger than xmax
                            % AND y smaller than ymin or bigger than ymax
                            II = find(((this.Chocs(:,1) < this.xClean(1)) | (this.Chocs(:,1) > this.xClean(2))) | ((this.Chocs(:,2) < this.yClean(1)) | (this.Chocs(:,2) > this.yClean(2))));
                            this.Chocs = chocs(II,:);
                            
                            this.mainState = 11;
                            
                        case 11 % flipping
                            this.gui.displayPickerState(3);
                            this.log('flipping chocolates');
                            this.flipAllChocolates(this.Chocs);
                            if this.flipAllState == 0 % this means they're all flipped
                                this.mainState = 12;
                                this.log('done flipping chocolates');
                                if this.isOrderFeasibleFromStacks(this.order) % send feedback to gui
                                    this.gui.displayPickerState(7);
                                    this.mainState = 14; % this is where we empty the box
                                end
                            end
                        case 12
                            this.gui.displayPickerState(6);
                            this.toSafePos();
                            if this.toSafeState == 0
                                this.mainState = 14;
                            end
                            % jump here due to a ghost state
                            
                        case 14 % emptying the box
                            % also have to allow this to jump to fill box
                            this.gui.displayPickerState(4);
                            this.log('emptying box');
                            B =  this.robController.getBoxes();
                            Box = B.boxLocations;
                            Box = this.conCam2Robot(Box);
                            Box = [Box, B.boxRotations];
                            Box(3) = Box(3)*pi/180; % this might be wrong
                            
                            
                            % don't forget to change the frame
                            this.unloadChocolatesFromBox(Box(1),Box(2),Box(3));
                            if this.unloadAllState == 0
                                if this.isOrderFeasibleFromStacks(this.order)
                                    this.mainState = 26; % fill the box
                                else
                                    this.mainState = 15; % emptied the box
                                    this.log('done unloading box');
                                end
                            end
                            
                        case 15
                            this.gui.displayPickerState(6);
                            this.toSafePos();
                            if this.toSafeState == 0
                                this.mainState = 16;
                            end
                            
                        case 16 % just looking what is available
                            this.getNewChocs(); % remove the ones that are stacked!
                            
                            chocs = this.Chocs; % filtering out the stacks
                            % keeping the ones that have: x smaller than
                            % xmin or bigger than xmax
                            % AND y smaller than ymin or bigger than ymax
                            II = find(((this.Chocs(:,1) < this.xClean(1)) | (this.Chocs(:,1) > this.xClean(2))) | ((this.Chocs(:,2) < this.yClean(1)) | (this.Chocs(:,2) > this.yClean(2))));
                            this.Chocs = chocs(II,:);
                            
                            % send correct value back to GUI here
                            this.mainState = 17;
                            this.orderPossible = this.isOrderFeasible(this.Chocs,this.order);
                            if this.orderPossible
                                this.gui.displayPickerState(7);
                            end
                        case 17 % stacking chocolates from the box
                            this.gui.displayPickerState(1);
                            this.log('stacking chocolates from the box');
                            this.fillStacks(this.Chocs);
                            if this.stackAllState == 0
                                this.mainState = 18;
                                if this.isOrderFeasibleFromStacks(this.order)
                                    this.mainState = 26;
                                    % might need to send something to GUI
                                end
                            end
                            
                        case 18 % getting ready for a new picture
                            this.gui.displayPickerState(6);
                            this.toSafePos();
                            if this.toSafeState == 0
                                this.mainState = 19;
                            end
                            
                        case 19 % getting ready to flip the chocolates that were in the box
                            this.log('6th picture'); % remove the ones that are stacked!
                            this.getNewChocs();
                            
                            chocs = this.Chocs; % filtering out the stacks
                            % keeping the ones that have: x smaller than
                            % xmin or bigger than xmax
                            % AND y smaller than ymin or bigger than ymax
                            II = find(((this.Chocs(:,1) < this.xClean(1)) | (this.Chocs(:,1) > this.xClean(2))) | ((this.Chocs(:,2) < this.yClean(1)) | (this.Chocs(:,2) > this.yClean(2))));
                            this.Chocs = chocs(II,:);
                            
                            % send correct value back to GUI here
                            this.mainState = 20;
                            
                        case 20
                            this.gui.displayPickerState(3);
                            this.flipAllChocolates(this.Chocs);
                            if this.flipAllState == 0; % done flipping
                                this.mainState = 21;
                                if this.isOrderFeasibleFromStacks(this.order)
                                    this.mainState = 26;
                                    % might need to send to GUI as well
                                end
                                if this.convStack > 0
                                    this.gui.displayPickerState(9); % telling we're unstacking on conveyor...
                                end
                                
                            end
                            
                        case 21 % the box has been emptied, we only have the stack as last resort
                            this.emptyConvStack();
                            if this.convStack == 0
                                this.mainState = 22;
                            end
                        case 22
                            this.gui.displayPickerState(6);
                            this.toSafePos();
                            if this.toSafeState == 0
                                this.mainState = 23;
                            end
                        case 23 % get a picture
                            this.log('seventh pic');
                            this.getNewChocs(); % remove the ones that are stacked!
                            chocs = this.Chocs; % filtering out the stacks
                            % keeping the ones that have: x smaller than
                            % xmin or bigger than xmax
                            % AND y smaller than ymin or bigger than ymax
                            II = find(((this.Chocs(:,1) < this.xClean(1)) | (this.Chocs(:,1) > this.xClean(2))) | ((this.Chocs(:,2) < this.yClean(1)) | (this.Chocs(:,2) > this.yClean(2))));
                            this.Chocs = chocs(II,:);
                            this.orderPossible = this.isOrderFeasible(this.Chocs, this.order);
                            if this.orderPossible == 1
                                this.gui.displayPickerState(7);
                            end
                            this.mainState = 24;
                        case 24 % now we stack
                            this.gui.displayPickerState(1);
                            this.log('filling stacks');
                            this.fillStacks(this.Chocs);
                            if this.stackAllState == 0
                                this.mainState = 25;
                                this.log('stacks: ', this.tableStacks);
                                this.log('order: ', this.order);
                                if this.isOrderFeasibleFromStacks(this.order)
                                    this.mainState = 27; % this is where we fill the box
                                end
                            end
                        case 25 % yet another picture
                            this.log('eighth pic');
                            this.getNewChocs(); % remove the ones that are stacked!
                            chocs = this.Chocs; % filtering out the stacks
                            % keeping the ones that have: x smaller than
                            % xmin or bigger than xmax
                            % AND y smaller than ymin or bigger than ymax
                            II = find(((this.Chocs(:,1) < this.xClean(1)) | (this.Chocs(:,1) > this.xClean(2))) | ((this.Chocs(:,2) < this.yClean(1)) | (this.Chocs(:,2) > this.yClean(2))));
                            this.Chocs = chocs(II,:);
                            
                            this.mainState = 26;
                        case 26 % stack the flipped ones, this is our final chance.
                            this.gui.displayPickerState(3);
                            this.log('flipping chocolates');
                            this.flipAllChocolates(this.Chocs);
                            if this.flipAllState == 0 % this means they're all flipped
                                
                                this.log('done flipping chocolates');
                                if this.isOrderFeasibleFromStacks(this.order)
                                    this.mainState = 27; % this is where we fill the box
                                    % send feedback to gui
                                    this.gui.displayPickerState(7);
                                else % can't do it, send feedback to gui
                                    this.mainState = 0;
                                    this.execOrder = 0;
                                    this.cancelOrder();
                                    this.gui.displayPickerState(-1);
                                end
                            end
                            
                        case 27 % loading the box
                            this.gui.displayPickerState(5);
                            B =  this.robController.getBoxes();
                            Box = B.boxLocations;
                            Box = this.conCam2Robot(Box);
                            Box = [Box, B.boxRotations];
                            Box(3) = Box(3)*pi/180;
                            this.loadAllInBox(Box);
                            if this.loadAllState == 0
                                this.mainState = 28;
                                this.robController.disableVacuumPump();
                            end
                            
                        case 28 % sending the box out
                            B =  this.robController.getBoxes();
                            Box = B.boxLocations;
                            Box = this.conCam2Robot(Box);
                            % the box should be sent to x = -1600
                            x = Box(1);
                            distance = -1600 - x; % distance towards the center of the conveyor guard
                            this.robController.moveConveyorDistance(distance);
                            this.mainState = 0;
                            this.execOrder = 0;
                            this.orderMade = 0;
                            this.cancelOrder(); % reset.
                            this.robController.moveJPos([0,0,0,0,0,0]);
                            this.gui.displayPickerState(8);
                            
                    end
                end
            end
            return
        end;
        
        
        
        
        % Author: Patrick
        % 30-05-2015
        % this is to be called by the GUI when an order is made...
        % Always fill all arguments.
        function out = takeOrder(this,Chocolates,Quantities,Sequence,inSequence)
            this.log('new order');
            this.log('quantities (milk,dark,orange,mint) ',Quantities);
            this.log('sequence: ', Sequence);
            this.log('ordered? ', inSequence);
            this.log('Chocolates ', Chocolates);
            
            
            this.order = Quantities;
            this.sequence = Sequence;
            this.sequencedOrder = inSequence;
            this.orderMade = 1;
            this.execOrder = 1;
            this.flipAllState = 0;
            this.flipState = 0;
            this.loadAllState = 0;
            this.loadState = 0;
            this.unloadAllState = 0;
            this.unloadState = 0;
            this.stackAllState = 0;
            this.stackState = 0;
            this.toSafeState = 0;
            this.toFlipState = 0;
            
            if(isempty(this.order))
                this.log('quantities are empty, y u hate the unicorns?');
                this.orderMade = 0;
            end
            
            return
        end;
        
        
        % Author: Patrick
        % 30-05-2015
        % stops the execution of the order
        function out = pauseOrder(this)
            this.log('order paused');
            this.execOrder = 0;
            return
        end;
        
        % Author: Patrick
        % 30-05-2015
        % allows robot to continue its task
        function out = resumeOrder(this)
            this.log('order resumed');
            this.execOrder = 1;
            return
        end;
        
        % Author: Patrick
        % 30-05-2015
        % reset everything and stop order execution.
        function out = cancelOrder(this)
            
            this.sequence = [];
            this.sequencedOrder = 0;
            this.order = [];
            
            this.currentMoveChoc = 1;
            this.currentStackChoc = 1;
            this.currentFlipChoc = 1;
            this.currentBoxEmptyChoc = 1;
            this.currentBoxFillChoc = 1;
            
            this.orderPossible = 0;
            this.cleanStackState = 0;
            this.moveChocState = 0;
            this.chocFlag = 0;
            this.orderMade = 0;
            this.execOrder = 0;
            this.mainState = 0;
            
            this.flipAllState = 0;
            this.flipState = 0;
            
            this.loadAllState = 0;
            this.loadState = 0;
            
            this.unloadAllState = 0;
            this.unloadState = 0;
            
            this.stackAllState = 0;
            this.stackState = 0;
            
            this.toSafeState = 0;
            this.toFlipState = 0;
            
            return
        end;
        
        
        % Author: Patrick
        % 30-05-2015
        % gets all chocolates from the stacking area, regardless of
        % orientation or if they just happen to be in the right spot,
        % doesn't assume stacking.
        % Chocolates are put to the conveyor
        function cleanStackArea(this)
            if(~isempty(this.Chocs))
                
                II = find((this.Chocs(:,1) > this.xClean(1)) & (this.Chocs(:,1) < this.xClean(2)) & (this.Chocs(:,2) > this.yClean(1)) & (this.Chocs(:,2) < this.yClean(2)));
                
                % make sure to get only the ones we can grab...
                if(~isempty(II))
                    toClean = this.Chocs(II,:);
                    this.log('in Clean Stack State');
                    this.log('toClean ', toClean);
                    if ~isempty(toClean)
                        switch this.cleanStackState
                            case 0
                                this.currentMoveChoc = 1;
                                this.log('begin cleaning stack area');
                                idx = II(this.currentMoveChoc);
                                % might have to turn by something else than
                                % just theta...
                                
                                this.moveOneChocolate(this.Chocs(idx,1),this.Chocs(idx,2),this.tableHeight, ...
                                    this.convStackX,this.convStackY,this.conHeight+this.convStack*this.chocThick,this.Chocs(idx,3));
                                this.cleanStackState = 1;
                            case 1
                                idx = II(this.currentMoveChoc);
                                this.moveOneChocolate(this.Chocs(idx,1),this.Chocs(idx,2),this.tableHeight, ...
                                    this.convStackX,this.convStackY,this.conHeight+this.convStack*this.chocThick,this.Chocs(idx,3));
                                if this.moveChocState == 0 % done with the chocolate
                                    this.log('increment stack area cleaning');
                                    this.currentMoveChoc = this.currentMoveChoc + 1;
                                    this.convStack = this.convStack +1; % increment number of chocs on the conveyor
                                    if this.currentMoveChoc > length(toClean(:,1)) % done with all
                                        this.cleanStackState = 0;
                                        this.currentMoveChoc = 1;
                                    end
                                end
                        end
                    end
                end
            end
            return
        end;
        
        
        % Author: Patrick
        % 30-05-2015
        % gets all chocolates from the conveyor and puts them back on the
        % table
        function emptyConvStack(this)
            if this.convStack > 0
                this.moveOneChocolate(this.convStackX,this.convStackY,this.conHeight+this.convStack*this.chocThick, ...
                    this.boxEmptyingPositions(mod(this.convStack,8)+1,1),this.boxEmptyingPositions(mod(this.convStack,8)+1,2),this.tableHeight ,0);
                if this.moveChocState == 0
                    this.convStack = this.convStack -1; % we're done moving one
                end
            end
            return
        end
        
        
        % Author: Patrick
        % 30-05-2015
        % moves one chocolate from x,y,z and put in in toX,toY,toZ,
        % applying a rotation by theta (rad)
        function moveOneChocolate(this,x,y,z,toX,toY,toZ,theta)
            this.log('move one chocolate x,y,z, toX,toY,toZ,theta',[x,y,z,toX,toY,toZ,theta]);
            switch this.moveChocState
                case 0 % initial position
                    this.maxSpeed();
                    this.log('to calib pos');
                    this.robController.moveJPos([0,0,0,0,0,0]);
                    this.moveChocState = 1;
                case 1 % to chocolate
                    this.maxSpeed();
                    this.log('moving to x y z', [x,y,z+100]);
                    this.robController.moveCartPos([x,y,z+100]);
                    this.moveChocState = 2;
                case 2 % getting it
                    this.minSpeed();
                    this.robController.enableSuction();
                    this.log('getting the choc');
                    this.robController.moveCartPos([x,y,z]);
                    this.moveChocState = 3;
                case 3 % back up
                    this.minSpeed();
                    this.log('back up');
                    this.robController.moveCartPos([x,y,z+100]);
                    this.moveChocState = 4;
                case 4 % going to destination
                    this.maxSpeed();
                    this.log('moving to xyz to drop chocolate', [toX,toY,toZ+100]);
                    this.robController.moveCartPos([toX,toY,toZ+100]);
                    this.moveChocState = 5;
                case 5 % rotation
                    this.maxSpeed();
                    J = this.robController.getCurJoints();
                    J(6) = J(6) + theta*180/pi;
                    this.robController.moveJPos(J);
                    this.moveChocState = 6;
                case 6 % go down
                    this.minSpeed();
                    this.log('moving down');
                    this.robController.moveCartPosNoOrient([toX,toY,toZ]);
                    this.moveChocState = 7;
                case 7 % drop
                    this.robController.disableSuction();
                    this.log('dropped');
                    this.moveChocState = 8;
                case 8 % go back up
                    this.maxSpeed();
                    this.robController.moveCartPosNoOrient([toX,toY,toZ+100]);
                    this.log('went back up, done');
                    this.moveChocState = 0;
            end
            
            return
        end;
        
        
        % Author: Patrick
        % 30-05-2015
        % fills the box with the desired quantities.
        function loadAllInBox(this,box)
            BPos = this.getBoxPositions(box(1),box(2),box(3));
            
            switch this.loadAllState
                case 0 % we haven't started yet
                    this.currentBoxFillChoc = 1; %
                    if this.sequencedOrder == 0 % if we don't want a sequence, we'll still make one...
                        seq = zeros(1,12);
                        milkSeq = ones(1,this.order(1));
                        darkSeq = 2*ones(1,this.order(2));
                        orangeSeq = 3*ones(1,this.order(3));
                        mintSeq = 4*ones(1,this.order(4));
                        
                        this.log('milk',milkSeq);
                        this.log('dark',darkSeq);
                        this.log('orange',orangeSeq);
                        this.log('mint',mintSeq);
                        seq = [milkSeq, darkSeq, orangeSeq, mintSeq];
                        
                        this.log('seq',seq);
                        
                        II = find(seq > 0);
                        seq = seq(II);
                        this.sequence = seq;
                        
                    end
                    
                    % we can now safely assume there's a sequence...
                    posInBox = mod(this.currentBoxFillChoc,4)+1;
                    this.boxStacks = [0,0,0,0]; % box is empty
                    this.log('begin putting the first chocolate in the box...');
                    this.log('complete data dump: ');
                    this.log('box: ', box);
                    this.log('posInBox: ', posInBox);
                    this.log('sequence: ', this.sequence);
                    this.log('currentBoxFillChoc: ', this.currentBoxFillChoc);
                    this.log('in sequence: ', this.sequencedOrder);
                    this.loadOneInBox(box,this.sequence(this.currentBoxFillChoc),posInBox);
                    this.loadAllState = 1;
                    
                case 1
                    posInBox = mod(this.currentBoxFillChoc,4)+1;
                    
                    this.loadOneInBox(box,this.sequence(this.currentBoxFillChoc),posInBox);
                    
                    if(this.loadState == 0 && this.toSafeState == 0) % safeState, or we get glitches...
                        this.boxStacks(posInBox) = this.boxStacks(posInBox) + 1;
                        this.currentBoxFillChoc = this.currentBoxFillChoc + 1;
                        this.log('stacks in box: ', this.boxStacks);
                        this.log('current fill Choc: ', this.currentBoxFillChoc);
                        this.log('sequence: ', this.sequence);
                        this.log('length of sequence: ', length(this.sequence));
                        if this.currentBoxFillChoc > length(this.sequence)
                            % this was the last one
                            this.loadAllState = 0;
                            this.log('got through the order...');
                        end
                    end
            end
            
            return
        end
        
        
        % Author: Patrick
        % 30-05-2015
        % loads one chocolate in the position "posInBox", takes it from the
        % stack corresponding to "flavour". box = [x,y,theta]
        function loadOneInBox(this,box,flavour,posInBox)
            BPos = this.getBoxPositions(box(1),box(2),box(3));
            switch this.loadState
                case 0 % just started, safe spot
                    this.maxSpeed();
                    this.toSafePos();
                    this.log('going to safe spot');
                    if this.toSafeState == 0
                        this.loadState = 1;
                    end
                case 1
                    % robot is in safe spot, move above the stack we want
                    this.log('going to the stack');
                    this.maxSpeed();
                    this.robController.moveCartPos([this.stacksX,this.stacksY(flavour),this.tableHeight+130]);
                    this.loadState = 2;
                case 2
                    % grab it
                    this.log('got the chocolate');
                    this.minSpeed();
                    this.robController.enableSuction();
                    this.robController.moveCartPos([this.stacksX,this.stacksY(flavour),this.tableHeight + this.chocThick*this.tableStacks(flavour)]);
                    this.loadState = 3;
                case 3
                    % go back up
                    this.log('going back up');
                    this.minSpeed();
                    this.robController.moveCartPos([this.stacksX,this.stacksY(flavour),this.tableHeight + 130]);
                    this.loadState = 4;
                case 4
                    % to safe spot
                    this.maxSpeed();
                    this.robController.moveCartPos([0,250,250]);
                    this.log('going to safe spot');
                    this.loadState = 5;
                    
                case 5 % above the box
                    this.log('me on the box');
                    this.maxSpeed();
                    this.robController.moveCartPos([BPos(posInBox,1),BPos(posInBox,2),this.conHeight+100]);
                    this.loadState = 6;
                case 6
                    
                    this.maxSpeed();
                    % take care of rotation here
                    T = [-1,0,0,BPos(1);
                        0,1,0,BPos(2);
                        0,0,-1,this.conHeight+100;
                        0,0,0,1];
                    
                    
                    T=T*trotz(BPos(posInBox,3))*troty(-pi/6); % should give what we want
                    % then add a rotation of theta of -theta on joint 6...
                    q = Quaternion(T);
                    quat = [q.s, q.v(1),q.v(2),q.v(3)];
                    this.robController.setLinearMode(0);
                    this.robController.moveCartQuatPos(BPos(posInBox,1),BPos(posInBox,2),this.conHeight+100,quat);
                    this.loadState = 7;
                    this.log('rotation of: ', [BPos(posInBox,1),BPos(posInBox,2),this.conHeight+100]);
                    this.log('quat: ', quat);
                case 7
                    this.log('dropping down');
                    this.minSpeed();
                    this.robController.moveCartPosNoOrient([BPos(posInBox,1),BPos(posInBox,2),this.conHeight + this.boxStacks(posInBox)*this.chocThick + 20]);
                    this.loadState = 8;
                case 8
                    this.log('done');
                    this.robController.disableSuction();
                    pause(0.15);
                    this.minSpeed();
                    this.robController.moveCartPosNoOrient([BPos(posInBox,1),BPos(posInBox,2),this.conHeight + 200]);
                    this.loadState = 0;
            end
            
            return
        end;
        
        % Author: Patrick
        % 30-05-2015
        % tells us if an order can be achieved using everything we see
        % (stacked chocolates + picture)
        function possible = isOrderFeasible(this,Chocolates,Order)
            % order is a vector of length 4, containing values 0 to 6. Order is
            % [nbrMilk, nbrDark, nbrOrange, nbrMint]
            
            milk = 1;
            dark = 2;
            orange = 3;
            mint = 4;
            
            % keeping only the reachable ones. If it's not pickable, it'll probably
            % become pickable later on
            II = find(Chocolates(:,8) == 1);
            
            Chocolates = Chocolates(II,:);
            
            % looking for milk
            II = find(Chocolates(:,6) == milk);
            if (length(II) + this.tableStacks(1)) < Order(1)
                % Sorry bud, can't do that for you
                possible = 0;
                return;
            end
            
            % dark...
            II = find(Chocolates(:,6) == dark);
            if (length(II) + this.tableStacks(2)) < Order(2)
                % Sorry bud, can't do that for you
                possible = 0;
                return;
            end
            
            % orange
            II = find(Chocolates(:,6) == orange);
            if (length(II) + this.tableStacks(3)) < Order(3)
                % Sorry bud, can't do that for you
                possible = 0;
                return;
            end
            
            % mint
            II = find(Chocolates(:,6) == mint);
            if (length(II) + this.tableStacks(4)) < Order(4)
                % Sorry bud, can't do that for you
                possible = 0;
                return;
            end
            
            % if we didn't quit by here, it's possible
            possible = 1;
            return
            
        end;
        
        
        % Author: Patrick
        % 30-05-2015
        % tells if we have enough chocolates on the stack to fulfill Order
        function possible = isOrderFeasibleFromStacks(this,Order)
            possible = 0;
            
            II = find(this.tableStacks < Order);
            if isempty(II)
                possible = 1;
            end
            return
        end;
        
        % Author: Patrick
        % 30-05-2015
        % given a box at x,y,theta, returns BPos = 4*[x,y,theta] the slots
        % of the chocolates in the box
        function BPos = getBoxPositions(this,x,y,theta)
            % x,y centre of the box
            % theta angle of the main axis
            % BPos is 4*3, with BPos(1,:) = [x,y,theta] of the position B1
            
            % the box is 214*119mm on inside, so we give split that equally in 4.
            % 214/4 = 53.5mm per chocolate, a chocolate is 53mm.
            SideCenter = [x-107*cos(theta),y-107*sin(theta)];
            for i=1:4
                % center left + half a chocolate for the first one, center left + a
                % chocolate + half a chocolate for the next one etc.
                BPos(i,:) = [SideCenter(1)+26.75*cos(theta)+53.5*(i-1)*cos(theta),SideCenter(2)+26.75*sin(theta)+53.5*(i-1)*sin(theta),theta+pi/2];
            end
            
            return
            
        end;
        
        % Author: Patrick
        % 30-05-2015
        % conversion from conveyor camera to robot frame
        function Pos = conCam2Robot(this,PosCon)
            % same as tableCam2Robot
            % PosCon = [x,y]
            % Pos = [x,y]
            % we know that xRob = -xcoeff*Xcam + xOffset
            % and yRob = yCoeff*Ycam + yOffset
            
            yCoeff = 100/161;
            xCoeff = 0.7245;
            xOffset = 572.72;
            yOffset = 7400/161;   % -15.07719298
            
            Pos(1) = -xCoeff*PosCon(1)+xOffset;
            Pos(2) = yCoeff*PosCon(2)+yOffset;
            return
        end;
        
        % Author: Patrick
        % 30-05-2015
        % conversion from camera frame to robot frame
        function Pos = tableCam2Robot(this,PosTable)
            % obvious function, returns coordinates of PosTable in robot frame
            % PosTable = [i,j]
            % Pos = [x,y]
            % new_x = a*line + b
            % new_y = a*column + b
            %   pix2mmx = 0.6520069808; % might not have been calculated for 1600*1200, 0.6554385965
            %     xOffset = -14.73403141;  % might not have been calculated for 1600*1200, -15.07719298
            
            %      pix2mmy = 1040/1583; % not rounding, Matlab is powerful enough to do it
            %      yOffset = -832520/1583; % same
            
            pix2mmx = 0.6554385965;
            xOffset = -15.07719298;
            
            pix2mmy = 104/159;
            yOffset = -82576/159;
            
            Pos = [];
            Pos(1) = pix2mmx*PosTable(2) + xOffset;
            Pos(2) = pix2mmy*PosTable(1) + yOffset;
            
            %             % old function below
            %             pix2mm =  0.6477;
            %             xOffset =  -800;
            %             yOffset = 140;
            %             % offset
            %             PosTable(1) = PosTable(1) + xOffset;
            %             PosTable(2) = PosTable(2) + yOffset;
            %
            %             % to mm
            %             Pos = pix2mm*PosTable;
            %
            %             % rotation
            %             temp = Pos(1);
            %             Pos(1) = Pos(2);
            %             Pos(2) = temp;
            return
            
        end;
        
        % Author: Patrick
        % 30-05-2015
        % gets the chocolate in x,y,z,theta and put it on the desired stack
        % (flavour)
        function putChocolateFromTableInSlot(this,x,y,z,theta,flavour)
            % x,y in robot frame, theta given with relation to positive X of robot
            % flavour = 1 milk, 2 dark, 3 orange, 4 mint
            
            % TODO - will need to update the handle
            % refereces here...
            switch(this.stackState)
                case 0 % to safe spot
                    this.maxSpeed();
                    this.robController.moveJPos([0,0,0,0,0,0]);
                    this.stackState = 1;
                    this.log('to safe spot while stacking');
                case 1 % getting above chocolate
                    this.maxSpeed();
                    this.robController.moveCartPos([x,y,z+60]);
                    this.stackState = 2;
                    this.log('should be going to chocolate at ', [x,y,z+60]);
                case 2 % dropping down + grabbing it, because we can
                    this.minSpeed();
                    this.robController.moveCartPos([x,y,z]);
                    this.robController.enableSuction();
                    this.stackState = 3;
                    this.log('should have the chocolate');
                    
                case 3 % going back up
                    this.minSpeed();
                    this.robController.moveCartPos([x,y,z+100]);
                    this.stackState = 4;
                    this.log('going back up');
                    
                case 4 % going to the correct stack
                    this.maxSpeed();
                    this.robController.moveCartPos([this.stacksX,this.stacksY(flavour),z+100]);
                    this.stackState = 5;
                    this.log('moving to stack');
                    
                case 5 % figure out the rotation here
                    this.maxSpeed();
                    J = this.robController.getCurJoints();
                    J(6) = J(6) + theta*180/pi;
                    this.robController.moveJPos(J);
                    this.stackState = 6;
                    this.log('rotation');
                    
                case 6 % go to correct height
                    this.minSpeed();
                    this.robController.moveCartPosNoOrient([this.stacksX,this.stacksY(flavour),z+this.chocThick*this.tableStacks(flavour)]);
                    this.stackState = 7;
                    this.log('going down on stack');
                    
                case 7 % drop the chocolate
                    this.robController.disableSuction();
                    this.stackState = 8;
                case 8
                    this.robController.moveCartPosNoOrient([this.stacksX,this.stacksY(flavour),z+100]); % avoiding issues with turning...
                    this.stackState = 0; % done, we can loop back again
                    this.tableStacks(flavour) = this.tableStacks(flavour) + 1;
                    this.log('dropped chocolate and went back up again')
                    this.log('updated stacks : ',this.tableStacks);
                    this.maxSpeed();
            end
            
            return
            
        end;
        
        % Author: Patrick
        % 30-05-2015
        % gets all possible chocolates from the table and puts them on the
        % desired stacks
        function fillStacks(this,Chocolates)
            
            this.Chocs = Chocolates;
            % disp(this.Chocs)
            % find the ones that are pickable and facing up
            II = find(this.Chocs(:,7) == 1 & this.Chocs(:,9) == 1);
            
            switch(this.stackAllState)
                case 0 % we haven't stacked yet
                    if(~isempty(II))
                        this.log('begin stacking the first chocolate...');
                        this.putChocolateFromTableInSlot(Chocolates(II(this.currentStackChoc),1),Chocolates(II(this.currentStackChoc),2),this.tableHeight,Chocolates(II(this.currentStackChoc),3),Chocolates(II(this.currentStackChoc),6));
                        this.stackAllState = 1;
                    else
                        this.log('No chocolates to pick up? y u do dis?');
                    end
                    
                case 1
                    this.putChocolateFromTableInSlot(Chocolates(II(this.currentStackChoc),1),Chocolates(II(this.currentStackChoc),2),this.tableHeight,Chocolates(II(this.currentStackChoc),3),Chocolates(II(this.currentStackChoc),6));
                    if(this.stackState == 0) % chocolate has been stacked
                        this.currentStackChoc = this.currentStackChoc + 1;
                        this.log('incremeting the choc pointer ', this.currentStackChoc);
                        this.log('out of XX chocolates ' , length(II));
                        if this.currentStackChoc > length(II) % this was the last one
                            this.stackAllState = 0;
                            this.currentStackChoc = 1; % reset to begin again
                            this.log('did all the table...');
                        end
                    end
            end
            return
        end;
        
        
        % Author: Patrick
        % 30-05-2015
        % unload the whole box, assuming 8 chocolates in the box, drops
        % them in different spots on the table
        function unloadChocolatesFromBox(this,x,y,theta)
            % x,y = center of box in robot frame
            % theta = orientation of box
            
            switch(this.unloadAllState)
                case 0 % we haven't started yet
                    this.currentBoxEmptyChoc = 8; % 8 chocs maximum in the box to be changed for demo
                    posInBox = mod(this.currentBoxEmptyChoc,4)+1; % cycles through 2-3-4-1
                    this.boxStacks = [2,2,2,2]; % acting as if it's full, replace to [2,2,2,2] for demo
                    this.log('begin getting the first chocolate...');
                    this.unloadOneFromBox(x,y,this.conHeight + this.boxStacks(posInBox)*this.chocThick,theta,posInBox);
                    this.unloadAllState = 1;
                    
                case 1
                    posInBox = mod(this.currentBoxEmptyChoc,4)+1;
                    this.log('getting chocolate at x,y,z = ', [x,y,this.conHeight + this.boxStacks(posInBox)*this.chocThick]);
                    this.unloadOneFromBox(x,y,this.conHeight + this.boxStacks(posInBox)*this.chocThick,theta,posInBox);
                    if(this.unloadState == 0) % if we're done unloading
                        this.log('current box stacks : ', this.boxStacks);
                        if this.currentBoxEmptyChoc == 0 % this means we've hit the last one
                            % this was the last one
                            this.unloadAllState = 0;
                            this.log('got through the box...');
                        end
                    end
            end
            
            return
        end;
        
        
        % Author: Patrick
        % 30-05-2015
        % unload one chocolate from the box, drops it on the table
        function unloadOneFromBox(this,x,y,z,theta,posInBox)
            
            switch(this.unloadState)
                case 0  % going to safe spot
                    this.maxSpeed();
                    this.toSafePos();
                    if this.toSafeState == 0
                        this.unloadState = 1;
                    end
                    this.log('going to safe spot');
                case 1 % going to the box
                    this.maxSpeed();
                    this.robController.moveCartPos([x,y,this.conHeight + 100]); % check if frame matches
                    this.unloadState = 2;
                    this.log('above the box');
                case 2 % drop on chocolate
                    box = this.getBoxPositions(x,y,theta); % check for frame
                    this.minSpeed();
                    this.robController.moveCartPos([box(posInBox,1),box(posInBox,2),z]);
                    this.unloadState = 3;
                    this.log('got the choc I was unloading, I guess');
                    this.robController.enableSuction();
                    
                case 3
                    box = this.getBoxPositions(x,y,theta);
                    this.minSpeed();
                    this.robController.moveCartPos([box(posInBox,1),box(posInBox,2),this.conHeight+250]);
                    this.unloadState = 4;
                    this.log('going back up');
                case 4 % moving to table
                    this.maxSpeed();
                    x = this.boxEmptyingPositions(this.currentBoxEmptyChoc,1);
                    y = this.boxEmptyingPositions(this.currentBoxEmptyChoc,2);
                    this.robController.moveCartPos([x,y,this.tableHeight+100]); % should be going to the right place
                    this.unloadState = 5;
                    this.log('should be somewhere on the table');
                case 5 % turning the chocolate
                    this.maxSpeed();
                    J = this.robController.getCurJoints();
                    J(6) = (theta+pi/2)*180/pi;
                    
                    this.robController.moveJPos(J);
                    this.unloadState = 6;
                    this.log('should have correct orientation (either pi or -pi, me no care');
                case 6 % putting it down
                    this.minSpeed();
                    x = this.boxEmptyingPositions(this.currentBoxEmptyChoc,1);
                    y = this.boxEmptyingPositions(this.currentBoxEmptyChoc,2);
                    this.robController.moveCartPosNoOrient([x,y,this.tableHeight+20]);
                    this.unloadState = 7;
                case 7 % dropping chocolate
                    this.robController.disableSuction();
                    this.unloadState = 0;
                    this.log('dropped it on the table');
                    this.currentBoxEmptyChoc = this.currentBoxEmptyChoc - 1;
                    this.boxStacks(posInBox) = this.boxStacks(posInBox) - 1;
                    this.log('decremeting the choc pointer ', this.currentBoxEmptyChoc);
            end
            
            return
        end
        
        
        % Author: Patrick
        % 30-05-2015
        % flip all inverted chocolates, stacks them one by one.
        function flipAllChocolates(this,Chocolates)
            
            if(~isempty(Chocolates))
                II = find(Chocolates(:,7) == 0 & Chocolates(:,9) == 1); % finding the ones facing down and pickable
                if this.flipState >= 12 % this is after we flipped... Yes it's horrible.
                    II = find(Chocolates(:,7) == 1 & Chocolates(:,9) == 1);
                end
                
                switch(this.flipAllState) % yes I know, a switch with cases 0 and 1 could have been an if/else...
                    case 0 % we haven't started yet
                        if(~isempty(II))
                            this.currentFlipChoc = 1;
                            this.log('begin flipping the first chocolate...');
                            this.flipChocolate(Chocolates(II(1),1),Chocolates(II(1),2),Chocolates(II(1),3));
                            this.flipAllState = 1;
                        end
                        
                    case 1 % if we've started flipping
                        if(~isempty(II))
                            this.flipChocolate(Chocolates(II(1),1),Chocolates(II(1),2),Chocolates(II(1),3));
                            if(this.flipState == 0) % if the chocolate has been flipped
                                this.currentFlipChoc = this.currentFlipChoc + 1;
                                this.log('incrementing counter ', this.currentFlipChoc);
                                this.log('out of XX chocolates ' , length(II));
                                if isempty(II) % we're getting a new picture each time, so when we're done with all of the chocolates to flip, this guy should be empty, hopefully.
                                    this.flipAllState = 0;
                                    this.currentFlipChoc = 1; % reset to begin again
                                    this.log('flipped all...');
                                end
                            end
                        else % damn, it's not working
                            this.flipAllState = 0;
                            this.currentFlipChoc = 1;
                            this.log('problem when flipping...');
                        end
                end
            else
                this.log('no chocolates to flip');
                this.flipAllState = 0;
            end
        end;
        
        
        
        % Author: Patrick
        % 30-05-2015
        % flips the chocolate in x,y, with angle theta. Stacks it
        % afterwards
        function flipChocolate(this,x,y,theta)
            switch(this.flipState)
                case 0 % safe position
                    this.maxSpeed();
                    this.robController.moveJPos([0,0,0,0,0,0]);
                    this.flipState = 1;
                    this.log('going to calib position');
                    
                case 1 % above the chocolate
                    this.maxSpeed();
                    this.robController.moveCartPos([x-this.halfChocLength*cos(theta),y-this.halfChocLength*sin(theta),this.tableHeight]);
                    this.flipState = 2;
                    this.log('going to the chocolate to flip' ,[x,y]);
                    this.log('grab position is ', [x-this.halfChocLength*cos(theta),y-this.halfChocLength*sin(theta)]);
                    
                case 2 % on the chocolate + pick up
                    this.minSpeed();
                    this.robController.moveCartPos([x-this.halfChocLength*cos(theta),y-this.halfChocLength*sin(theta),this.tableHeight]);
                    this.robController.enableSuction();
                    this.flipState = 3;
                    this.log('got it, hopefully');
                    
                case 3 % go back up
                    this.minSpeed();
                    this.robController.moveCartPos([x-this.halfChocLength*cos(theta),y-this.halfChocLength*sin(theta),this.tableHeight]);
                    this.flipState = 4;
                    this.log('going back up');
                    
                case 4 % move to flipPos
                    this.maxSpeed();
                    this.toFlipPos();
                    if (this.toFlipState == 0)
                        this.flipState = 5;
                    end
                    this.log('going to flip position');
                    
                case 5 % get in right orientation etc
                    this.maxSpeed();
                    J = this.robController.getCurJoints();
                    J(6) = J(6) + theta*180/pi;
                    this.robController.moveJPos(J);
                    this.flipState = 6;
                    this.log('Me be ready to flip this');
                    
                case 6 % move forward on table
                    this.minSpeed();
                    this.robController.moveCartPosNoOrient([150,-250,220]);
                    this.flipState = 7;
                    this.log('moved forward');
                    
                case 7  % turn a bit round
                    this.minSpeed();
                    J = this.robController.getCurJoints();
                    J(1) = -15;
                    this.robController.moveJPos(J);
                    this.flipState = 8;
                    this.log('you spin my chocolate right round babe');
                    
                case 8 % move out a bit further
                    this.minSpeed();
                    this.robController.moveCartPosNoOrient([300,-100,220]);
                    this.log('moved forward again');
                    this.flipState = 9;
                    
                case 9 % let go of chocolate
                    this.robController.disableSuction();
                    % move robot slightly up so that we don't destroy the
                    % EE
                    this.maxSpeed();
                    J = this.robController.getCurJoints();
                    J(2) = 0;
                    this.robController.moveJPos(J);
                    this.flipState = 10;
                    this.log('should be flipped');
                    
                case 10 % now going to pick it up and move it ...
                    this.maxSpeed();
                    this.robController.moveJPos([0,0,0,0,0,0]);
                    this.flipState = 11;
                    this.log('to calib pos');
                    
                case 11 % getting out of frame
                    this.maxSpeed();
                    this.robController.moveJPos([100,0,0,0,0,0]);
                    this.flipState = 12;
                    this.log('getting out of frame...');
                    
                case 12 % getting a new picture
                    this.log('getting new chocolates');
                    this.getNewChocs();
                    this.flipState = 13;
                    
                case 13 % getting the chocolate
                    this.log('checking if I can stack it now');
                    if(~isempty(this.Chocs))
                        II = find(this.Chocs(:,2) < 0); % chocolats can only be on the left...
                        toGrab = this.Chocs(II,:);
                        
                        if(~isempty(toGrab))
                            % finding the one that is facing up and grabbable
                            II = find(toGrab(:,7) == 1 & toGrab(:,9) == 1) ;
                            toGrab = toGrab(II,:);
                            
                            % putting it on the stack
                            if ~isempty(toGrab) % making sure there's something to grab
                                this.currentStackChoc = 1;
                                this.log('theres something to stack');
                                this.log('stack state ', this.stackState);
                                this.putChocolateFromTableInSlot(toGrab(this.currentStackChoc,1),toGrab(this.currentStackChoc,2),this.tableHeight,toGrab(this.currentStackChoc,3),toGrab(this.currentStackChoc,6));
                                this.flipState = 14;
                            else
                                this.log('the unicorns cry once more, we failed, theres a random chocolate lying around now');
                                this.flipState = 0; %failed I have, in exile I must go
                            end
                        else
                            this.log('cant find anything...');
                            this.flipState = 0;
                        end
                    else
                        this.log('cant find anything...');
                        this.flipState = 0;
                    end
                    
                case 14 % not too sure why this is here
                    if(~isempty(this.Chocs))
                        II = find(this.Chocs(:,2) < 0);
                        toGrab = this.Chocs(II,:);
                        
                        % finding the one that is facing up and grabbable
                        if(~isempty(toGrab))
                            II = find(toGrab(:,7) == 1 & toGrab(:,9) == 1) ;
                            toGrab = toGrab(II,:);
                            
                            
                            this.putChocolateFromTableInSlot(toGrab(this.currentStackChoc,1),toGrab(this.currentStackChoc,2),this.tableHeight,toGrab(this.currentStackChoc,3),toGrab(this.currentStackChoc,6));
                            if this.stackState == 0 % have we stacked that sob?
                                this.currentStackChoc = this.currentStackChoc + 1;
                                this.log('current stack choc', this.currentStackChoc);
                                this.log('out of ', length(toGrab(:,1)));
                                if this.currentStackChoc > length(toGrab(:,1))
                                    this.log('done stacking the flipped chocolate');
                                    this.currentStackChoc = 1; % reset this guy
                                    this.flipState = 0; % chocolate has been put away
                                end
                            end
                        end
                    end
            end
            
            return
        end;
        
        
        
        % Author: Patrick
        % 30-05-2015
        % sets the robot to its "safe" position, above the conveyor, useful
        % to take pictures
        function toSafePos(this)
            switch(this.toSafeState)
                case 0
                    this.maxSpeed();
                    this.robController.moveJPos([0,0,0,0,0,0]);
                    this.toSafeState = 1;
                case 1
                    this.maxSpeed();
                    this.robController.moveCartPos([0,250,250]);
                    this.toSafeState = 0;
            end
            return
        end;
        
        
        % Author: Patrick
        % 30-05-2015
        % acquires a new chocolates matrix from the vision, converts it to
        % robot frame.
        function getNewChocs(this)
            this.chocFlag = 1;
            Chocolates = this.vision.blockProcessTableIm();
            
            if(~isempty(Chocolates))
                Chocolates(:,1) = 1600 - Chocolates(:,1); % correcting frame
                Chocolates(:,3) = Chocolates(:,3) - pi/2;
                
                for i = 1:length(Chocolates(:,1))
                    pix = [Chocolates(i,1),Chocolates(i,2)];
                    xy = this.tableCam2Robot(pix); % setting in xy robot frame
                    Chocolates(i,1) = xy(1);
                    Chocolates(i,2) = xy(2);
                end
            end
            this.Chocs = Chocolates;
            this.chocFlag = 0;
            return
        end;
        
        % Author: Patrick
        % 30-05-2015
        % sets the robot to the flipping position, on the side of the
        % table.
        function toFlipPos(this)
            switch(this.toFlipState)
                case 0
                    this.maxSpeed();
                    this.robController.moveJPos([0,0,0,0,0,0]);
                    this.toFlipState = 1;
                case 1
                    this.maxSpeed();
                    this.robController.moveJPos([-90,0,0,0,0,0]);
                    this.toFlipState = 2;
                case 2
                    this.maxSpeed();
                    %      this.robController.moveJPos([-55.4368,38,49.6827,-55.4361,89.42,0.8]);
                    this.robController.moveJPos([-89.38,38.4,51.09,-95.67,89.53,0]);
                    this.toFlipState = 0;
            end
            return
        end;
        
        
        
        % Author: Patrick
        % 30-05-2015
        % sets the robot to maximum speed (400,400,400,400) for when it has
        % a clear path
        function maxSpeed(this)
            Speed = [400,400,400,400]; % max allowed
            this.robController.setSpeed(Speed);
            return
        end
        
        % Author: Patrick
        % 30-05-2015
        % sets the robot to minimum speed (20,20,20,20) for when it might
        % hit something.
        function minSpeed(this)
            Speed = [20,20,20,20];
            this.robController.setSpeed(Speed);
            return
        end
        
        
        
        % Author: Patrick
        % 30-05-2015
        % sets the stack on the table. newStacks is [nMilk,
        % nDark, nOrange, nMint]
        % debug function.
        function setStacks(this,newStacks)
            this.tableStacks = newStacks;
            return
        end
        
        % Author: Patrick
        % 30-05-2015
        % sets the order for the robot. newOrder is [nMilk,
        % nDark, nOrange, nMint]
        % debug function.
        function setOrder(this,newOrder)
            this.order = newOrder;
            this.execOrder = 1;
            return
        end
        
        % Author: Patrick
        % 30-05-2015
        % sets the sequence of the robot, sequence is an array of N < 13,
        % values in 1,2,3,4 for milk,dark,orange,mint.
        % debug function.
        function setSequence(this,inSequence, Sequence)
            this.sequencedOrder = inSequence;
            this.sequence = Sequence;
            return
        end
        
        % Author: Patrick
        % 30-05-2015
        % will stack all available chocolates on the table.
        % only to be used in command line! Is not linked to GUI because
        % this is a BLOCKING function
        function cmdFillAllStacks(this)
            this.robController.enableVacuumPump();
            this.robController.setLinearMode(0);
            this.toSafePos();
            while (this.toSafeState ~= 0) % waiting to get where we want
                while (this.robController.isReady() == 0)
                    % empty wait loop...
                end
                this.robController.setLinearMode(0);
                this.toSafePos();
            end
            
            pause(5);
            this.getNewChocs();
            pause(3); % pause 3 seconds because duck this.
            this.fillStacks(this.Chocs);
            while (this.stackAllState ~= 0)
                while (this.robController.isReady() == 0)
                    % wait loop
                end
                this.robController.setLinearMode(0);
                this.fillStacks(this.Chocs);
            end
            this.robController.disableVacuumPump();
            J = [0,0,0,0,0,0];
            this.robController.moveJPos(J);
            
            disp('done stacking');
            return;
        end
        
        
        % Author: Patrick
        % 30-05-2015
        % will flip all available inverted chocolates on the table.
        % only to be used in command line! Is not linked to GUI because
        % this is a BLOCKING function
        function cmdFlipAllChocolates(this)
            this.maxSpeed();
            this.toSafeState = 0;
            this.flipAllState = 0;
            this.flipState = 0;
            this.robController.enableVacuumPump();
            this.robController.setLinearMode(0);
            this.toSafePos();
            while (this.toSafeState ~= 0) % waiting to get where we want
                while (this.robController.isReady() == 0)
                    % empty wait loop...
                end
                this.robController.setLinearMode(0);
                this.toSafePos();
            end
            pause(5);
            this.getNewChocs();
            
            pause(3); % pause 3 seconds because duck this.
            this.flipAllChocolates(this.Chocs);
            while (this.flipAllState ~= 0)
                while (this.robController.isReady() == 0)
                    % empty wait loop...
                end
                this.flipAllChocolates(this.Chocs);
            end
            
            this.robController.disableVacuumPump();
            J = [0,0,0,0,0,0];
            this.robController.moveJPos(J);
            
            disp('done flipping');
            return;
        end
        
        
        % Author: Patrick
        % 30-05-2015
        % will stack and flip all available chocolates on the table.
        % only to be used in command line! Is not linked to GUI because
        % this is a BLOCKING function.
        function cmdStackAndFlip(this)
            
            this.cmdFillAllStacks();
            this.cmdFillAllStacks();
            this.cmdFlipAllChocolates();
            
            disp('done');
            return;
        end
        
        
        % Author: Patrick
        % 30-05-2015
        % will unload the chocolates from the box (8 max, nicely stacked) to the table.
        % only to be used in command line! Is not linked to GUI because
        % this is a BLOCKING function.
        function cmdUnloadAll(this)
            % initialise the states to 0
            this.maxSpeed();
            this.unloadAllState = 0;
            this.unloadState = 0;
            this.robController.setLinearMode(0);
            
            this.robController.moveJPos([0,0,0,0,0,0]);
            pause(3);
            % trigger conveyor camera
            this.vision.triggerProcessConvIm();
            % get box...
            pause(2); % wait 2 seconds to make sure it happens...
            B =  this.robController.getBoxes();
            Box = B.boxLocations;
            Box = this.conCam2Robot(Box);
            Box = [Box, B.boxRotations];
            Box(3) = Box(3)*pi/180; % this might be wrong....
            
            this.robController.enableVacuumPump();
            unloadChocolatesFromBox(this,Box(1),Box(2),Box(3))
            while (this.unloadAllState ~= 0)
                while (this.robController.isReady() == 0)
                    % waiting loop
                end
                unloadChocolatesFromBox(this,Box(1),Box(2),Box(3))
            end
            
            pause(3);
            this.robController.moveJPos([0,0,0,0,0,0]);
            this.robController.disableVacuumPump();
            disp('done');
            return;
        end
        
        
        % Author: Patrick
        % 30-05-2015
        % assumes we have enough chocolates on the stack
        % assumes box is empty...
        % will load the chocolates from Order in the box. Order is [nMilk,
        % nDark, nOrange, nMint]
        % only to be used in command line! Is not linked to GUI because
        % this is a BLOCKING function.
        function cmdLoadChocs(this,Order,Sequence,InSequence)
            % initialise states to 0
            this.maxSpeed();
            this.robController.setLinearMode(0);
            this.loadAllState = 0;
            this.loadState = 0;
            this.order = Order;
            this.sequence = Sequence;
            this.sequencedOrder = InSequence;
            
            this.robController.moveJPos([0,0,0,0,0,0]);
            pause(3);
            % trigger conveyor camera
            this.vision.triggerProcessConvIm();
            % get box...
            pause(2); % wait 2 seconds to make sure it happens...
            B =  this.robController.getBoxes();
            Box = B.boxLocations;
            Box = this.conCam2Robot(Box);
            Box = [Box, B.boxRotations];
            Box(3) = Box(3)*pi/180; % this might be wrong....
            
            
            % load...
            this.robController.enableVacuumPump();
            this.loadAllInBox(Box);
            while (this.loadAllState ~= 0)
                while (this.robController.isReady() == 0)
                    pause(0.3);
                end
                this.loadAllInBox(Box);
            end
            
            % done
            pause(3);
            this.robController.moveJPos([0,0,0,0,0,0]);
            this.robController.disableVacuumPump();
            disp('done');
            return;
        end
        
        
        
        % Debug function to read the chocolates stored in the object.
        % use = obvious.
        % Author: Patrick
        % 30-05-2015
        function chocs = getChocs(this)
            chocs = this.Chocs;
            return
        end;
        
    end
    
    
    
    
    methods (Access = private)
        
    end
    
end

