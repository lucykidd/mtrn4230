classdef (Abstract) GUIInterface < Process
    %GUIInterface : The GUI class is used to handle everything to do with the gui. It
    % will have public functions that other classes can call to update the
    % gui (i.e gui.drawImage to draw the image from the vision processor),
    % and private callback methods that are used to handle button presses etc...
    %
    % Usage : TODO
    %
    % Author : Martin & Isuru
    % Date : 24/03/2015
    
    properties (Access = protected)
        robotHandle;
        pickerHandle; % handle to the picker object...
        chocMatrix;
        % managerHandle; % now defined in the super class...
    end
    
    methods (Abstract)
        % Note : All Methods here are just definitions
        % Do not use a function...end block to define an abstract method, use only the method signature.
        
        %% Things you need to implement...
        
        updateTableImage (this, image, imageState, chocolateLocations, chocolateOrientations, chocolateTypes)
        % updateBaseImage : this is called by our vision object to set a new
        % image for the table (image) with locations of detected chocolates
        % specified in chocolateLocations vector, orientations in the
        % chocolateOrientations vector and types in the chocolateTypes Vector
        %
        % imageState: tells us if the image is occluded (i.e by the robot) or
        % is not occluded(clear)... Typically we only want to process non
        % occluded images...
        % Usage : gui.updateTableImage(...)
        % Author : Lindsay
        % Date : 24/03/2015
        
        updateConveyorImage (this, image, imageState, boxLocations, boxOrientations, chocolateLocations, chocolateOrientations, chocolateTypes)
        % updateConveyorImage : this is called by our vision object to set a new
        % image for the conveyor image (image) with locations of detected chocolates
        % specified in chocolateLocations vector, orientations in the
        % chocolateOrientations vector and types in the chocolateTypes Vector
        %
        % NB: all coordinates are w.r.t to the image frame and not the world
        % frame..
        % imageState: tells us if the image is occluded (i.e by the robot) or
        % is not occluded(clear)... Typically we only want to process non
        % occluded images...
        % Usage : gui.updateConveyorImage(...)
        % Author : Lindsay
        % Date : 24/03/2015
        
        updateChocolateStatus (this,chocMatrix)
        % This function takes the same C matrix defined in ASST1 for
        % storing chocolate locations and uses it to update our chocolate
        % image representations...
        
        updateBoxStatus (this,centroids,orientations,boxes)
        % This function takes a vector of centroids, orientations and a
        % matrice of box points to allow it to know the box position and
        % also draw a box around it...
        
        updateRobotMode (this, robotMode)
        % updateRobotMode : called by robotcontroller to update the visual representation of the mode of the
        % robot (-1 = error (estop??) , 0 =inactive (hasn't been started yet) ,
        % 1 = active Manual , 2= active automatic)
        % Usage: gui.updateRobotMode(robotMode);
        
        updateRobotIOState(this,robotIO)
        % updateIOState : called by robotcontroller to update the visual
        % representation of the IO of the robot ...
        %
        % Usage: gui.updateRobotMode(robotMode);
        
        updateRobotPose(this,robotPosition,jointAngles,endRotation)
        % updateRobotPose : called by robotcontroller to update a guis
        % representation of the robots current pose.
        % TODO - describe the robotPose
        % Usage : gui.updateRobotPose(robotPose);
        
        updateCommsList (this,comms)
        % updateCommsList : Advise the user about any commands sent ?to or from
        % the Robot System in a user�readable format
        % hence this will be called by the robot controller when it sends a
        % command to the robot and when it recieves a command from the robot...
        
        dispRobotError (this,error)
        % dispRobotError: advises the user about any errors that may have
        % happened to the robot. (e-stop etc...)
        % TODO - define the error varaible...
        
%         updateSystemStatus (this,status)
%         % updateSystemStatus: update the status of each object that the manager
%         % manages (i.e if the executables are still running etc...)
%         % TODO - define the status variable...
        
        %% Stuff to not touch!!!
        addHandles (this,robotHandle)
        % addHandles : adds the robotObject handle...
        % Usage : robcontroller.addHandles(guiHandle,siocommshandle,movecommshandle);
        % Author : Lindsay
        % Date : 24/03/2015
    end
    
    % !!!! Please do not add any concrete methods here... do that in the
end

