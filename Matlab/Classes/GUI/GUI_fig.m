function varargout = GUI_fig(varargin)
% STANDARD_FIG MATLAB code for standard_fig.fig
%      STANDARD_FIG, by itself, creates a new STANDARD_FIG or raises the existing
%      singleton*.
%
%      H = STANDARD_FIG returns the handle to a new STANDARD_FIG or the handle to
%      the existing singleton*.
%
%      STANDARD_FIG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STANDARD_FIG.M with the given input arguments.
%
%      STANDARD_FIG('Property','Value',...) creates a new STANDARD_FIG or raises
%      the existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before standard_fig_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to standard_fig_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help standard_fig

% Last Modified by GUIDE v2.5 11-May-2015 01:34:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_fig_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_fig_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before standard_fig is made visible.
function GUI_fig_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to standard_fig (see VARARGIN)

% Choose default command line output for standard_fig
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

initialize_gui(hObject, handles, false);

% --- Outputs from this function are returned to the command line.
function varargout = GUI_fig_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the reset_btn flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to reset_btn the data.
if isfield(handles, 'metricdata') && ~isreset
    return;
end

set(handles.joint_mode, 'Value', 1);
set(handles.linear_mode, 'Value', 0);
set(handles.manual_mode, 'Value', 1);
set(handles.auto_mode, 'Value', 0);

%for robot controller
% Update handles structure
guidata(handles.figure1, handles);

% --- Executes on button press in joint_mode.
function joint_mode_Callback(hObject, eventdata, handles)
% hObject    handle to joint_mode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of joint_mode
set(hObject,'Value',0);
guidata(hObject, handles);

% --- Executes on button press in linear_mode.
function linear_mode_Callback(hObject, eventdata, handles)
% hObject    handle to linear_mode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of linear_mode
set(hObject,'Value',0);
guidata(hObject, handles);

% --------------------------------------------------------------------


function joint_mode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to joint_mode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
set(hObject, 'Value', 0);

function edit_speed_EE_linear_Callback(hObject, eventdata, handles)
% hObject    handle to edit_speed_EE_linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_speed_EE_linear as text
%        str2double(get(hObject,'String')) returns contents of edit_speed_EE_linear as a double


% --- Executes during object creation, after setting all properties.
function edit_speed_EE_linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_speed_EE_linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pause_robot.
function pause_robot_Callback(hObject, eventdata, handles)
% hObject    handle to pause_robot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key release with focus on figure1 and none of its controls.
function figure1_KeyReleaseFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was released, in lower case
%	Character: character interpretation of the key(s) that was released
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) released
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in vac_state.
function vac_state_Callback(hObject, eventdata, handles)
% hObject    handle to vac_state (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in sol_state.
function sol_state_Callback(hObject, eventdata, handles)
% hObject    handle to sol_state (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in conv_state.
function conv_state_Callback(hObject, eventdata, handles)
% hObject    handle to conv_state (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in conv_dir.
function conv_dir_Callback(hObject, eventdata, handles)
% hObject    handle to conv_dir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function yaw_desired_Callback(hObject, eventdata, handles)
% hObject    handle to yaw_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of yaw_desired as text
%        str2double(get(hObject,'String')) returns contents of yaw_desired as a double


% --- Executes during object creation, after setting all properties.
function yaw_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yaw_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x_desired_Callback(hObject, eventdata, handles)
% hObject    handle to x_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of x_desired as text
%        str2double(get(hObject,'String')) returns contents of x_desired as a double


% --- Executes during object creation, after setting all properties.
function x_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pitch_desired_Callback(hObject, eventdata, handles)
% hObject    handle to pitch_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pitch_desired as text
%        str2double(get(hObject,'String')) returns contents of pitch_desired as a double


% --- Executes during object creation, after setting all properties.
function pitch_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pitch_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function roll_desired_Callback(hObject, eventdata, handles)
% hObject    handle to roll_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of roll_desired as text
%        str2double(get(hObject,'String')) returns contents of roll_desired as a double


% --- Executes during object creation, after setting all properties.
function roll_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to roll_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function z_desired_Callback(hObject, eventdata, handles)
% hObject    handle to z_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of z_desired as text
%        str2double(get(hObject,'String')) returns contents of z_desired as a double


% --- Executes during object creation, after setting all properties.
function z_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to z_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y_desired_Callback(hObject, eventdata, handles)
% hObject    handle to y_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of y_desired as text
%        str2double(get(hObject,'String')) returns contents of y_desired as a double


% --- Executes during object creation, after setting all properties.
function y_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function j1_desired_Callback(hObject, eventdata, handles)
% hObject    handle to j1_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of j1_desired as text
%        str2double(get(hObject,'String')) returns contents of j1_desired as a double


% --- Executes during object creation, after setting all properties.
function j1_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j1_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function j2_desired_Callback(hObject, eventdata, handles)
% hObject    handle to j2_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of j2_desired as text
%        str2double(get(hObject,'String')) returns contents of j2_desired as a double


% --- Executes during object creation, after setting all properties.
function j2_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j2_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function j3_desired_Callback(hObject, eventdata, handles)
% hObject    handle to j3_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of j3_desired as text
%        str2double(get(hObject,'String')) returns contents of j3_desired as a double


% --- Executes during object creation, after setting all properties.
function j3_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j3_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function j4_desired_Callback(hObject, eventdata, handles)
% hObject    handle to j4_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of j4_desired as text
%        str2double(get(hObject,'String')) returns contents of j4_desired as a double


% --- Executes during object creation, after setting all properties.
function j4_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j4_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function j5_desired_Callback(hObject, eventdata, handles)
% hObject    handle to j5_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of j5_desired as text
%        str2double(get(hObject,'String')) returns contents of j5_desired as a double


% --- Executes during object creation, after setting all properties.
function j5_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j5_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function j6_desired_Callback(hObject, eventdata, handles)
% hObject    handle to j6_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of j6_desired as text
%        str2double(get(hObject,'String')) returns contents of j6_desired as a double


% --- Executes during object creation, after setting all properties.
function j6_desired_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j6_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on mouse press over axes background.
function robot_image_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to robot_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in choc_states.
function choc_states_Callback(hObject, eventdata, handles)
% hObject    handle to choc_states (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in comms_state.
function comms_state_Callback(hObject, eventdata, handles)
% hObject    handle to comms_state (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in robot_mode_display.
function robot_mode_display_Callback(hObject, eventdata, handles)
% hObject    handle to robot_mode_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in conv_state_main.
function conv_state_main_Callback(hObject, eventdata, handles)
% hObject    handle to conv_state_main (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in comms_mode.
function comms_mode_Callback(hObject, eventdata, handles)
% hObject    handle to comms_mode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_speed_robot_angular_Callback(hObject, eventdata, handles)
% hObject    handle to edit_speed_robot_angular (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_speed_robot_angular as text
%        str2double(get(hObject,'String')) returns contents of edit_speed_robot_angular as a double


% --- Executes during object creation, after setting all properties.
function edit_speed_robot_angular_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_speed_robot_angular (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_speed_robot_linear_Callback(hObject, eventdata, handles)
% hObject    handle to edit_speed_robot_linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_speed_robot_linear as text
%        str2double(get(hObject,'String')) returns contents of edit_speed_robot_linear as a double


% --- Executes during object creation, after setting all properties.
function edit_speed_robot_linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_speed_robot_linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_speed_EE_angular_Callback(hObject, eventdata, handles)
% hObject    handle to edit_speed_EE_angular (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_speed_EE_angular as text
%        str2double(get(hObject,'String')) returns contents of edit_speed_EE_angular as a double


% --- Executes during object creation, after setting all properties.
function edit_speed_EE_angular_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_speed_EE_angular (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in update_robot_angular_speed.
function update_robot_angular_speed_Callback(hObject, eventdata, handles)
% hObject    handle to update_robot_angular_speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in update_robot_linear_speed.
function update_robot_linear_speed_Callback(hObject, eventdata, handles)
% hObject    handle to update_robot_linear_speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in update_EE_angular_speed.
function update_EE_angular_speed_Callback(hObject, eventdata, handles)
% hObject    handle to update_EE_angular_speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in update_EE_linear_speed.
function update_EE_linear_speed_Callback(hObject, eventdata, handles)
% hObject    handle to update_EE_linear_speed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in update_robot_pose.
function update_robot_pose_Callback(hObject, eventdata, handles)
% hObject    handle to update_robot_pose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in update_joints_desired.
function update_joints_desired_Callback(hObject, eventdata, handles)
% hObject    handle to update_joints_desired (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in disp_term.
function disp_term_Callback(hObject, eventdata, handles)
% hObject    handle to disp_term (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns disp_term contents as cell array
%        contents{get(hObject,'Value')} returns selected item from disp_term


% --- Executes during object creation, after setting all properties.
function disp_term_CreateFcn(hObject, eventdata, handles)
% hObject    handle to disp_term (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in update_robot_pose.
function update_robot_desired_Callback(hObject, eventdata, handles)
% hObject    handle to update_robot_pose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on slider movement.
function sliderLinSpeedEE_Callback(hObject, eventdata, handles)
% hObject    handle to sliderLinSpeedEE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderLinSpeedEE_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderLinSpeedEE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderAngSpeedEE_Callback(hObject, eventdata, handles)
% hObject    handle to sliderAngSpeedEE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderAngSpeedEE_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderAngSpeedEE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderLinSpeedRobot_Callback(hObject, eventdata, handles)
% hObject    handle to sliderLinSpeedRobot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderLinSpeedRobot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderLinSpeedRobot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderAngSpeedRobot_Callback(hObject, eventdata, handles)
% hObject    handle to sliderAngSpeedRobot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderAngSpeedRobot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderAngSpeedRobot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in hide_gui.
function hide_gui_Callback(hObject, eventdata, handles)
% hObject    handle to hide_gui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
