classdef GUI_Base < GUIInterface
    %% GUI_Base : This class is a base class for our gui. Since everything has become a little bit bloated I decided to try and hide all of this code away and start afresh..
    %% Usage : Displays a GUI
    %% Author : Martin, Isuru, Patrick, Lindsay
    %% Date last modified: 30/05/2015
    
    %% Properties
    properties (Access = public)
        % Everyone can see and modify these properties
        % NB: there is a way so that people can read properties but not write
        gui_h;
        gui_new;
        robObj;
        display_table;
        
        % clear scene image
        clearTableImage;
        clearConvImage;
    end
    
    properties (Access = protected)
        % Only this class can access these properties
        currentPressedKey; % A string of only a single key...
        buttonRed;
        %IO related handles
        vacValue;
        vacSolValue;
        convValue;
        convDir;
        
        % Robot movement modes
        joggingMode; % mode of movement when jogging
        motionMode; % mode of movement when a coordinate is picked...
        
        % image related handles...
        errorTableImage; % simply an error image to display
        errorConveyorImage;% simply an error image to display
        tableImageHandle; % a handle to our main table image
        conveyorImageHandle; % a handle to our main conveyor image...
        chocLabelHandles; % a vector of handles to the gui numbered text objects...
        boxHandles; % a vector of handles to boxes plotted in our gui scene...
        convBoxHandles; % a vector of handles to boxes that are plotted in our gui scene...
        reachablePlot;
        centroidBoxConv;
        conveyorBoxRect; % handle to a rectangle drawn on the conveyor box..
        convBoxCentroids;
        convBoxLabels;
        % Image display triggering
        
    end
    
    %% Methods
    methods (Access = public)
        function this = GUI_Base()
            %% Usage: Defines public Methods used throughout this class
            %% Author: Isuru, Martin, Patrick, Lindsay
            %% Date modified: 15/04/15
            this.name='gui';
            this.buttonRed = 1; % green =0 , red =1
            this.vacValue = 0; %off = 0, on = 1;
            this.vacSolValue = 0;%off = 0, on = 1;
            this.convValue = 0;
            this.convDir = 0;
            
            this.joggingMode = 0; % default jogging mode to joints ?
            this.motionMode=0; % default motion mode to joints
            robotPosition = 0;
            jointAngles = 0;
            endRotation = 0;
            
            % make the gui handle and store it locally
            this.gui_new = guihandles(GUI_NEW_fig); % this is our new gui...simplified version...
            this.gui_h = guihandles(GUI_fig); % this is our old gui which will be using for advanced control...
            
            %Selection change function and push button callbacks
            set(this.gui_h.jog_mode, 'selectionchangefcn', @(src, event)Robot_Movement(this, src, event));
            set(this.gui_h.motion_mode, 'selectionchangefcn', @(src, event) Change_Motion_Mode(this, src, event));
            set(this.gui_h.sys_mode, 'selectionchangefcn', @(src, event)System_Mode(this, src, event));
            
            set(this.gui_h.vac_state, 'callback', @(src, event) Vac_State(this, src, event));
            set(this.gui_h.sol_state, 'callback', @(src, event) Sol_State(this, src, event));
            set(this.gui_h.conv_state, 'callback', @(src, event) Conv_State(this, src, event));
            set(this.gui_h.conv_dir, 'callback', @(src, event) Conv_Dir(this, src, event));
            
            set(this.gui_h.sliderLinSpeedEE, 'callback', @(src, event) updateLinSpeedEE(this, src, event));
            set(this.gui_h.sliderAngSpeedEE, 'callback', @(src, event) updateAngSpeedEE(this, src, event));
            set(this.gui_h.sliderLinSpeedRobot, 'callback', @(src, event) updateLinSpeedRobot(this, src, event));
            set(this.gui_h.sliderAngSpeedRobot, 'callback', @(src, event) updateAngSpeedRobot(this, src, event));
            
            set(this.gui_h.sliderLinSpeedEE, 'max', 400, 'min', 30, 'value', 200);
            set(this.gui_h.sliderAngSpeedEE, 'max', 400, 'min', 30, 'value', 200);
            set(this.gui_h.sliderLinSpeedRobot, 'max', 400, 'min', 30, 'value', 200);
            set(this.gui_h.sliderAngSpeedRobot, 'max', 400, 'min', 30, 'value', 200);
            
            % Robot Start and stop callbacks...
            set(this.gui_h.update_robot_pose, 'callback', @(src, event) updateRobotButton(this,src,event)); % done by pat
            set(this.gui_h.pause_robot, 'callback', @(src, event) Pause_Robot(this, src, event));
            set(this.gui_new.pause_robot, 'callback', @(src, event) Pause_Robot(this, src, event));
            
            % Set key press callback function
            set(this.gui_h.figure1,'KeyPressFcn',@(src, event) Key_Down(this, src, event));
            set(this.gui_h.figure1, 'KeyReleaseFcn', @(src, event) Key_Up(this, src, event));
            
            % Create the figures for our gui image...
            tableSample = which('disconnectedTable.jpg');
            this.errorTableImage = imread(tableSample);
            conveyorSample = which('disconnectedConveyor.png');
            this.errorConveyorImage = imread(conveyorSample);
            %this.tableImageHandle = imshow(this.errorTableImage, 'Parent', this.gui_h.robot_image);
            this.tableImageHandle = imshow(this.errorTableImage, 'Parent', this.gui_new.robot_image);
            this.plotBoxesInit(); % lets put our text and boxes into the scene...
            
            %this.conveyorImageHandle = imshow(this.errorConveyorImage, 'Parent', this.gui_h.conveyor_image);
            this.conveyorImageHandle = imshow(this.errorConveyorImage, 'Parent', this.gui_new.conveyor_image);
            this.initConveyorBoxes(); % initialise our conveyor boxes handles...
            
            % Setup the callback functions for button click on image...
            set(this.tableImageHandle,'ButtonDownFcn',@(src,event) Table_Click_Callback(this,src,event));
            set(this.conveyorImageHandle,'ButtonDownFcn',@ (src,event) Conveyor_Click_Callback(this,src,event));
            
            % sets the figure close function. This lets the class know that
            %the figure wants to close and thus the class should cleanup in
            %memory as well
            set(this.gui_h.figure1,  'closerequestfcn', @(src,event) Close_fcn(this, src, event));
            set(this.gui_new.figure1,  'closerequestfcn', @(src,event) Close_fcn(this, src, event));
        end
        
        %% Usage: This function gets the clicked coordinates from the table.
        %% Inputs: Inputs are 'this' which is a reference to an instance of the 'this'object. Event represents changes or actions that occur within objects
        %% Outputs: this function calls the robotHandle to cause it to move to a new position
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function Table_Click_Callback (this,src,event)
            this.log('Table image has been clicked...');
            axesHandle  = get(src,'Parent');
            coordinates = get(axesHandle,'CurrentPoint');
            coordinates = coordinates(1,1:2);
            this.log('Table click coordinates :',coordinates);
            
            if this.buttonRed == 1
                this.robotHandle.moveToTableImage(coordinates); % sending through to the controller
            end
        end;
        
        %% Usage: This function gets the clicked coordinates from the conveyor.
        %% Inputs: Inputs are 'this' which is a reference to an instance of the 'this'object. Event represents changes or actions that occur within objects
        %% Outputs: this function calls the robotHandle to cause it to move to a new position
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function Conveyor_Click_Callback (this,src,event)
            this.log('Conveyor image has been clicked...');
            axesHandle  = get(src,'Parent');
            coordinates = get(axesHandle,'CurrentPoint');
            coordinates = coordinates(1,1:2);
            this.log('Conveyor click coordinates :',coordinates);
            
            if this.buttonRed == 1
                this.robotHandle.moveToConveyorImage(coordinates) % sending through to the controller
            end
        end;
        
        %% Usage: The modes of movement between joint mode and XYZ mode
        %% Author: Isuru
        %% Date modified: 30/5/15
        function out = Robot_Movement(this, src, event)
            %Selects the modes of movement between joint mode and XYZ mode.
            %author: Isuru
            selected_btn = event.NewValue;
            %set us a switch/case to handle the two current possible choises
            switch selected_btn
                case this.gui_h.joint_mode
                    this.joggingMode = 1;
                case this.gui_h.linear_mode
                    this.joggingMode = 2;
            end
        end
        
        %% Usage: Function to toggle the system mode for the robot. Any manual command should automatically reset the system
        %% Author: Isuru & Martin
        %% Date modified: 30/5/15
        function System_Mode(this,src,event)
            selected_btn = event.NewValue;
            
            switch selected_btn
                case this.gui_h.manual_mode
                    this.robotHandle.changeMode(1);
                    this.log('Change robot mode to manual');
                    message = 'Change robot mode to manual';
                    commandWindow(this, message);
                case this.gui_h.auto_mode
                    this.robotHandle.changeMode(2);
                    this.log('Change robot mode to automatic');
                    message = 'Change robot mode to automatic';
                    commandWindow(this, message);
            end
        end;
        
        %% Usage: Function for retrieving value for conveyor connection state
        %% Author: Isuru & Martin
        %% Date modified: 30/5/15
        function conveyorState(this,conv_state)
            %dummy variable replace with connection value
            if conv_state == 0
                try
                    set(this.gui_h.conv_state_main, 'String', 'Off', 'Value', 0, 'BackgroundColor', 'red');
                    set(this.gui_new.conv_state_main, 'String', 'Off', 'Value', 0, 'BackgroundColor', 'red');
                catch e
                end
                this.convValue = 1; % Set it such that the conveyor is turned off...
                this.Conv_State(1,1); % set it to off and send a disable message as well..to be sure...
                set(this.gui_h.conv_state, 'Enable', 'off');
            else
                try
                    set(this.gui_h.conv_state_main, 'String', 'On', 'Value', 1, 'BackgroundColor', 'green');
                    set(this.gui_new.conv_state_main, 'String', 'On', 'Value', 1, 'BackgroundColor', 'green');
                catch e
                end
                set(this.gui_h.conv_state, 'Enable', 'on');
            end
            this.log('Change conveyor state to:',conv_state);
        end
        
        %% Usage: Function for updating robot speeds
        %% Author: Isuru & Martin
        %% Date modified: 30/5/15
        function out = Update_Robot_Angular_Speed(this,src,event)
            speed = str2double(get(this.gui_h.edit_speed_robot_angular, 'String'));
        end
        
        function out = updateLinSpeedEE(this,src,event)
            val1 = get(this.gui_h.sliderLinSpeedEE, 'Value');
            set(this.gui_h.text98, 'String', val1);
            this.log('Robot angular speed changed');
        end
        
        function out = updateAngSpeedEE(this,src,event)
            val2 = get(this.gui_h.sliderAngSpeedEE, 'Value');
            set(this.gui_h.text99, 'String', val2);
            this.log('Robot angular speed changed');
        end
        
        function out = updateLinSpeedRobot(this,src,event)
            val3 = get(this.gui_h.sliderLinSpeedRobot, 'Value');
            set(this.gui_h.text100, 'String', val3);
            this.log('Robot angular speed changed');
        end
        
        function out = updateAngSpeedRobot(this,src,event)
            val4 = get(this.gui_h.sliderAngSpeedRobot, 'Value');
            set(this.gui_h.text101, 'String', val4);
            this.log('Robot angular speed changed');
        end
        
        %% Usage: Updates robot values found in boxes, XYZ or joint from jogging
        %% Author: Patrick
        %% Date modified: 20/04/15
        function out = updateRobotButton(this,src,event)
            if this.buttonRed == 1 % are we willing to move?
                v_tcp = str2double(get(this.gui_h.sliderLinSpeedEE,'String'));
                v_ori = str2double(get(this.gui_h.sliderAngSpeedEE,'String'));
                v_leax = str2double(get(this.gui_h.sliderLinSpeedRobot,'String'));
                v_reax = str2double(get(this.gui_h.sliderAngSpeedRobot,'String'));
                
                % Set the robot speed in the robot controller
                
                speed = [v_tcp,v_ori,v_leax,v_reax];
                
                this.robotHandle.setSpeed(speed);
                
                if this.joggingMode == 2 %  Linear
                    % getting xyz
                    XYZ(1) = str2double(get(this.gui_h.x_desired,'String'));
                    XYZ(2) = str2double(get(this.gui_h.y_desired,'String'));
                    XYZ(3) = str2double(get(this.gui_h.z_desired,'String'));
                    % roll pitch yaw unused
                    
                    % sending to controller
                    this.robotHandle.moveCartPos(XYZ);
                    
                else % joint mode
                    % getting joint values
                    
                    Joints(1) = str2double(get(this.gui_h.j1_desired,'String'));
                    Joints(2) = str2double(get(this.gui_h.j2_desired,'String'));
                    Joints(3) = str2double(get(this.gui_h.j3_desired,'String'));
                    Joints(4) = str2double(get(this.gui_h.j4_desired,'String'));
                    Joints(5) = str2double(get(this.gui_h.j5_desired,'String'));
                    Joints(6) = str2double(get(this.gui_h.j6_desired,'String'));
                    
                    % sending to controller, their job to check.
                    
                    this.robotHandle.moveJPos(Joints);
                    
                end
            end
        end
        
        %% Usage: Function for engaging vacuum
        %% Author: Isuru
        %% Date modified: 30/5/15
        function Vac_State(this, src, event)
            
            if this.vacValue == 1
                this.log('Vacuum is turned off');
                message = 'Vacuum is turned off';
                commandWindow(this, message);
                set(this.gui_h.vac_state,'string','Vaccum OFF','BackgroundColor',[0.94 0.94 0.94]);
                this.robotHandle.disableVacuumPump();
                this.vacValue = 0;
            else
                this.log('Vaccum is turned on');
                message = 'Vacuum is turned on';
                commandWindow(this, message);
                set(this.gui_h.vac_state,'string','Vaccum ON','BackgroundColor','yellow');
                this.vacValue = 1;
                this.robotHandle.enableVacuumPump();
            end
        end
        
        function Sol_State(this, src, event)
            %Toggles on/off for Solenoid functionality
            if this.vacSolValue == 1
                this.log('Solenoid is turned off');
                message = 'Solenoid is turned off';
                commandWindow(this, message);
                set(this.gui_h.disp_term, 'String', 'Solenoid is turned off', 'ForegroundColor', 'black');
                set(this.gui_h.sol_state,'string','Solenoid OFF','BackgroundColor',[0.94 0.94 0.94]);
                this.vacSolValue = 0;
                this.robotHandle.disableSuction();
            else
                this.log('Solenoid is turned on');
                message = 'Solenoid is turned on';
                commandWindow(this, message);
                set(this.gui_h.sol_state,'string','Solenoid ON','BackgroundColor','yellow');
                this.vacSolValue = 1;
                this.robotHandle.enableSuction();
            end
            
        end
        
        %% Usage: Function that toggles turns conveyor on/off
        %% Author: Isuru
        %% Date modified: 30/5/15
        function Conv_State(this, src, event)
            if this.convValue == 1
                % button is currently in stopped mode (i.e red)
                % lets change it to green and resume the robot..
                this.log('Conveyor is turned off');
                message = 'Conveyor is turned off';
                commandWindow(this, message);
                set(this.gui_h.conv_state,'string','Conveyor OFF','BackgroundColor',[0.94 0.94 0.94]);
                this.convValue = 0;
                this.robotHandle.disableConveyor();
                
            else
                % Lets set it back to the stop state...
                this.log('Conveyor is turned on');
                message = 'Conveyor is turned on';
                commandWindow(this, message);
                set(this.gui_h.conv_state,'string','Conveyor ON','BackgroundColor','yellow');
                this.convValue = 1;
                this.robotHandle.enableConveyor();
            end
        end
        
        function Conv_Dir(this, src, event)
            if this.convDir == 1
                % button is currently in stopped mode (i.e red)
                % lets change it to green and resume the robot..
                this.log('Conveyor is moving backwards');
                message = 'Conveyor is moving backwards';
                commandWindow(this, message);
                set(this.gui_h.conv_dir,'string','Conveyor BACK','BackgroundColor',[0.94 0.94 0.94]);
                this.convDir = 0;
                this.robotHandle.convDir(this.convDir);
            else
                % Lets set it back to the stop state...
                this.log('Conveyor is moving forward');
                message = 'Conveyor is moving forward';
                commandWindow(this, message);
                set(this.gui_h.conv_dir,'string','Conveyor FORWARD','BackgroundColor','yellow');
                this.convDir = 1;
                this.robotHandle.convDir(1);
            end
            this.log('ConDir ', this.convDir);
        end
        
        %% Usage: Called continuosly by matlab gui whenever a key is down hence ignored unless it changes. Assumes only one key pressed at a time
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function Key_Down(this,src,event)
            set(this.gui_h.manual_mode, 'Value', 1);
            this.robotHandle.changeMode(1);
            if this.buttonRed == 1 % if we want to move
                if isequal(event.Key,this.currentPressedKey)
                    % do nothing...we have the same key press...
                else
                    this.log(strcat('A new key is pressed:',event.Key));
                    
                    this.currentPressedKey=event.Key; % only look at first key pressed
                    
                    % Take action...
                    % A Manual Key press will override automatic mode...
                    thisInput = strcmp(event.Key,{'leftarrow','rightarrow','downarrow','uparrow','s','w','a','d','z','x','c','v'});
                    set(this.gui_h.manual_mode, 'Value', 1);
                    this.robotHandle.changeMode(1);
                    %robot
                    if max(thisInput) > 0 % we have a valid key press...
                        %if this.robotHandle.mode == manual % TODO ..??
                        this.log('Send a new manual movement command to the gui');
                        mode = get(this.gui_h.linear_mode,'Value'); % 1 for linear, 0 for joint...
                        % speed = str2double(get(this.gui_h.edit_speed)); %
                        % perhaps check for validity...  crashes, commented
                        % out + bypassed
                        v_tcp = get(this.gui_h.sliderLinSpeedEE,'Value');
                        v_ori = get(this.gui_h.sliderAngSpeedEE,'Value');
                        v_leax = get(this.gui_h.sliderLinSpeedRobot,'Value');
                        v_reax = get(this.gui_h.sliderAngSpeedRobot,'Value');
                        % 1. Set the robot speed in the robot controller
                        
                        speed = [v_tcp,v_ori,v_leax,v_reax];
                        
                        % 2. Call the appropriate motion command...
                        joystick = [0 0 0 0 0 0 ]; % vector of joint commands or cartesian..see robcontroller line 205
                        % key mapping joint : <-- j1 -, j1 + --> , ^ j2 +, v j2 -, w J3 positive, s
                        % j3 - , a j4 -,d j4 + , z j5-,x j5+,c j6-,v j6+
                        % Key mapping cartesian : < -- -x, --> +x , v -z, ^ +z , s -y, w +y
                        
                        joystick = [thisInput(2) - thisInput(1), thisInput(3) - thisInput(4), thisInput(5) - thisInput(6), thisInput(7) - thisInput(8), thisInput(9) - thisInput(10),thisInput(11) - thisInput(12)];
                        this.robotHandle.manualRobotMove(mode,joystick,speed);% tell the robot controller to send a movement command...
                    else
                        % we might want to configure some other hot keys..i.e joints
                        % etc....and pause etc...
                    end;
                end;
            end
        end;
        %% Usage: This function has same funcitonality as Key_Down function
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function Key_Up(this,src,event)
            set(this.gui_h.manual_mode, 'Value', 1);
            this.robotHandle.changeMode(1);
            this.log(strcat('A key is released:',event.Key));
            % NB: this assumes only one key press at a time
            this.currentPressedKey='';
            if max(strcmp(event.Key,{'leftarrow','rightarrow','downarrow','uparrow','s','w','a','d','z','x','c','v'})) > 0
                %if this.robotHandle.mode == manual
                this.log('Stop the robot from moving...');
                this.robotHandle.pauseRobot(); % pause the robot's motion...
            end
        end;
        
        %% Usage: This function introduces a toggle button which pauses/resumes the program
        %% Author: Isuru
        %% Date modified: 30/5/15
        function Pause_Robot(this,src,event)
            if this.buttonRed == 1
                % button is currently in stopped mode (i.e red)
                % lets change it to green and resume the robot..
                this.log('Stop the robot!!!');
                this.robotHandle.pauseRobot();
                this.buttonRed=0;
                set(this.gui_h.pause_robot,'string','Resume','BackgroundColor','green');
                set(this.gui_new.pause_robot,'string','Resume','BackgroundColor','green');
            else
                % Lets set it back to the stop state...
                this.log('Restart the robot!!!');
                this.robotHandle.restartRobot();
                this.buttonRed=1;
                set(this.gui_h.pause_robot,'string','Stop','BackgroundColor','red');
                set(this.gui_new.pause_robot,'string','Stop','BackgroundColor','red');
            end;
        end
        
        
        %% Usage: Takes the same C matrix defined in ASST1 for storing choc loactions to update image representations.s
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function updateChocolateStatus (this,chocMatrix,boxes)
            this.log('Update our chocolate Status...:',chocMatrix);
            this.chocMatrix=chocMatrix; % Save the choc matrix.
            
            % replot the new coordinates...
            colors = ['k','b','y','r','g','k','k'];
            len=length(chocMatrix(:,1));
            for i = 1:len
                % disp(i)
                % lets plot the respective numbers and boxes...
                set(this.chocLabelHandles(i),'Position',chocMatrix(i,1:2),'Color',colors(uint8(chocMatrix(i,6))+1),'EdgeColor',colors(uint8(chocMatrix(i,6)+1)),'Visible','off');
                
                if chocMatrix(i,9) < 1 % chocolate is not pickable...
                    set(this.boxHandles(i),'XData',boxes(i,1:5)','YData',boxes(i,6:10)','Color',colors(uint8(chocMatrix(i,6))+1),'Visible','on','Marker','x','LineStyle','--');
                else
                    set(this.boxHandles(i),'XData',boxes(i,1:5)','YData',boxes(i,6:10)','Color',colors(uint8(chocMatrix(i,6))+1),'Visible','on','Marker','.','LineStyle','-');
                end
                
            end
            for j = len+1:length(this.chocLabelHandles)
                % lets plot the respective numbers and boxes...
                set(this.chocLabelHandles(j),'Visible','off');
                set(this.boxHandles(j),'Visible','off');
            end
            
            % Update the status in the table...
            ID = 1:1:length(chocMatrix(:,1));
            table_array=horzcat(ID',chocMatrix(:,1:3),chocMatrix(:,6:9));
            %             set(this.display_table.choc_table, 'Data', table_array);
            set(this.gui_h.choc_table, 'Data', table_array);
            
            %update our
            milkCount = find(chocMatrix(:,6) == 1);
            milkCount = length(milkCount);
            orangeCount = find(chocMatrix(:,6) == 3);
            orangeCount=length(orangeCount);
            darkCount = find(chocMatrix(:,6) == 2);
            darkCount=length(darkCount);
            mintCount = find(chocMatrix(:,6) == 4);
            mintCount=length(mintCount);
            %             set(this.display_table.milk_cont, 'String',  num2str(milkCount));
            %             set(this.display_table.dark_cont, 'String',  num2str(darkCount));
            %             set(this.display_table.orange_cont, 'String',  num2str(orangeCount));
            %             set(this.display_table.mint_cont, 'String',  num2str(mintCount));
            set(this.gui_h.milk_cont, 'String',  num2str(milkCount));
            set(this.gui_h.dark_cont, 'String',  num2str(darkCount));
            set(this.gui_h.orange_cont, 'String',  num2str(orangeCount));
            set(this.gui_h.mint_cont, 'String',  num2str(mintCount));
        end;
        
        %% Usage: Labels fixed to chocs called once at gui init and set when we desire gui update calls
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function initConveyorBoxes(this)
            hold (this.gui_new.conveyor_image,'on');
            this.convBoxHandles = zeros(5,1);
            this.convBoxCentroids = zeros(5,1);
            this.convBoxLabels = [text(0,0,'1','Visible','off');text(0,0,'2','Visible','off');text(0,0,'3','Visible','off');text(0,0,'4','Visible','off');text(0,0,'5','Visible','off');
                text(0,0,'6','Visible','off');text(0,0,'6','Visible','off');text(0,0,'7','Visible','off');text(0,0,'8','Visible','off');text(0,0,'9','Visible','off');text(0,0,'10','Visible','off')];
            this.conveyorBoxRect = plot(0,0,'g','Parent',this.gui_new.conveyor_image,'MarkerSize',7,'Visible','off');
            this.centroidBoxConv = plot (0,0,'g*','Parent',this.gui_new.conveyor_image,'MarkerSize',10,'Visible','off');
            for i=1:5
                this.convBoxHandles(i) = plot(0,0,'Parent',this.gui_new.conveyor_image,'MarkerSize',7,'Visible','off','Color','green');
                this.convBoxCentroids(i) = plot(0,0,'r*','Parent',this.gui_new.conveyor_image,'MarkerSize',7,'Visible','off');
            end;
            
            t = 0:0.02:pi;
            x=790+850*cos(t);
            y=-20+850*sin(t);
            
            plot(this.gui_new.conveyor_image,x,y,'Color','magenta','LineWidth',0.7);
            hold (this.gui_new.conveyor_image,'off');
        end
        
        %% Usage: Takes vector of centroids, orientations and box points to know box position and draw a box around it
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function updateBoxStatus (this,centroids,orientations,boxes)
            % 1. Update the box status in the table....
            % TODO...
            this.log('Update the status of our conveyor boxes',boxes);
            % 2. Update our boxImage...with a nice line...
            len=min(length(boxes(:,1)),5);
            %set(this.conveyorBoxRect,'XData',[boxes(1,1:4) boxes(1,1)],'YData',[boxes(1,5:8),boxes(1,5)],'Visible','on');
            %set(this.centroidBoxConv,'XData',centroids(1),'YData',centroids(2),'Visible','on');
            for i = 1:len
                % update the figure...
                
                set(this.convBoxHandles(i),'XData',[boxes(i,1:4) boxes(i,1)],'YData',[boxes(i,5:8),boxes(i,5)],'Visible','on');
                set(this.convBoxCentroids(i),'XData',centroids(i,1),'YData',centroids(i,2),'Visible','on');
                set(this.convBoxLabels(i),'Position',[centroids(i,1)-200,centroids(i,2)+200],'Visible','on');
            end
            
            if len <= 4
                % We want to update the rest of our handles...
                for j = len+1:length(this.convBoxHandles)
                    set(this.convBoxHandles(j),'Visible','off');
                    set(this.convBoxCentroids(j),'Visible','off');
                    set(this.convBoxLabels(j),'Visible','off');
                end;
            end;
            % 3. Update the other figure...
            ID = 1:1:len;
            boxMatrix = horzcat(ID',centroids(:,1),centroids(:,2),orientations);
            %             set(this.display_table.box_table, 'Data', boxMatrix);
            set(this.gui_h.box_table, 'Data', boxMatrix);
        end
        
        %% Usage: Creates labels fixed to chocs called once at gui init and set when we desire gui update calls
        %% Author: Isuru
        %% Date modified: 30/5/15
        function plotBoxesInit(this)
            %hold (this.gui_h.robot_image,'on');
            hold (this.gui_new.robot_image,'on');
            
            this.chocLabelHandles = zeros(50,1);
            this.boxHandles = zeros(50,1);
            
            for i=1:50
                this.chocLabelHandles(i) = text(0,0,num2str(i),'Parent',this.gui_new.robot_image,'FontSize',3,'Visible','off');
                this.boxHandles(i,1) = line(0,0,'Parent',this.gui_new.robot_image,'LineWidth',0.8,'Visible','off');
            end;
            
            % lastly lets plot our reachable circle..
            [x,y] = this.reachableCircle(846,800,0);% robot centered at 846,800,-130 w.r.t standard frame...
            this.reachablePlot = plot(this.gui_new.robot_image,x,y,'Color','magenta','LineWidth',0.7);
            
            hold (this.gui_new.robot_image,'off');
        end
        
        function [x,y] = reachableCircle(this,r,x0,y0)
            % Function computes if the specified chocolate is able to be reached at its
            % current point on the table...
            
            % Robot is centered at
            %             x0=800;
            %             y0=-130;
            %             r=846; % for the table...
            
            t=-pi:0.2:pi;
            x=x0+r*cos(t);
            y=y0+r*sin(t);
        end
        
        %% Usage: Called by vision object to set a new image for table (image)
        %% NB: no longer setting the chocolate states in this function
        %% Author: Isuru
        %% Date modified: 30/5/15
        function out = updateTableImage (this, image, imageState, chocolateLocations, chocolateOrientations, chocolateTypes)
            try
                set(this.tableImageHandle ,'CData',image); % this sets the new image data
                this.log('Update the Table Image again...');
                
            catch e
                this.error('Unable to update the table image!!');
            end
        end
        
        %% Usage: Called by vision object to set a new image for conveyor (image) with locations of detected chocs
        %% NB: updating gui box image has been moved and need box coordinates to bet set into interface
        %% Author: Isuru
        %% Date modified: 30/5/15
        function out = updateConveyorImage (this, image, imageState, boxLocations, boxOrientations, chocolateLocations, chocolateOrientations, chocolateTypes)
            try
                set(this.conveyorImageHandle ,'CData',image); % this sets the new image data to the handle
                this.log('Update the conveyor..');
            catch e
                this.error('Unable to update conveyor image..problem in updateconveyorimage');
            end
            
        end
        
        %% Usage: gui.updateRobotMode(robotMode) called by robotHandle to update visual representation of robot mode
        %% -1 = error (estop), 0 = inactive (hasn't been started yet), 1 = active manual, 2 = active automatic
        %% Author: Isuru
        %% Date modified: 30/5/15
        function updateRobotMode (this, robotMode)
            
            switch robotMode
                case -1
                    set(this.gui_h.robot_mode_display, 'BackgroundColor', 'red', 'String', 'E-Stop');
                    set(this.gui_new.robot_mode_display, 'BackgroundColor', 'red', 'String', 'E-Stop');
                    this.error('E-stop has been activated');
                case 0
                    set(this.gui_h.robot_mode_display, 'BackgroundColor', [0.94 0.94 0.94], 'String', 'Inactive');
                    set(this.gui_new.robot_mode_display, 'BackgroundColor', [0.94 0.94 0.94], 'String', 'Inactive');
                case 1
                    set(this.gui_h.robot_mode_display, 'BackgroundColor', 'cyan', 'String', 'Active Manual');
                    set(this.gui_new.robot_mode_display, 'BackgroundColor', 'cyan', 'String', 'Active Manual');
                case 2
                    set(this.gui_h.robot_mode_display, 'BackgroundColor', 'yellow', 'String', 'Active Automatic');
                    set(this.gui_new.robot_mode_display, 'BackgroundColor', 'yellow', 'String', 'Active Automatic');
            end
            %this.log('Update the robot mode');
        end
        
        %% Usage: change the motion mode from joint to linear. Also called to robot controller to update motion mode when moving to coordinates
        %% Author: Lindsay
        %% Date modified: 20/04/15
        function Change_Motion_Mode(this, src, event)
            selected_btn = event.NewValue;
            %set us a switch/case to handle the two current possible choises
            switch selected_btn
                case this.gui_h.motion_joint_mode
                    this.motionMode = 0; % Joint Mode
                    this.log('Set robot motion mode joint');
                    % TODO - call functioin in robot controller to update
                    % the motion mode
                case this.gui_h.motion_linear_mode
                    this.motionMode = 1;
                    this.log('Set robot motion mode linear');
                    % TODO - call function in robot controller to update
                    % the motion mode...
            end
            this.robotHandle.setLinearMode(this.motionMode);
        end;
        
        %% Usage: called by robotHandle to update visual representation of IO of robot
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function out = updateRobotIOState(this,robotIO)
            this.log('Update the RobotIOState .. TODO .. link back to gui...',robotIO);
            % TODO..link this back to the gui buttons...
        end
        
        %% Usage: gui.updateRobotPose(robotPose) called by roboHandle to update gui representation of robots current pose
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function out = updateRobotPose(this,robotPosition,jointAngles,endRotation)
            out=1; % all good...
            this.robotHandle.changeMode(1);%???? whats going on here???changes to manual after button has been pressed
            % Update the data fields...
            try
                set(this.gui_h.x_actual, 'String', num2str(robotPosition(1),'%.2f'));
                set(this.gui_h.y_actual, 'String', num2str(robotPosition(2),'%.2f'));
                set(this.gui_h.z_actual, 'String', num2str(robotPosition(3),'%.2f'));
                set(this.gui_h.roll_actual, 'String', num2str(endRotation(1),'%.2f'));
                set(this.gui_h.pitch_actual, 'String', num2str(endRotation(2),'%.2f'));
                set(this.gui_h.yaw_actual, 'String', num2str(endRotation(3),'%.2f'));
                
                set(this.gui_h.j1_actual, 'String', num2str(jointAngles(1),'%.2f'));
                set(this.gui_h.j2_actual, 'String', num2str(jointAngles(2),'%.2f'));
                set(this.gui_h.j3_actual, 'String', num2str(jointAngles(3),'%.2f'));
                set(this.gui_h.j4_actual, 'String', num2str(jointAngles(4),'%.2f'));
                set(this.gui_h.j5_actual, 'String', num2str(jointAngles(5),'%.2f'));
                set(this.gui_h.j6_actual, 'String', num2str(jointAngles(6),'%.2f'));
                
                %this.log('Update the Robot Pose...TODO fix roll pitch yaw ...',robotPosition);
                %this.log('Update jointAngles', jointAngles);
            catch e
                this.error ('Cannot set robot pose in gui...');
            end
            
        end
        
        %% Usage: gui.updateRobotPose(robotPose) called by roboHandle to update gui representation of robots current pose
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function out = updateRobotDesired(this,robotPosition,jointAngles,endRotation)
            out=1; % all good...
            %
            %             try
            %                 set(this.gui_h.x_desired, 'String', num2str(robotPosition(1),'%.2f'));
            %                 set(this.gui_h.y_desired, 'String', num2str(robotPosition(2),'%.2f'));
            %                 set(this.gui_h.z_desired, 'String', num2str(robotPosition(3),'%.2f'));
            %                 set(this.gui_h.roll_desired, 'String', num2str(endRotation(1),'%.2f'));
            %                 %set(this.gui_h.pitch_desired, 'String', num2str(endRotation(2)));
            %                 %set(this.gui_h.yaw_desired, 'String', num2str(endRotation(3)));
            %
            %                 set(this.gui_h.j1_desired, 'String', num2str(jointAngles(1),'%.2f'));
            %                 set(this.gui_h.j2_desired, 'String', num2str(jointAngles(2),'%.2f'));
            %                 set(this.gui_h.j3_desired, 'String', num2str(jointAngles(3),'%.2f'));
            %                 set(this.gui_h.j4_desired, 'String', num2str(jointAngles(4),'%.2f'));
            %                 set(this.gui_h.j5_desired, 'String', num2str(jointAngles(5),'%.2f'));
            %                 set(this.gui_h.j6_desired, 'String', num2str(jointAngles(6),'%.2f'));
            %
            %                 this.log('Update the Robot Pose...TODO fix roll pitch yaw ...',robotPosition);
            %                 this.log('Update jointAngles', jointAngles);
            %             catch e
            %                 this.error ('Cannot set robot pose in gui...');
            %             end
        end
        
        function out = updateChocReachability(this,chocReach)
            chocReach = 1;
            set(this.gui_h.choc_reach, 'String', chocReach);
            this.log('Update Chocolate Reachability');
        end
        
        function setCommsState(this,state)
            if state > 0
                this.log('Comms are connected!!!');
                set(this.gui_h.comms_state,'string','Connected','BackgroundColor','green');
                try
                    set(this.gui_new.comms_state,'string','Connected','BackgroundColor','green');
                catch e
                end
                this.enableGUI(); % Comms are up..we want the gui to work...
            else
                this.log('Comms are disconnected!!!');
                set(this.gui_h.comms_state,'string','Disconnected!','BackgroundColor','red');
                try
                    set(this.gui_new.comms_state,'string','Disconnected!','BackgroundColor','red');
                catch e
                end
                this.disableGUI();
            end
            
        end
        
        %% Usage: set source of comms (simmulation or robot) or real system or not
        %% source == 0 gives simulator, 1 == gives robotStudio
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function setCommsSource(this,source)
            if source ==1
                try
                    set(this.gui_h.comms_mode,'string','Real Robot','BackgroundColor','Cyan');
                    set(this.gui_new.comms_mode,'string','Real Robot','BackgroundColor','Cyan');
                catch e
                end
            else
                try
                    set(this.gui_h.comms_mode,'string','Simulation','BackgroundColor','Magenta');
                    set(this.gui_new.comms_mode,'string','Simulation','BackgroundColor','Magenta');
                catch e
                end
            end
        end
        
        %% Usage: This function enables relevant fields in the gui
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function enableGUI (this)
            % Set the enable properties on all the required gui elements...
            set(this.gui_h.manual_mode,'Enable','on');
            set(this.gui_h.auto_mode,'Enable','on');
            set(this.gui_h.joint_mode,'Enable','on');
            set(this.gui_h.linear_mode,'Enable','on');
            set(this.gui_h.sliderLinSpeedEE,'Enable','on');
            %set(this.gui_h.update_EE_linear_speed,'Enable','on');
            set(this.gui_h.sliderAngSpeedEE,'Enable','on');
            %set(this.gui_h.update_EE_angular_speed,'Enable','on');
            set(this.gui_h.sliderLinSpeedRobot,'Enable','on');
            %set(this.gui_h.update_robot_linear_speed,'Enable','on');
            set(this.gui_h.sliderAngSpeedRobot,'Enable','on');
            %set(this.gui_h.update_robot_angular_speed,'Enable','on');
            set(this.gui_h.x_actual,'Enable','on');
            set(this.gui_h.x_desired,'Enable','on');
            set(this.gui_h.y_actual,'Enable','on');
            set(this.gui_h.y_desired,'Enable','on');
            set(this.gui_h.z_actual,'Enable','on');
            set(this.gui_h.z_desired,'Enable','on');
            set(this.gui_h.roll_actual,'Enable','on');
            set(this.gui_h.roll_desired,'Enable','on');
            set(this.gui_h.pitch_actual,'Enable','on');
            set(this.gui_h.pitch_desired,'Enable','on');
            set(this.gui_h.yaw_actual,'Enable','on');
            set(this.gui_h.yaw_desired,'Enable','on');
            set(this.gui_h.j1_actual,'Enable','on');
            set(this.gui_h.j1_desired,'Enable','on');
            set(this.gui_h.j2_actual,'Enable','on');
            set(this.gui_h.j2_desired,'Enable','on');
            set(this.gui_h.j3_actual,'Enable','on');
            set(this.gui_h.j3_desired,'Enable','on');
            set(this.gui_h.j4_actual,'Enable','on');
            set(this.gui_h.j4_desired,'Enable','on');
            set(this.gui_h.j5_actual,'Enable','on');
            set(this.gui_h.j5_desired,'Enable','on');
            set(this.gui_h.j6_actual,'Enable','on');
            set(this.gui_h.j6_desired,'Enable','on');
            
            set(this.gui_h.update_robot_pose,'Enable','on');
            set(this.gui_h.pause_robot,'Enable','on');
            set(this.gui_h.vac_state,'Enable','on');
            set(this.gui_h.sol_state,'Enable','on');
            set(this.gui_h.conv_state,'Enable','on');
            set(this.gui_h.conv_dir,'Enable','on');
            set(this.gui_h.motion_joint_mode,'Enable','on');
            set(this.gui_h.motion_linear_mode,'Enable','on');
            
        end
        
        %% Usage: This function will enable relevant fields in the gui
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function disableGUI (this)
            % Set the enable properties on all the required gui elements...
            set(this.gui_h.manual_mode,'Enable','off');
            set(this.gui_h.auto_mode,'Enable','off');
            set(this.gui_h.joint_mode,'Enable','off');
            set(this.gui_h.linear_mode,'Enable','off');
            set(this.gui_h.sliderLinSpeedEE,'Enable','off');
            %             set(this.gui_h.update_EE_linear_speed,'Enable','off');
            set(this.gui_h.sliderAngSpeedEE,'Enable','off');
            %             set(this.gui_h.update_EE_angular_speed,'Enable','off');
            set(this.gui_h.sliderLinSpeedRobot,'Enable','off');
            %             set(this.gui_h.update_robot_linear_speed,'Enable','off');
            set(this.gui_h.sliderAngSpeedRobot,'Enable','off');
            %             set(this.gui_h.update_robot_angular_speed,'Enable','off');
            set(this.gui_h.x_actual,'Enable','off');
            set(this.gui_h.x_desired,'Enable','off');
            set(this.gui_h.y_actual,'Enable','off');
            set(this.gui_h.y_desired,'Enable','off');
            set(this.gui_h.z_actual,'Enable','off');
            set(this.gui_h.z_desired,'Enable','off');
            set(this.gui_h.roll_actual,'Enable','off');
            set(this.gui_h.roll_desired,'Enable','off');
            set(this.gui_h.pitch_actual,'Enable','off');
            set(this.gui_h.pitch_desired,'Enable','off');
            set(this.gui_h.yaw_actual,'Enable','off');
            set(this.gui_h.yaw_desired,'Enable','off');
            set(this.gui_h.j1_actual,'Enable','off');
            set(this.gui_h.j1_desired,'Enable','off');
            set(this.gui_h.j2_actual,'Enable','off');
            set(this.gui_h.j2_desired,'Enable','off');
            set(this.gui_h.j3_actual,'Enable','off');
            set(this.gui_h.j3_desired,'Enable','off');
            set(this.gui_h.j4_actual,'Enable','off');
            set(this.gui_h.j4_desired,'Enable','off');
            set(this.gui_h.j5_actual,'Enable','off');
            set(this.gui_h.j5_desired,'Enable','off');
            set(this.gui_h.j6_actual,'Enable','off');
            set(this.gui_h.j6_desired,'Enable','off');
            
            set(this.gui_h.update_robot_pose,'Enable','off');
            set(this.gui_h.pause_robot,'Enable','off');
            set(this.gui_h.vac_state,'Enable','off');
            set(this.gui_h.sol_state,'Enable','off');
            set(this.gui_h.conv_state,'Enable','off');
            set(this.gui_h.conv_dir,'Enable','off');
            set(this.gui_h.motion_joint_mode,'Enable','off');
            set(this.gui_h.motion_linear_mode,'Enable','off');
            
        end
        
        %% Usage: Called by robot controller an advise user any commands sent to/from robot system.
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function out = updateCommsList (this,comms)
            try
                out=0;
                comms = 'Send this command';
                commandWindow(comms);
                set(this.gui_h.command_disp, 'String', comms);
                set(this.gui_h.move_state, 'String', comms);
                set(this.gui_h.sio_state, 'String', comms);
                this.log('Update the comms state list');
            catch e
                e
                this.error('Unknown problem when trying to update comms list');
            end
        end
        
        %displays errors and important input acknowledgements on GUI
        function commandWindow(this, message)
            t=clock();
            outputString = sprintf('%d:%d:%2.0f - %s',t(4),t(5),t(6),message);
            oldmsgs = cellstr(get(this.gui_h.disp_term,'String'));
            set(this.gui_h.disp_term, 'String',[{outputString}; oldmsgs] );
        end
        
        %% Usage: Called by robot controller an advise user any errors to robot
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function out = dispRobotError (this,error)
            if (error == 1)
                errordlg('E-stop has been triggered and communication has been lost','E-stop');
                Manager.stop;
            end
            
            %for target point not reachable Refer to 4.3.3
            if (error == 2)
                errordlg('Target Point not Reachable', 'Not reachable');
            end
            out=0;
            
            this.log('Update the Robot Errors in gui');
        end
        
        %% Usage: Implements abstract methods from the interface
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function addHandles (this,robotHandle,managerHandle,pickerHandle)
            % addHandles : adds the gui object handle, siocomms handle and
            this.robotHandle=robotHandle;
            this.managerHandle=managerHandle; % gui has a handle back to the manager
            this.pickerHandle=pickerHandle;
            % now we can do things to the manager from the gui, like
            % destroying the program properly...
        end
        
        %% Usage: class deconstructor handles cleaning of class and figure
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function delete(this)
            %remove the closerequestfcn from the figure, this prevents an
            %infitie loop with the following delete command
            set(this.gui_h.figure1,  'closerequestfcn', '');
            set(this.gui_new.figure1,'closerequestfcn','');
            
            % tell the manager to destroy itself...
            this.managerHandle.destroyProgram();
            %delete the figure
            delete(this.gui_h.figure1);
            delete(this.gui_new.figure1);
            %clear out the pointer to the figure - prevents memory leaks
            this.gui_h = [];
            this.gui_new =[];
        end
        
        
    end
    
    methods (Access = protected)
        % Gui CallBack Functions Go here ...
        %movement test function
        %function - Close_fcn
        
        %% Usage: closerequestfcn of the figure from class delete function above
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function this = Close_fcn(this, src, event)
            delete(this);
        end
        
        %% Usage: Callback function for the reset button directly
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function this = Reset_callback(this, src, event)
            this = Reset(this);
        end
        
    end
end

