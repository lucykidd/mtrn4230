classdef GUI < GUI_Base
    %% GUI : This class is used to handle everything to do with the gui. It
    % will have public functions that other classes can call to update the
    % gui (i.e gui.drawImage to draw the image from the vision processor),
    % and private callback methods.
    
    %% NB: GUI now extends from GUI_base...this was a simple way to ensure that we kept our old gui without having to
    % use a new gui...
    %% Usage : Displays a GUI
    %% Author : Isuru & Martin
    %% Date last modified: 05/05/2015
    
    %% Properties
    properties (Access = public)
        order_array;
    end
    
    properties (Access = private)
        stopbuttonRed;
        milk;
        dark;
        oj;
        mint;
        advSelected; % bool for the advanced Selected field
        orderPaused; % bool to know if order is on hold or not
        boxSelected;
        previousPickerState;
    end
    
    %% Usage: Defines public Methods used throughout this class
    %% Author: Isuru, Martin, Patrick, Lindsay
    %% Date modified: 30/5/15
    methods (Access = public)
        function this = GUI()
            this.previousPickerState = 0;
            
            this.stopbuttonRed = 1;
            milk = 1;
            dark = 2;
            oj = 3;
            mint = 4;
            this.advSelected=1;
            this.order_array = [];
            this.orderPaused = 0;
            %set initial values for flavour order
            set(this.gui_new.milk_order_input, 'String', '0');
            set(this.gui_new.dark_order_input, 'String', '0');
            set(this.gui_new.oj_order_input, 'String', '0');
            set(this.gui_new.mint_order_input, 'String', '0');
            
            %initialising callbacks
            set(this.gui_new.send_flav_order, 'callback', @(src, event) sendFlavourOrder(this, src, event));
            set(this.gui_new.pattern_checkbox, 'callback', @(src, event) toggleAccessOrder(this, src, event));
            set(this.gui_new.milk_button, 'callback', @(src, event) placePatternOrder(this, src, event, milk));
            set(this.gui_new.dark_button, 'callback', @(src, event) placePatternOrder(this, src, event, dark));
            set(this.gui_new.oj_button, 'callback', @(src, event) placePatternOrder(this, src, event, oj));
            set(this.gui_new.mint_button, 'callback', @(src, event) placePatternOrder(this, src, event, mint));
            set(this.gui_new.advanced_button, 'callback', @(src, event) hideAdvanced(this, src, event));
            
            set(this.gui_new.send_order, 'callback', @(src,event) sendPatternOrder(this,src,event));
            set(this.gui_new.clear_order, 'callback', @(src,event) clearOrder(this,src,event));
            set(this.gui_new.pause_order, 'callback', @(src,event) pauseOrder(this,src,event));
            set(this.gui_new.cancel_order, 'callback', @(src,event) cancelOrder(this,src,event));
            %Sends push button command to unload box
            set(this.gui_new.unload_box, 'callback', @(src,event) unloadBox(this,src,event));
            set(this.gui_new.figure1,'KeyPressFcn',@(src, event) new_Key_Down(this, src, event));
            set(this.gui_new.flip_chocs, 'callback', @(src,event) flipChocs(this,src,event));
            set(this.gui_new.stack_chocs, 'callback', @(src,event) stackChocs(this, src, event));
            
            set(this.gui_new.pause_order, 'callback', @(src, event) pauseOrder(this, src, event));
            set(this.gui_new.box_selection, 'selectionchangefcn', @(src, event)selectBox(this, src, event));
            
            %box 1 is selected by default
            default_selection = get(this.gui_new.box_1_radio, 'Value');
            if default_selection == 1
                this.boxSelected = 1;
                message = 'Box 1 is the default selection';
                orderCommandWindow(this, message);
            else
                this.boxSelected = 0;
            end
            
        end
        
        %% Usage: Callback for pausing the order so that robot stops it's current movement
        %% Author: Isuru, Martin
        %% Date modified: 30/5/15
        function pauseOrder(this, src, event)
            if this.orderPaused == 0
                this.robotHandle.pauseRobot();
                this.pickerHandle.pauseOrder();
                this.orderPaused = 1;
                set(this.gui_new.pause_order,'string','Resume order','BackgroundColor',[0,0.5,0.5]);
            else
                this.orderPaused = 0;
                this.robotHandle.restartRobot(); % dodgy name, but should only be a resume
                this.pickerHandle.resumeOrder();
                set(this.gui_new.pause_order,'string','Pause order','BackgroundColor',[0.5,0,0.5]);
            end
            return
        end
        
        %% Usage: Hides the advanced GUI from the user as it is only used as a debugging tool
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function hideAdvanced(this, src, event)
            % get the button value
            
            if this.advSelected == 0
                set(this.gui_new.advanced_button,'String','Hide Advanced GUI','BackgroundColor','magenta');
                this.advSelected=1;
                set(this.gui_h.figure1,'Visible','on');
            else
                set(this.gui_new.advanced_button,'String','Show Advanced GUI','BackgroundColor','cyan');
                this.advSelected=0;
                set(this.gui_h.figure1,'Visible','off');
            end
        end
        
        %% Usage: Callback for sending flavour order by number of choclates instead of pattern order
        %% Author:Martin
        %% Date modified: 30/5/15
        function sendFlavourOrder(this,src,event)
            milk_count = str2double(get(this.gui_new.milk_order_input, 'String'));
            dark_count = str2double(get(this.gui_new.dark_order_input, 'String'));
            oj_count = str2double(get(this.gui_new.oj_order_input, 'String'));
            mint_count = str2double(get(this.gui_new.mint_order_input, 'String'));
            
            msgbox('order has been sent');
            message = 'We are working on it. If you wanted it faster, you should have asked 3PO';
            orderCommandWindow(this, message);
            %this is the final array that is sent ---> Needs to be linked
            %up appropriately
            flavour_order = [milk_count, dark_count, oj_count, mint_count];
            
            Chocolates = this.chocMatrix; % still have to grab the correct chocolate matrix
            this.pickerHandle.takeOrder(Chocolates,flavour_order,[],0);
        end
        
        %% Usage: Toggles the selesction of count order or pattern order
        %% Author: Isuru
        %% Date modified: 30/5/15
        function toggleAccessOrder(this,src,event)
            pattern_toggle = get(this.gui_new.pattern_checkbox, 'Value');
            if pattern_toggle == 1
                this.enableOrder();
            else
                this.disableOrder();
            end
        end
        
        %% Usage: Callback for placing a pattern order . Creates an array that records user input.
        %% Author: Isuru
        %% Date modified: 30/5/15
        function placePatternOrder(this, src, event, flavour)
            %milk = 1, dark = 2, oj = 3, mint = 4
            switch flavour
                case 1
                    this.order_array = [this.order_array; {'Milk'}];
                case 2
                    this.order_array = [this.order_array; {'Dark'}];
                case 3
                    this.order_array = [this.order_array; {'Orange'}];
                case 4
                    this.order_array = [this.order_array; {'Mint'}];
            end
            
            if length(this.order_array) > 12
                msgbox('You can only order up to 12 chocolates');
                this.order_array(13:end) = []; %tuncates vector to 12
            else
                set(this.gui_new.pattern_table, 'Data', this.order_array);
            end
        end
        
        %% Usage: Key press function used to debug
        %% Author: Lindsay
        %% Date modified: 30/5/15
        function new_Key_Down(this,src,event)
            % This function is called continuosly by the new matlab gui whenever a
            % new key is pressed down...
            % this allows hot keys to be used in the program...
            key = event.Key;
            
            if key=='t'
                % trigger a new table image capture
                this.managerHandle.vision.triggerProcessTableIm(); % NB: this is a very hacky way of doing things
            elseif key == 'c'
                % trigger a new conveyor image capture
                this.managerHandle.vision.triggerProcessConvIm(); % NB: hack way of doing business...
            elseif key == 'b'
                this.managerHandle.vision.blockProcessTableIm(); % Hack! very bad!
            end
            
        end;
        
        %% Usage: Callback for sending pattern order
        %% Author: Isuru, Martin
        %% Date modified: 30/5/15
        function sendPatternOrder(this,src,event)
            disp('This is the final pattern order');
            msgbox('Order has been sent');
            message = 'Order has been sent';
            orderCommandWindow(this, message);
            visualiseOrder(this, src, event);
            disp(this.order_array);%-----> Send this order array to robot controller
            
            milks = length(find(strcmp(this.order_array, 'Milk')));
            darks = length(find(strcmp(this.order_array, 'Dark')));
            oranges = length(find(strcmp(this.order_array, 'Orange')));
            mints = length(find(strcmp(this.order_array, 'Mint')));
            order = [milks,darks,oranges,mints];
            
            
            % making useful values out of those silly arrays...
            sequence = zeros(order(1)+order(2)+order(3)+order(4),1);
            
            II = find(strcmp(this.order_array, 'Milk'));
            sequence(II) = 1;
            
            II = find(strcmp(this.order_array, 'Dark'));
            sequence(II) = 2;
            II = find(strcmp(this.order_array, 'Orange'));
            sequence(II) = 3;
            II = find(strcmp(this.order_array, 'Mint'));
            sequence(II) = 4;
            
            Chocolates = this.chocMatrix;
            % have to grab the Chocolates somehow
            this.pickerHandle.takeOrder(Chocolates,order,sequence,1);
        end
        
        %% Usage: Visualises the pattern order on a table with appropriate colour scheme
        %% Author: Isuru
        %% Date modified: 30/5/15
        function visualiseOrder(this, src, event)
            %for visualisation into textbox
            bg_red = [1 0 0];%for chocolates to complete
            bg_green = [0 1 0];%for completed chocolates
            bg_yellow = [1 1 0];%for chocolates that are being currently chosen
            
            clr = dec2hex(round(bg_red*255),2)';
            clg = dec2hex(round(bg_green*255),2)';
            cly = dec2hex(round(bg_yellow*255),2)';
            
            clr = ['#';clr(:)]';
            cly = ['#';cly(:)]';
            clg = ['#';clg(:)]';
            %sets all background cells to red initially
            not_complete = strcat(...
                ['<html><body bgcolor="' clr '" text="#000000" width="100px">'], ...
                this.order_array(:,1));
            set(this.gui_new.pattern_table, 'Data', not_complete);
            
            %%DUMMY CODE for visualising robot feedback with background
            %colours of the table
            move_1 = 1;%robot moves to first chocolate to pick up and bring to box
            
            if move_1 == 1
                processed = [];
                processing = strcat(...
                    ['<html><body bgcolor="' cly '" text="#000000" width="100px">'], ...
                    this.order_array(1,1));
                not_processed = strcat(...
                    ['<html><body bgcolor="' clr '" text="#000000" width="100px">'], ...
                    this.order_array(2:end,1));
                final = [processed; processing; not_processed];
                set(this.gui_new.pattern_table, 'Data', final);
                message = 'Chocolate 1 has been delivered';
                orderCommandWindow(this, message);
            end
            
            move_1 = 2;%finished delivering first chocolate to box
            
            if move_1 == 2
                processed = strcat(...
                    ['<html><body bgcolor="' clg '" text="#000000" width="100px">'], ...
                    this.order_array(1,1));
                processing = [];
                not_processed = strcat(...
                    ['<html><body bgcolor="' clr '" text="#000000" width="100px">'], ...
                    this.order_array(2:end,1));
                final = [processed; processing; not_processed];
                set(this.gui_new.pattern_table, 'Data', final);
            end
            
            move_2 = 1;%robot moves to second chocolate to pick up
            
            if move_2 == 1
                processed = strcat(...
                    ['<html><body bgcolor="' clg '" text="#000000" width="100px">'], ...
                    this.order_array(1,1));
                processing = strcat(...
                    ['<html><body bgcolor="' cly '" text="#000000" width="100px">'], ...
                    this.order_array(2,1));
                not_processed = strcat(...
                    ['<html><body bgcolor="' clr '" text="#000000" width="100px">'], ...
                    this.order_array(3:end,1));
                final = [processed; processing; not_processed];
                set(this.gui_new.pattern_table, 'Data', final);
            end
            
            move_2 = 2;%finished delivering second chocolate to box
            
            
            if move_2 == 2
                processed = strcat(...
                    ['<html><body bgcolor="' clg '" text="#000000" width="100px">'], ...
                    this.order_array(1:2,1));
                processing = [];
                not_processed = strcat(...
                    ['<html><body bgcolor="' clr '" text="#000000" width="100px">'], ...
                    this.order_array(3:end,1));
                final = [processed; processing; not_processed];
                set(this.gui_new.pattern_table, 'Data', final);
                message = 'Chocolate 2 has been delivered';
                orderCommandWindow(this, message);
            end
            
        end
        
        %% Usage: Callback for clearing an incorrect pattern order
        %% Author: Isuru, Martin
        %% Date modified: 30/5/15
        function clearOrder(this, src, event)
            this.order_array = [];%clears the array
            set(this.gui_new.pattern_table, 'Data', this.order_array);
        end
        
        %% Usage: Callback for cancelling an order
        %% Author: Isuru, Martin
        %% Date modified: 30/5/15
        function cancelOrder(this, src, event)
            this.order_array = [];%clears the array
            set(this.gui_new.pattern_table, 'Data', this.order_array);
            msgbox('Order has been cancelled');
            message = 'Order has been cancelled';
            orderCommandWindow(this, message);
            %Send cancel to robot controller
            this.pickerHandle.cancelOrder();
        end
        
        %% Usage: Callback for unloading chocolates out of the box on the conveyor
        %% Author: Isuru, Martin
        %% Date modified: 30/5/15
        function  unloadBox(this, src, event)
            %Send unload box command
            h = msgbox('Box is being unloaded by the IRB200');
            message = 'Box is being unloaded by the IRB200';
            orderCommandWindow(this, message);
        end
        
        %% Usage: Displays a command window for user feedback
        %% Author: Martin
        %% Date modified: 30/5/15
        function orderCommandWindow(this, message)
            t=clock();
            outputString = sprintf('%d:%d:%2.0f - %s',t(4),t(5),t(6),message);
            oldmsgs = cellstr(get(this.gui_new.order_window,'String'));
            set(this.gui_new.order_window, 'String',[{outputString}; oldmsgs] );
        end
        
        %% Usage: Displays the state of operation to the end user.
        % states between -1 and 9, -1 is "order impossible" state
        % read function for complete value <> state equivalence.
        %% Author: Patrick
        %% Date modified: 30/5/15
        function displayPickerState(this,state)
            message = 'unknown state';
            switch(state)
                case 0
                    message = 'checking and cleaning stacking area';
                case 1
                    message = 'looking for and stacking available chocolates';
                case 2
                    message = 'looking for and stacking overlapped chocolates';
                case 3
                    message = 'looking for and stacking flipped chocolates';
                case 4
                    message = 'emptying the box';
                case 5
                    message = 'preparing order';
                case 6
                    message = 'getting a new picture';
                case 7
                    message = 'order possible';
                    h = msgbox('Order seems possible.');
                case 8
                    message = 'box sent out';
                    h = msgbox('Box has been sent out, nom nom much');
                case 9
                    message = 'emptying the stack on conveyor';
                case -1 % when order is impossible
                    message = 'cannot do this order';
                    % pop up a box
                    h = msgbox('Sorry, cannot fulfill the order.');
            end
            % avoiding a potential flood
            if (state ~= this.previousPickerState)
                this.previousPickerState = state;
                this.orderCommandWindow(message);
            end
        end
        
        %% Usage: Callback for flipping chocolates
        %% Author: Isuru, Martin
        %% Date modified: 30/5/15
        function flipChocs(this, src, event)
            message = 'Chocolates are being flipped';
            orderCommandWindow(this, message);
        end
        
        %% Usage: Callback for stacking chocolates chocolates
        %% Author: Isuru
        %% Date modified: 30/5/15
        function stackChocs(this, src, event)
            message = 'Chocolates are being stacked';
            orderCommandWindow(this, message);
        end
        
        %% Usage: Callback for selecting between maximum of two boxes
        %% Author: Isuru, Martin
        %% Date modified: 30/5/15
        function selectBox(this, src, event)
            
            selected_btn = event.NewValue;
            
            switch selected_btn
                case this.gui_new.box_1_radio
                    this.boxSelected = 1;
                    message = 'Box 1 has been selected';
                    orderCommandWindow(this, message);
                case this.gui_new.box_2_radio
                    this.boxSelected = 2;
                    message = 'Box 2 has been selected';
                    orderCommandWindow(this, message);
            end
        end
        
        %% Usage: Enables the selectibility of buttons
        %% Author: Isuru
        %% Date modified: 30/5/15
        function enableOrder(this)
            %enables table
            set(this.gui_new.pattern_table, 'Enable', 'on');
            
            %enables button
            set(this.gui_new.milk_button, 'Enable', 'on');
            set(this.gui_new.dark_button, 'Enable', 'on');
            set(this.gui_new.oj_button, 'Enable', 'on');
            set(this.gui_new.mint_button, 'Enable', 'on');
            set(this.gui_new.send_order, 'Enable', 'on');
            set(this.gui_new.clear_order, 'Enable', 'on');
            set(this.gui_new.unload_box, 'Enable', 'on');
            
            %sets background/font colours
            set(this.gui_new.milk_button, 'BackgroundColor', [0 0.45 0.74]);%blue colour
            set(this.gui_new.dark_button, 'BackgroundColor', [0.45 0.26 0.26]);%brown colour
            set(this.gui_new.oj_button, 'BackgroundColor', [0.85 0.33 0.1]);%orange colour
            set(this.gui_new.mint_button, 'BackgroundColor', [0 0.5 0]);%mint colour
            set(this.gui_new.send_order, 'BackgroundColor', 'cyan');
            set(this.gui_new.clear_order, 'BackgroundColor', 'yellow');
            set(this.gui_new.unload_box, 'BackgroundColor', [0.75 0 0.75]);
            
            set(this.gui_new.text107, 'ForegroundColor', [0 0 0]);
            
            %disables input order if pattern order is selected
            set(this.gui_new.milk_order_input, 'Enable', 'off');
            set(this.gui_new.dark_order_input, 'Enable', 'off');
            set(this.gui_new.oj_order_input, 'Enable', 'off');
            set(this.gui_new.mint_order_input, 'Enable', 'off');
            
            set(this.gui_new.text102, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.text102, 'Enable', 'off');
            set(this.gui_new.text108, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.text108, 'Enable', 'off');
            set(this.gui_new.text109, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.text109, 'Enable', 'off');
            set(this.gui_new.text110, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.text110, 'Enable', 'off');
            
            set(this.gui_new.send_flav_order, 'Enable', 'off');
            set(this.gui_new.send_flav_order, 'BackgroundColor', [0.8 0.8 0.8]);
        end
        
        %% Usage: Disables the selectivity for an order
        %% Author: Isuru
        %% Date modified: 30/5/15
        function disableOrder(this)
            set(this.gui_new.pattern_table, 'Enable', 'off');
            
            set(this.gui_new.milk_button, 'Enable', 'off');
            set(this.gui_new.dark_button, 'Enable', 'off');
            set(this.gui_new.oj_button, 'Enable', 'off');
            set(this.gui_new.mint_button, 'Enable', 'off');
            set(this.gui_new.send_order, 'Enable', 'off');
            set(this.gui_new.clear_order, 'Enable', 'off');
            set(this.gui_new.unload_box, 'Enable', 'off');
            
            %if disabled set background colour to grey
            set(this.gui_new.milk_button, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.dark_button, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.oj_button, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.mint_button, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.send_order, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.clear_order, 'BackgroundColor', [0.8 0.8 0.8]);
            set(this.gui_new.unload_box, 'BackgroundColor', [0.8 0.8 0.8]);
            
            
            %enables input order properties
            set(this.gui_new.milk_order_input, 'Enable', 'on');
            set(this.gui_new.dark_order_input, 'Enable', 'on');
            set(this.gui_new.oj_order_input, 'Enable', 'on');
            set(this.gui_new.mint_order_input, 'Enable', 'on');
            
            set(this.gui_new.text102, 'BackgroundColor', [0 0.45 0.74]);
            set(this.gui_new.text102, 'Enable', 'on');
            set(this.gui_new.text108, 'BackgroundColor', [0.45 0.26 0.26]);
            set(this.gui_new.text108, 'Enable', 'on');
            set(this.gui_new.text109, 'BackgroundColor', [0.85 0.33 1.0]);
            set(this.gui_new.text109, 'Enable', 'on');
            set(this.gui_new.text110, 'BackgroundColor', [0 0.5 0]);
            set(this.gui_new.text110, 'Enable', 'on');
            
            set(this.gui_new.send_flav_order, 'Enable', 'on');
            set(this.gui_new.send_flav_order, 'BackgroundColor', 'cyan');
            
        end
        
        
    end
    
    methods (Access = private)
        
    end
end

