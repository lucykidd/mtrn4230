function varargout = StandardGUI(varargin)
% STANDARD_FIG MATLAB code for standard_fig.fig
%      STANDARD_FIG, by itself, creates a new STANDARD_FIG or raises the existing
%      singleton*.
%
%      H = STANDARD_FIG returns the handle to a new STANDARD_FIG or the handle to
%      the existing singleton*.
%
%      STANDARD_FIG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STANDARD_FIG.M with the given input arguments.
%
%      STANDARD_FIG('Property','Value',...) creates a new STANDARD_FIG or raises
%      the existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before standard_fig_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to standard_fig_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help standard_fig

% Last Modified by GUIDE v2.5 28-Apr-2015 08:53:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @standard_fig_OpeningFcn, ...
                   'gui_OutputFcn',  @standard_fig_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before standard_fig is made visible.
function standard_fig_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to standard_fig (see VARARGIN)

% Choose default command line output for standard_fig
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

initialize_gui(hObject, handles, false);

% UIWAIT makes standard_fig wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = standard_fig_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the reset_btn flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to reset_btn the data.
% if isfield(handles, 'metricdata') && ~isreset
%     return;
% end
% 
% handles.metricdata.density = 0;
% handles.metricdata.volume  = 0;
% 
% set(handles.density_box, 'String', handles.metricdata.density);
% set(handles.volume_box,  'String', handles.metricdata.volume);
% set(handles.mass_box, 'String', 0);
% 
% set(handles.unitgroup, 'SelectedObject', handles.english_radio_btn);
% 
% set(handles.text4, 'String', 'lb/cu.in');
% set(handles.text5, 'String', 'cu.in');
% set(handles.text6, 'String', 'lb');

% Update handles structure
guidata(handles.figure1, handles);


% Hint: get(hObject,'Value') returns toggle state of conv_check


% --- Executes on key press with focus on figure1 and none of its controls.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
