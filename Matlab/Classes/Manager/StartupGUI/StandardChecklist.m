
classdef StandardChecklist < handle  
    % A simple class to handle the standard checklist required at
    % startup...
    %class properties - access is private so nothing else can access these
    %variables. Useful in different sitionations
    properties (Access = private)
        
        gui_h;
        checkboxes;
        enabled;
        manager;
    end
    
    methods
        
        %function - class constructor - creates and init's the gui
        function this = StandardChecklist(myManager)
            
            % Create a handle to our manager object
            this.manager=myManager;
            
            % Creat a checkboxes object...
            this.checkboxes=zeros(1,7);
            this.enabled=0;
            
            %make the gui handle and store it locally
            this.gui_h = guihandles(StandardGUI);
            
            % Checkbox callbacks...
            set(this.gui_h.flex_check, 'callback', @(src, event) Startup_Check(this, src, event));
            set(this.gui_h.obs_check, 'callback', @(src, event) Startup_Check(this, src, event));
            set(this.gui_h.light_check, 'callback', @(src, event) Startup_Check(this, src, event));
            set(this.gui_h.mode_check, 'callback', @(src, event) Startup_Check(this, src, event));
            set(this.gui_h.power_check, 'callback', @(src, event) Startup_Check(this, src, event));
            set(this.gui_h.vac_check, 'callback', @(src, event) Startup_Check(this, src, event));
            set(this.gui_h.conv_check, 'callback', @(src, event) Startup_Check(this, src, event));
            set(this.gui_h.figure1,'KeyPressFcn',@(src, event) Key_Down(this, src, event));
            
            % Options check backs...
            set(this.gui_h.comms, 'selectionchangefcn', @(src, event)Comms_Change(this, src, event));
            set(this.gui_h.output, 'selectionchangefcn', @(src, event)Output_Change(this, src, event));
            
            % Start button
            set(this.gui_h.start_button, 'callback', @(src, event) Start_Button(this, src, event));
            
            set(this.gui_h.figure1,  'closerequestfcn', @(src,event) Close_fcn(this, src, event));
        end
        
        function this = Startup_Check(this, src, event)
        % A startup checkbox is changed...
            % Get all checkbox values...
            this.checkboxes(1) = get(this.gui_h.flex_check,'Value');
            this.checkboxes(2) = get(this.gui_h.obs_check,'Value');
            this.checkboxes(3) = get(this.gui_h.light_check,'Value');
            this.checkboxes(4) = get(this.gui_h.mode_check,'Value');
            this.checkboxes(5) = get(this.gui_h.power_check,'Value');
            this.checkboxes(6) = get(this.gui_h.vac_check,'Value');
            this.checkboxes(7) = get(this.gui_h.conv_check,'Value');
            
            % If all enabled...enable button
            if min(this.checkboxes) >= 1
                this.enabled=1;
                set(this.gui_h.start_button,'string','Start','BackgroundColor','green');
            else
                this.enabled=0;
                set(this.gui_h.start_button,'string','Unable to Start!','BackgroundColor','red');
            end;
        end
        
        function Key_Down(this,src,event)
            % This function is called continuosly by matlab gui
            % whenever a key is down hence is ignored unless it
            % changes. This assumes only one key press at a time
            
            currentPressedKey=event.Key; % only look at first key pressed
            
            % Take action...
            if strcmp(event.Key,'a')==1
               % lets select all boxes..
                set(this.gui_h.flex_check,'Value',1);
                set(this.gui_h.obs_check,'Value',1);
                set(this.gui_h.light_check,'Value',1);
                set(this.gui_h.mode_check,'Value',1);
                set(this.gui_h.power_check,'Value',1);
                set(this.gui_h.vac_check,'Value',1);
                set(this.gui_h.conv_check,'Value',1);
                this.Startup_Check(0,0);
            end
        end;
        
        function this = Comms_Change(this, src, event)
        % A startup checkbox is changed...
            selected_btn = event.NewValue;
            switch selected_btn
                case this.gui_h.sim
                    this.manager.setStartupComms(0);
                case this.gui_h.real
                    this.manager.setStartupComms(1);
            end
        end
        
        function this = Start_Button(this, src, event)
        % A startup checkbox is changed...
            if this.enabled == 1
               % Tell the manager to start ....
               this.manager.runProgram();
               
               % delay for a second
               % pause(1);
               % perhaps display something here
               
               % close me
               delete(this); % lets close this gui...
            else
                
            end
        end
        
        function this = Output_Change(this, src, event)
            % log mode is changed
            selected_btn = event.NewValue;
            switch selected_btn
                case this.gui_h.log
                    this.manager.setStartupLogs(1);
                case this.gui_h.none
                    this.manager.setStartupLogs(-1);
                case this.gui_h.comm_window
                    this.manager.setStartupLogs(0);
            end
            
        end
        
    end
    
    
    %Private Class Methods - these functions can only be access by the
    %class itself.
    methods (Access = private)
        
        %class deconstructor - handles the cleaning up of the class &
        %figure. Either the class or the figure can initiate the closing
        %condition, this function makes sure both are cleaned up
        function delete(this)
            %remove the closerequestfcn from the figure, this prevents an
            %infitie loop with the following delete command
            set(this.gui_h.figure1,  'closerequestfcn', '');
            %delete the figure
            delete(this.gui_h.figure1);
            %clear out the pointer to the figure - prevents memory leaks
            this.gui_h = [];
        end
        
        %function - Close_fcn
        %
        %this is the closerequestfcn of the figure. All it does here is
        %call the class delete function (presented above)
        function this = Close_fcn(this, src, event)
            Manager.destroy(); % tell manager to destroy itself...
            delete(this);
        end
        
        %function - Reset
        %
        %resets the gui to initial values. Called from the Reset_btn
        %callback and when the gui init's.
        %This function is mainly kept to mirror the functionality of the
        %MATLAB guide example
        function this = Reset(this)
            
           
        end
        
        %function - Reset_callback
        %
        %the callback function for the reset button. This simply calls
        %Reset function directly
        function this = Reset_callback(this, src, event)
            this = Reset(this);
        end
        
         %%PUT ME WHERE APPROPRIATE. I.E RIGHT AFTER GUI STARTS
        function out = Startup_Checklist(this, src, event)
            check1 = questdlg('Is the Flex Pendant placed in its� appropriate position?', ...
                'Checklist', ...
                'Yes','No');
            switch check1
                case 'Yes'
                    set(this.gui_h.flex_check, 'Value', 1);
                case 'No'
                    errordlg('Ensure that Flex Pendant is placed in appropriate position', 'Fail');
                    Manager.destroy;
            end
            
            check2 = questdlg('Are unnecessary items in the Robot�s working envelop or conveyor removed?', ...
                'Checklist', ...
                'Yes','No');
            
            switch check2
                case 'Yes'
                    set(this.gui_h.obs_check, 'Value', 1);
                case 'No'
                    errordlg('Ensure obstructions are removed from workspaces', 'Fail');
                    Manager.destroy;
            end
            
            check3 = questdlg('Is the Light Curtain operational?', ...
                'Checklist', ...
                'Yes','No');
            
            switch check3
                case 'Yes'
                    set(this.gui_h.light_check, 'Value', 1);
                case 'No'
                    errordlg('Ensure Light Curtain is Operational', 'Fail');
                    Manager.destroy;
            end
            
            check4 = questdlg('Is the Mode Switch on the front panel of the Robot Controller in MANUAL mode?', ...
                'Checklist', ...
                'Yes','No');
            
            switch check4
                case 'Yes'
                    set(this.gui_h.mode_check, 'Value', 1);
                case 'No'
                    errordlg('Ensure Robot Controller is in MANUAL Mode', 'Fail');
                    Manager.destroy;
            end
            
            check5 = questdlg('Is the power switch for the Robot Controller turned on?', ...
                'Checklist', ...
                'Yes','No');
            
            switch check5
                case 'Yes'
                    set(this.gui_h.power_check, 'Value', 1);
                case 'No'
                    errordlg('Ensure Robot Controller Power is turned on', 'Fail');
                    Manager.destroy;
            end
            
            check6 = questdlg('Is the power switch for the Gripper Vacuum pad turned on?', ...
                'Checklist', ...
                'Yes','No');
            
            switch check6
                case 'Yes'
                    set(this.gui_h.vac_check, 'Value', 1);
                case 'No'
                    errordlg('Ensure Gripper Vacuum power is turned on', 'Fail');
                    Manager.destroy;
            end
            
            check7 = questdlg('Is the power switch for the Conveyor Controller turned on?', ...
                'Checklist', ...
                'Yes','No');
            
            switch check7
                case 'Yes'
                    set(this.gui_h.conv_check, 'Value', 1);
                case 'No'
                    errordlg('Ensure Conveyor Power is turned on', 'Fail');
                    Manager.destroy;
            end
            out = 0;
        end
        
    end
    
end