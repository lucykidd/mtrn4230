classdef (Abstract) ManagerInterface < Base
    % Manager : The manager class is used to run the main program and also
    % to monitor each component of the program (i.e performance, errors and
    % problems, basic overall program state ...).
    %
    % Usage : Create a Single Manager Object and then invoke the manager
    % run method .. or all methods and variables in manager are static and
    % thus can only exist once and also an actual manager object need not
    % be instantiated ??? (could be messy)...or just use a static counter
    %
    % Author : Lindsay
    % Date : 25/03/2015
    
    %% Properties..
    properties (Access = public)
    
    % Objects...
    gui;
    vision;
    siocomms;
    movecomms;
    robot;
    state;
    startupGUI;
    picker;
    
    % Variables...
    startLogMode;
    startCommsMode;
    
    
    end
    
    %% Methods..
    methods (Abstract)
    % Note : All Methods here are just definitions
    % Do not use a function...end block to define an abstract method, use only the method signature.
    
    run (this)
    % run : This will call and run the main function required to run
    % our whole program...
    % Author : Lindsay
    % Date : 24/04/15
    
    pause (this)
    % Deprecated!!!!
    
    stop (this)
    % stop : This will try and stop the main program....
    % Author : Lindsay
    % Date : 24/04/15
    
    systemStatus (this)
    % Deprecated.....
    
    destroy(this)
    % run : This will destroy our singleton object associated with this
    % method...
    % Author : Lindsay
    % Date : 24/04/15
    
    runProgram(this)
    % This starts all of the running executables for our project...
    % Author : Lindsay
    % Date : 24/04/15
    
    stopProgram(this)
    % This will stop our programs operation
    % Author : Lindsay
    % Date : 24/04/15
    
    end
end

