classdef (Sealed) Manager < ManagerInterface % Sealed makes this manager a singleton
    % Hence only one object instance of type manager can exist at a given
    % time...
    % Manager : The manager class is used to run the main program and also
    % to monitor each component of the program (i.e performance, errors and
    % problems, basic overall program state ...).
    %
    % Usage : Create a Single Manager Object and then invoke the manager
    % run method .. or all methods and variables in manager are static and
    % thus can only exist once and also an actual manager object need not
    % be instantiated ??? (could be messy)...or just use a static counter
    %
    % Author : Lindsay
    % Date : 25/03/2015
    
    %% Properties
    properties (Access = public)
        % Everyone can see and modify these properties
        % NB: there is a way so that people can read properties but not write
    end
    % NB: at the moment this is not handled as a singleton, but in the
    % future we should do that...
    
    %% Public Methods
    methods (Access = private)
        function this = Manager
            % This gives us a single manager instance and sets the values of
            % the variables correctly...
            % Author : Lindsay
            % Date : 24/04/15
            
            this.name = 'Manager';
            this.outState(0);
            % Instantiate all of the objects in our system...
            %this.instantiateObjects();
            
            % Initialise the objects...
            %this.initialise();
            this.state=0; % Initialised but not running anything...
            this.startLogMode=1; % normally to log file...
            this.startCommsMode=0;
        end
        
        function instantiateObjects(this)
            % Instantiate: calls the constructors on our main program objects..
            % Author : Lindsay
            % Date : 24/04/15
            
            this.gui=GUI();
            pause(1.25); % give GUI time to think..setup
            
            this.siocomms=SIOComms();
            this.movecomms=MoveComms();
            this.robot = RobController();
            this.picker = Picker();
            this.vision=Vision(this.startCommsMode);
            %    this.vision=Vision(1);% hijacking vision
        end;
        
        function initialise(this)
            % Initialise : lets pass each object the required object handles
            % and also lets set their output to file...
            % Author : Lindsay
            % Date : 24/04/15
            
            this.gui.addHandles(this.robot,this,this.picker); % gui has a handle back to the manager...
            this.robot.addHandles(this.gui,this.siocomms,this.movecomms,this.vision);
            
            this.siocomms.addHandles(this.robot);
            this.movecomms.addHandles(this.robot);
            this.picker.addHandles(this.gui,this.robot,this.vision);
            
            % Give required objects a handle back to this manager
            % be careful as we don't want these objects to have too much
            % control..
            this.gui.setManagerHandle(this); % gui has a handle back to the manager...
            this.robot.setManagerHandle(this);
            
            this.siocomms.setManagerHandle(this);
            this.movecomms.setManagerHandle(this);
            
            this.vision.addHandles(this.gui,this.robot);
            this.vision.setManagerHandle(this);
        end
        
        function setCommsMode(this,mode)
            % mode == 0 (default) for robot studio simulator
            % mode == 1 for real robot cell...
            % Author : Lindsay
            % Date : 24/04/15
            
            this.log('Change robot comms mode to :',mode);
            this.siocomms.setCommsMode(mode);
            this.movecomms.setCommsMode(mode);
            
            % set this in the gui...
            try
                this.gui.setCommsSource(mode);
            catch e
                e
            end
        end
        
    end
    
    %% Static Methods
    methods (Static)
        function out=myInstance (in)
            % This gives us only ever a single instance of manager through which everything else runs...
            % Stops us from running multiple instances of our main program...
            % Author : Lindsay
            % Date : 24/04/15
            
            persistent instance
            
            if in ==0
                % We are seeing if an instance exists and so forth ...
                if isempty(instance) || ~isvalid(instance)
                    instance = Manager;
                end
                % Hack -
                if isempty(instance.gui) || ~isvalid(instance.gui)
                    disp('Instance object invalid - make a new one');
                    instance = Manager;
                end
                out = instance;
            else
                % we want to remove this persistance instance
                %delete (instance)
                %clear instance;
            end
        end
        
        function inst = run(varargin)
            % run : This will call and run the main function required to run
            % our whole program...
            % Author : Lindsay
            % Date : 24/04/15
            
            disp('run : run the main program');
            
            inst=Manager.myInstance(0);
            
            if inst.state == 0 % i.e program is not running/startup...
                inst.startupGUI = StandardChecklist(inst);
                inst.log('Show the startup checklist GUI');
            end
            
        end
        
        function pause
            % run : This will call and run the main function required to run
            % our whole program...
            % Author : Lindsay
            % Date : 24/04/15
            
            disp('Pause the program');
        end
        
        function stop
            % stop : This will try and stop the main program....
            % Author : Lindsay
            % Date : 24/04/15
            
            disp('Stop : Stop the program but do not delete any modules');
            inst=Manager.myInstance(0);
            inst.stopProgram();
        end
        
        function systemStatus
            % run : This will call and run the main function required to run
            % our whole program...
            % Author : Lindsay
            % Date : 24/04/15
            % DEPRECATED!!!!
            disp('Display status of the system ... TODO');
        end
        
        function destroy
            % run : This will destroy our singleton object associated with this
            % method...
            % Author : Lindsay
            % Date : 24/04/15
            
            disp('Destroy : Stop the robot program and delete all modules');
            inst=Manager.myInstance(0);
            inst.stopProgram();
            inst.destroyProgram();
            %close all hidden;
        end
        
    end
    methods (Access = public)
        % Methods to implement Abstract Methods..
        
        function runProgram(this)
            % This starts all of the running executables for our project...
            % Author : Lindsay
            % Date : 24/04/15
            
            this.startupGUI=[]; % remove handle to dead object...
            
            % 1. Start all of the processes
            if this.state==0
                % Instantiate all of the objects in our system...
                this.instantiateObjects();
                
                % Initialise the objects...
                this.initialise();
                
                % change the gui state...
                this.gui.updateRobotMode(0); % set robot to be inactive...
                
                % Set our comms mode and startup mode to reflect the
                % startup configuration...
                this.logMode(this.startLogMode)
                this.setCommsMode(this.startCommsMode);
                
                this.siocomms.run(); % start the sio comms regular process..
                this.movecomms.run();
                
                this.vision.run(); % start the vision regular process..
                this.state=1; % everything is running...
                % Will need to do a bit more here...later
                this.log('Started the Main Program successfully');
                
            else
                this.error('Program is already running...cannot call run again');
            end
        end;
        
        function setStartupComms(this,mode)
            % Called by our startupGUI to select the desired comms mode
            % Author : Lindsay
            % Date : 24/04/15
            this.startCommsMode=mode;
        end;
        
        function setStartupLogs(this,mode)
            % Called by our startupGUI to select the desired log mode
            % Author : Lindsay
            % Date : 24/04/15
            this.startLogMode=mode;
        end;
        
        function logMode(this,mode)
            % < 0 for no logs
            % 0 for command window
            % > 0 or anytihng for log to file
            % Author : Lindsay
            % Date : 24/04/15
            
            this.gui.outState(mode);
            this.robot.outState(mode);
            this.vision.outState(mode);
            this.siocomms.outState(mode);
            this.movecomms.outState(mode);
            this.outState(mode);
            this.picker.outState(mode);
        end
        
        function stopProgram(this)
            % This will stop our programs operation
            % Author : Lindsay
            % Date : 24/04/15
            
            if this.state ==1
                %this.gui.stop(); % probably leave the gui running for now...
                this.siocomms.stop();
                this.movecomms.stop();
                this.vision.stop();
                this.state=0; % say we are stopped...
                this.log('Stopped the Main Program successfully');
            else
                this.error('Cannot Stop : Main program is not running');
            end;
        end
        
        function destroyProgram(this)
            % Deletes the objects pointed to by the handles stored by this manager
            % Author : Lindsay
            % Date : 24/04/15
            
            disp('Destroy this program....');
            delete(this.siocomms);
            delete(this.movecomms);
            delete(this.robot);
            delete(this.vision);
            delete(this.picker);
            clear this.gui;
            clear this.siocomms;
            clear this.movecomms;
            clear this.robot;
            clear this.vision;
            %             this.gui=[];
            %             this.siocomms=[];
            %             this.movecomms=[];
            %             this.robot=[];
            
            delete(timerfind); % Just in case timers somehow still exist
            %close all hidden;
            clear all;
        end
        
        %% Manager monitoring functions ... callbacks for respective classes
        
        %         function commsLog(this,message)
        %             % Function logs a message from a comms object (for sent and
        %             % recieved commands..i.e redirects the message to the gui..)
        %             this.log(strcat('Comms log message:',message));
        %             % TODO - call the console function in gui
        %         end;
        
        function componentError (this,componentName, error)
            % This logs an error in  a component (i.e all component error
            % messages will be redirected to this output...)
            % Author : Lindsay
            % Date : 24/04/15

            this.gui.commandWindow(error); %display on GUI terminal window
        end;
        
        function componentLog (this,componentName, log)
            % this logs a high importance log from a specific component to
            % the gui consol...
            % identity is the name of the component (i.e SIOComms or
            % robotcontroller etc...)
            % Author : Lindsay
            % Date : 24/04/15
            
            try
                if strcmp(componentName,'SIOComms') == 1 || strcmp(componentName,'MoveComms') == 1
                    % Call the console logging for the comms object...
                    this.log(strcat(componentName,' : log message:',log));
                    this.gui.commandWindow(log);%display on GUI terminal window
                else
                    this.log(strcat(componentName,' : component log :',log));
                    this.gui.commandWindow(log);%display on GUI terminal window
                end;
                
            catch e
                this.log(strcat('Unable to log message from component:',componentName));
                this.gui.commandWindow(log);%display on GUI terminal window
            end
        end;
        
        function restartComms (this)
            % Deprecated!!!!
            
        end;
        
        function updateComponentState (this,componentName,state)
            % calling this function will tell it to check the states of
            % each program...
            % 1 = good
            % 0 = not initialise/connecting...
            % -1 = bad...error...
            % Author : Lindsay
            % Date : 24/04/15
            
            if strcmp(componentName,'SIOComms') == 1
                this.log('Update the component state of the SIOComms Object to:',state);
                this.gui.setCommsState(state);
            elseif strcmp(componentName,'MoveComms') == 1
                this.log('Update the component state of the MoveComms Object to:',state);
                this.gui.setCommsState(state);
            else
                % TODO - expand me to handle problems with other components..
            end
        end
        
        function commsLost(this)
            % Called if comms are lost...should come from siocomms...
            % Author : Lindsay
            % Date : 24/04/15
            
            % 1. close connection on movecomms...this should put it into
            % restart mode...
            this.movecomms.closeConnection();
            
            % 2. display not connected...
            this.gui.setCommsState(-1);
        end
    end
    
end

