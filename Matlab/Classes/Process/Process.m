classdef Process < ProcessInterface
    % Process: any class is deemed to be a process class if it is required to do some 
    % sort of regular execution through the use of a matlab timer. Hence
    % the process class provides a simple common,consistent implementation
    % of regular tasks using matlab timers. This is so that any problems
    % with timers that may happen later down the track can be alleviated...
    %
    % Usage : Simply extend this class to have all regular process
    % methods...
    %
    % Author : Lindsay
    % Date : 24/03/2015
    % NB: This extends the handle superclass which therein enables 
    
    %% Properties
    properties (Access = public)
    end
    
    properties (Access = protected)
        % Only sublclasses can access these properties
    end
    
    %% Methods
    methods (Access = public)
        % Implement the Abstract methods from the interface here
        function this = Process(this)
           % Short constructor for taskState
           this.taskState=0;
        end
        
        function delete(this)
           this.stop();
           if this.taskTimerHandle ~= -1 
            delete(this.taskTimerHandle);
           end;
           this.taskTimerHandle=[];
        end
        
        function state = mainTaskState(this)
        % mainTaskState : return 0 for no task, 1 for task created but not
        % running, 2 for running fine and -1 for error with task...
            state=this.taskState;
        end
        
        %% run Function
        function out = run(this)
            % run - starts the periodic task that is required by this class
            % Will report errors if 
            if this.taskState == 0
                this.log(strcat('No valid task has been defined'));
                this.error(strcat('No valid task specified for',this.name));
            elseif this.taskState == 1
                this.log(strcat('Try to start the ',this.name, ' main task'));
                %this.taskState = 2; % update the state of the task
                start(this.taskTimerHandle);
            elseif this.taskState == 2
                this.log(strcat('The ',this.name,' main task is already running'));
            else
                this.log(strcat('There is an error with ',this.name,' main task'));
                this.error(strcat('There is an error with the',this.name,' main task'));
            end
            out=this.taskState;
        end
        
        %% Stop Function
        function stop(this)
            % stop - stops the periodic task that is required by this class
            % NB: To be overriden in each individual class...
            if this.taskState == 0
                this.log(strcat('No valid task has been defined'));
                this.error(strcat('No valid task specified for',this.name));
            elseif this.taskState == 1
                this.error(strcat('The main task for ',this.name, ' is not running , cannot stop'));
            elseif this.taskState == 2
                this.log(strcat('Try to stop the ',this.name,' main task'));
                %this.taskState=1; % task is defined but inactive..
                stop(this.taskTimerHandle);
            else
                this.log(strcat('There is an error with ',this.name,' main task'));
                this.error(strcat('There is an error with the',this.name,' main task'));
            end
        end
        
        function setRegularTask (this,priority,startDelay,period)
        % setRegularTask: This initialises a timer object to
        % periodically call the mainTask at the defined period (in
        % seconds) with the priority (1=high, 0 =low), the timeOffset (s -
        % time after calling run that first instance should be called) 
        % Usage : this.stop()
        % Author : Lindsay , Date : 24/03/2015
        
            if this.taskState ~= 0
               this.error('Task has already been set!!!');
               return;
            end
            
            this.taskTimerHandle = timer('StartDelay', startDelay, 'Period', period,'ExecutionMode', 'fixedRate');
            this.taskTimerHandle.TimerFcn = @this.mainTask; % Link to our mainTask function for error function
            this.taskTimerHandle.startFcn = @this.startCallback; % Define a function to call when task started
            this.taskTimerHandle.stopFcn = @this.stopCallback; % Define a function to call when task stopped
            this.taskTimerHandle.errorFcn = @this.errorCallback; % Define a function to call when error with task...
            this.taskState=1; %task is defined..but it is not running yet...
        end
        
        function out = mainTask (this,timerObj,event,string_arg)
           % Do nothing ... this is a dummy implementation of the mainTask function
           % The idea is that it is actually implemented in your concrete
           % class later......
           this.log('Main Task function has not been defined...check Process.m line 71');
           this.error('Main Task function has not been defined...check Process.m line 71');
           out=-1;
        end
        
    end
    methods (Access = protected)
        function out = startCallback (this,timerobj,event,string_arg)
            % stopCallback: this function is executed when the timer is
            % stopped
            % If overrided, make sure you set the taskState still as per
            % below
            this.log(strcat('Successfully Started the main task for',this.name));
            this.taskState=2; % update state to stated
        end
        
        function out = stopCallback (this,timerobj,event,string_arg)
            % stopCallback: this function is executed when the timer is
            % stopped
            % If overrided, make sure you set the taskState still as per
            % below
            this.log(strcat('Successfully Stopped the main task for',this.name));
            this.taskState=1;
        end
        
        function out = errorCallback (this,timerobj,event,stirng_arg)
            % errorCallback: this function is executed when the timer
            % encounters an error
            % For now, do not override this function ..
            this.error(strcat('Main task for ',this.name,' has encountered an error'));
            
            % TODO - some error mitigation procedure might be nice here....
            this.taskState=-1;
        end
    end

    methods (Access = private)
       
%         function taskShell(this,functionHandle)
%             % taskShell: This is just a wrapper around the defined functionHandle
%             % which allows stats about this function to be recorded incase
%             % we need them...
%             tstart = 
%             
%         end;
% Do this later if we need it...
    end
end

