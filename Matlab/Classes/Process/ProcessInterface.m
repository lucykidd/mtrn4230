classdef (Abstract) ProcessInterface < Base
    % ProcessInterface : any class is deemed to be a process class if it is required to do some 
    % sort of regular execution through the use of a matlab timer. Hence
    % the process class provides a simple common,consistent implementation
    % of regular tasks using matlab timers. This is so that any problems
    % with timers that may happen later down the track can be alleviated...
    %
    % Usage : Simply extend this class to have all regular process
    % methods...
    %
    % Author : Lindsay
    % Date : 24/03/2015
    % NB: This extends the handle superclass which therein enables 
    
    properties (Access = protected)
        taskTimerHandle; % handle to the timer object that is calling the function...
        taskState; % state of regular task , 0 = no task , 1 = task is created but not running, 2 = running fine , -1 = error in task
%         taskAvExecutionTime; % Average execution time of task...
%         taskMaxExecutionTime; % Maximum recorded execution time of task...
    end
    
    methods (Abstract)
    % Note : All Methods here are just definitions
    % Do not use a function...end block to define an abstract method, use only the method signature.
        
        mainTaskState(this)
        % mainTaskState : return 0 for no task, 1 for task created but not
        % running, 2 for running fine and -1 for error with task...
    
        run (this)
        % run : this function starts the periodic task that is used by this
        % To be overriden in the concrete final class...
        % class (i.e for siocomms it starts the tcp server)
        % a message and a code ..
        % Usage : this.run()
        % For now the code should just start some kind of periodic
        % message...
        % Author : Lindsay , Date : 24/03/2015
        
        stop (this)
        % stop : this function stops the periodic task used by this class (i.e for siocomms it stops the tcp server)
        % a message and a code ..
        % Usage : this.stop()
        % Author : Lindsay , Date : 24/03/2015
        
        % NB : right now it is assumed that a given object would only need
        % one regular task...let me know if this changes for you
        
        setRegularTask (this,priority,startDelay,period)
        % setRegularTask: This initialises a timer object to
        % periodically call a taskFunction handle at the defined period (in
        % seconds) with the priority (1=high, 0 =low), the timeOffset (s -
        % time after calling run that first instance should be called) 
        % 
        % Usage : e.g this.setRegularTask(0,1,0.2);
        % Author : Lindsay , Date : 24/03/2015
        
        mainTask(this,timerObj,event,string_arg)
        % mainTask : This is the function that is called periodically by
        % the timer to execute some code (i.e for comms it would check to
        % see if we have new data to read...). 
        %
        % Usage: implement this function in your concrete class...
        % This function is essentially a copy of that presented by matlab 
        % in http://au.mathworks.com/help/matlab/matlab_prog/timer-callback-functions.html
        
    end
    
    % !!!! Please try not add any concrete methods here... do that in the
    % Base concrete class
end

