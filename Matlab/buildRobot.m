function rob = buildRobot()




    DH = [0,0.290,0,pi/2;
          0, 0,0.270,0;
          0, 0, 0.070, -pi/2;
          0, 0.302,0,pi/2;
          0, 0,0,pi/2;
          0, 0.137, 0,0];

    offset = [pi,pi/2,0,0,pi,0];

    M = [3.067,3.909,2.944,1.328,0.547,0.013];
    B = [5*10^(-4),2*10^(-4),10^(-4),5*10^(-5),2.5*10^(-5),10^(-5)];
    r = [0, 0.250, 0;
        0.100, 0, 0;
        0.070, 0, 0.050;
        0, 0.250, 0;
        0, 0, 0.030;
        0, 0, 0];

    I = [0.014, 0.014, 0.014;
        0.025, 0.04, 0.06;
        0.01, 0.01, 0.008;
        0.004, 0.003, 0.005;
        0.0008, 0.0004, 0.0009;
        10^(-6), 10^(-6), 2*10^(-6)];
    J = [0.0005, 0.0002, 0.0001, 0.0001, 0.00005, 10^(-5)];


    gear_ratio = 80;
k = pi/180;
qlim = k*[-165, 165;
            -110, 110;
            -110, 70;
            -160, 160;
            -120, 120;
            -400, 400];
    % building the links
    for i=1:length(DH(:,1))
        inertia = diag(I(i,:));
        L(i) = Link(DH(i,:));
        L(i).offset = offset(i);
        L(i).m = M(i);
        L(i).r = r(i,:);
        L(i).I = inertia;
        L(i).Jm = J(i);
        L(i).B = B(i);
        L(i).nofriction();
        L(i).G = gear_ratio;
        L(i).qlim = qlim(i,:);
    end

    % building robot
    irb_120 = SerialLink(L,'name','IRB120');



    % Define 
    irb_120.nofriction();
    irb_120.gravity = [0 0 9.8]';
    
    rob = irb_120;
end