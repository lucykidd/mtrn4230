MODULE Comms_IO
    
    ! Current State Globals
    PERS pos XYZGoal; ! coordinates for the goal
    PERS jointtarget JGoal;
    PERS string IOstate{4}; ! This contains the value of ConRun, ConDir, VacSol, VacPump in that order. It's an array of strings.
    
    PERS bool ErrFlag;
    PERS bool OutOfRange; ! to feedback if the target can't be reached.
    PERS bool SIOCommsError:=FALSE; ! a flag to indicate if comms are in error state or not...set by movecomms when heartbeat is lost or by siocomms on connection loss..
    
    VAR bool Estop:=FALSE;
    ! Main Task...
    PROC Main() 
                      
       ! 1. Try to initialise communications...if first time...
       IF commsState = FALSE THEN
           initLogs("comms_io.log"); !NB: This might do funny shit on the flexpendant/real robot....but good for debugging at home...
           log("Try to initialise the comms_io_socket");
           initComms;
           log("Socket initialised successfully!!!");
       ENDIF
       
        WHILE TRUE DO ! maybe while true is needed
            !Determine the current robot State and send to the server..
            sendCurrentState;
                
           ! if we get here, the comms have been lost and must be recovered...
           IF SIOCommsError=TRUE THEN
               log("Try to recover our comms...main function");
               recoverComms;
               SIOCommsError:=FALSE; ! reset our flag as the comms issue is now resolved...
           ElSE
               log("No error with comms..");
           ENDIF
           
           WaitTime 1.1; ! Lets wait for 1.1 seconds before sending our next message
       ENDWHILE
       
       ERROR
        log("Error: unknown error thrown in Main task...retry");
        RETRY;
        
    ENDPROC
        
    PROC sendCurrentState()
        ! This function attempts to read all relevant robot data into the outputData rawbytes struct
        ! It then will send the data to the server...
        VAR rawbytes raw_data;
        VAR jointtarget curJoints;
        VAR robtarget curRob;
        VAR byte header1;
        VAR byte header2;
        VAR byte header3;
        VAR byte header4;
        VAR num trailer := -69843.276;
        VAR num ConStat;
        VAR num OutReach;
        VAR num EstopNum;
        
        curJoints := CJointT();
        curRob := CRobT();
        
        ! TODO - might need to redefine the coordinate system of the robot...
        
        ! -32567896.11 is floating value of our header...
        header1 := StrToByte("CB" \Hex);
        header2 := StrToByte("F8" \Hex);
        header3 := StrToByte("79" \Hex);
        header4 := StrToByte("2C" \Hex);
        
        ! Header
        PackRawBytes header1, raw_data,(RawBytesLen(raw_data)+1) \Hex1;
        PackRawBytes header2, raw_data,(RawBytesLen(raw_data)+1) \Hex1;
        PackRawBytes header3, raw_data,(RawBytesLen(raw_data)+1) \Hex1;
        PackRawBytes header4, raw_data,(RawBytesLen(raw_data)+1) \Hex1;
        
        ! Lets pack current tool position, current tool orientation 
        PackRawBytes curRob.trans.x, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curRob.trans.y, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curRob.trans.z, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        
        PackRawBytes curRob.rot.q1, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curRob.rot.q2, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curRob.rot.q3, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curRob.rot.q4, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        
        ! Lets pack the current joint orientations
        PackRawBytes curJoints.robax.rax_1, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curJoints.robax.rax_2, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curJoints.robax.rax_3, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curJoints.robax.rax_4, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curJoints.robax.rax_5, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes curJoints.robax.rax_6, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        
        ! Lets pack the current 4 IO States...
        ! Dummy shit for now...
        PackRawBytes 1.0, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes 1.0, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes 1.0, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes 1.0, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        
        ! Lets pack the desired XYZ Position
        PackRawBytes XYZGoal.x, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes XYZGoal.y, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes XYZGoal.z, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        
        ! Lets pack the desired Joint positions
        PackRawBytes JGoal.robax.rax_1, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes JGoal.robax.rax_2, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes JGoal.robax.rax_3, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes JGoal.robax.rax_4, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes JGoal.robax.rax_5, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        PackRawBytes JGoal.robax.rax_6, raw_data, (RawBytesLen(raw_data)+1) \Float4;
             
        IF OutOfRange = TRUE THEN
            OutReach := 1;
        ELSE
            OutReach := 0;
        ENDIF
        PackRawBytes OutReach, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        
        ConStat := DI10_1;
        PackRawBytes ConStat, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        
        IF EStop = TRUE THEN
            EStopNum := 1;
        ELSE
            EStopNum := 0;
        ENDIF
        PackRawBytes EStopNum, raw_data, (RawBytesLen(raw_data)+1) \Float4;
        ! Total message length : 104 bytes ... should be okay...        

        
        ! Lets send the message through to the server...
        ! NB: might want to handle errors with this later...
        log("Send current state to server : ");
        SocketSend client_socket \RawData:=raw_data;
        
        ERROR
            IF ERRNO=ERR_SOCK_TIMEOUT THEN
                ! Lets just recall out send and recieve function...should not happen...
                log("Error: Socket timeout...should not occur but lets just try again...");
                ! TODO .. should never happend , but 5 consecutive timeouts might signal that it is time to close the connection and erinit
                RETURN;
            ELSEIF ERRNO=ERR_SOCK_CLOSED THEN
                log("Error: Connection with base computer lost....");
                SIOCommsError:=TRUE; ! set the error comms flag...this will cause the main loop to try and recover...
                RETURN;
            ELSE
                log("Error: Unknown exception thrown in sendCurrent State...retry..");
                RETURN;
            ENDIF
    ENDPROC
    
    PROC SendEStop()
        Estop := TRUE;
        sendCurrentState;
    ENDPROC
ENDMODULE