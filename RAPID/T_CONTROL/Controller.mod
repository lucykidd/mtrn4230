MODULE Controller
    
    ! Controller for MTRN4230
    ! Author: Patrick Bobbink, z5030085
    ! Last modified: 2015-04-20
    
    
    ! This module is the actual controller.
    ! The position is updated almost immediatly after receiving a new target.
    ! IO are updated every 0.1s using an interrupt.
    ! Stops movement in case of communication failure
    ! Stops conveyor in case of open guard.
    
    
   ! All PERS are shared with at least one other task.
    PERS bool IOUpdateReady;  ! IOUpdateReady is to be set by the Communication, if it's set, this task actuates the robots in/outputs. Once updated, the Controller sets to FALSE
    PERS bool PosUpdateReady; ! MoveUpdate is to be set by the Communication, if set, this task actuates the robot to get to the position in XYZGoal or JGoal. Once updated, Controller sets to FALSE
    PERS bool Ready; ! Ready is to be set by the Controller, if set, the robot is ready to accept a new task. Once updated, Communication sets to TRUE.
   
    PERS bool XYZMode; ! If set to 1, robot moves to XYZ, else it moves to Jgoal.
    PERS bool LinearMode; ! If set to 1, robot moves in linear fashion, else it moves in joint mode
    PERS pos XYZGoal; ! coordinates for the goal
    PERS jointtarget JGoal; ! This is a structure containing robjoint and external axis, please use only robjoint.
    PERS bool StopCommand;
    PERS bool VertTool; ! To be set when the tool has to be vertical, else it will keep current orientation when moving in XYZ mode
    
    PERS string IOstate{4} := ["0", "0", "0", "0"]; ! This contains the value of ConRun, ConDir, VacSol, VacPump in that order. It's an array of strings.
    PERS speeddata speed := [200,100,200,100]; ! Contains speed to move the joints
    
    PERS bool OutOfRange; ! if the robot is out of its reach
    
    VAR bool Started := FALSE; ! Tells if Robot has successfully started or not
     
    VAR intnum IOUpdateInt; ! Interrupt number for IO update
    VAR intnum ConStatInt; ! Int number for Conveyor status
    VAR intnum CommsFailureInt; ! int number for comms failure
    
    PERS bool ErrFlag; ! error flag to be set when an error occurs in other parts of the code
    PERS num Quaternions{4};
    
    ! Main procedure, task loops through this.
    PROC Main() 
        IF Started = FALSE THEN ! Startup function, setting robot in known position, setting variables etc. To be called only once
            StartUp;
        ENDIF
            
        ! If the robot hasn't been told to stop and has a position ready, move it.
        IF StopCommand = FALSE AND PosUpdateReady = TRUE THEN
            UpdatePos;
        ENDIF
        
    ENDPROC
   
   ! Startup procedure setting robot in a known state.
    PROC StartUp()
        SingArea \Wrist; ! Trying to avoid singularities
        
        ! Initialising all update states in a way the robot won't start moving after going to calib pos
        IOUpdateReady := FALSE;
        PosUpdateReady := FALSE;
        LinearMode := FALSE;
        XYZMode := FALSE;
        OutOfRange := FALSE;
        XYZGoal := [300,00,100];
        JGoal := [[0,0,0,0,0,0],[0,9E9,9E9,9E9,9E9,9E9]];
        
        ! Setting up timer interrupt for IO
        CONNECT IOUpdateInt WITH IOTrap;
        ITimer 0.1, IOUpdateInt;

        ! Setting conveyor interrupt
        CONNECT ConStatInt WITH ConStatTrap;
        ISignalDI DI10_1, 0, ConStatInt;
        
        ! setting comms interrupt 
        CONNECT CommsFailureInt WITH CommsFailureTrap;
        IPers ErrFlag, CommsFailureInt;
        
        ! Robot has started
        Started := TRUE;
        
        ! Robot is fully ready
        Ready := TRUE;
    ENDPROC

     
    ! Trap handling the IO Update. If IOUpdateReady is set to TRUE we Update the IOs
    ! IOs are set to state given in IOState.
    TRAP IOTrap
        ISleep IOUpdateInt;
        IF IOUpdateReady = TRUE THEN
            UpdateIO;
        ENDIF
        IWatch IOUpdateInt;
    ENDTRAP
    
    ! Trap for conveyor, turns it off if open so that it doesn't start running on closure.
    TRAP ConStatTrap
        ISleep ConStatInt;
        IF DI10_1 = 0 THEN
            TurnConOff;
        ENDIF
        IWatch ConStatInt;
    ENDTRAP
    
    ! stops movement on communication failures    
    TRAP CommsFailureTrap
        ISleep CommsFailureInt;
        IF ErrFlag = TRUE THEN
            StopMove;
        ENDIF
        IWatch CommsFailureInt;
    ENDTRAP
ENDMODULE