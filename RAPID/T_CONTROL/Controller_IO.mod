MODULE Controller_IO
        
    ! Controller_IO Module for MTRN4230, this gives procedures to control all IO on the ABB robot available for the course
    ! Author: Patrick Bobbink, z5030085
    ! Last modified: 2015-04-13
    
    ! variables to give easy names for the IOs
    VAR num VacOn;
    VAR num VacSolOn;
    VAR num ConDir;
    VAR num Conval;
    VAR bool ok;
        
    PROC UpdateIO() 
   ! IOstate contains the value of ConRun, ConDir, VacSol, VacPump in that 
        ok := StrToVal(IOstate{1}, Conval);
        ok := StrToVal(IOstate{2}, ConDir);
        ok := StrToVal(IOstate{3}, VacSolOn);
        ok := StrToVal(IOstate{4}, VacOn);
        
        ! First setting the conveyor in right direction and starting it
        IF ConDir > 0 THEN
            SetConDir;
        ELSE
            CLearConDir;
        ENDIF
        IF Conval > 0 THEN
            TurnConOnSafely;
        ELSE
            TurnConOff;
        ENDIF
        
        ! Taking care of Vacuum pump
        IF VacOn > 0 THEN
            TurnVacOn;
        ELSE
            TurnVacOff;
        ENDIF
        
        ! Taking care of vacuum solenoid
        IF VacSolOn > 0 THEN
            TurnVacSolOn;
        ELSE
            TurnVacSolOff;
        ENDIF
        
        IOUpdateReady := FALSE; ! We're up to date
    ENDPROC
    
    
    !! given in example
    ! Set VacRun on.
    PROC TurnVacOn()
        SetDO DO10_1, 1;
    ENDPROC
    
    
    ! Set VacRun off.
    PROC TurnVacOff()   
        SetDO DO10_1, 0;
    ENDPROC
    
    
    ! Set VacSol on
    PROC TurnVacSolOn()
        SetDO DO10_2, 1;
    ENDPROC
   
    
    ! Set VacSol off
    PROC TurnVacSolOff()  
        SetDO DO10_2, 0;
    ENDPROC
    
    
    ! Turn conveyor ON with checking for conGuard
    PROC TurnConOnSafely()
        IF DI10_1 = 1 THEN
            SetDO DO10_3, 1;
        ELSE
            SetDO DO10_3, 0;
        ENDIF
    ENDPROC
    
    
    ! Turn conveyor OFF
    PROC TurnConOff()
        SetDO DO10_3, 0;
    ENDPROC
    
    
    ! Set conveyor direction towards robot
    PROC SetConDir()
        SetDO DO10_4, 1;
    ENDPROC
    
    
    ! Set conveyor direction away from robot
    PROC CLearConDir()
        SetDO DO10_4, 0;
    ENDPROC
    
    
ENDMODULE