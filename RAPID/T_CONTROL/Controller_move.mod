MODULE Controller_move
    
    ! Controller_IO Module for MTRN4230, this gives a procedure to control the position of the robot in various ways. 
    ! Author: Patrick Bobbink, z5030085
    ! Last modified: 2015-04-30
    ! PERS used: speed, XYZGoal, JGoal, XYZMode, LinearMode, VertTool, posUpdateReady, outOfRange
    ! PERS modified: posUpdateReady, outOfRange
    
    ! inputs:
    ! XYZMode: if set, the robot will go to the current XYZ target.
    ! LinearMode: if set, robot will go linearly to its target. Else it uses moveAbsJ, which allows for better singularities handling.
    ! Jgoal: Joints positions to be used when XYZ mode is cleared.
    ! XYZGoal: XYZ position of end effector to be used when XYZ mode is set
    ! speed: defines the speed to be used when moving the robot
    ! posUpdateReady: if set, the robot has to update its position. This procedure clears it.
    
    ! outputs:
    ! posUpdateReady: is cleared by the procedure
    ! outOfRange: is set when target is out of reach (not always working?)
    
    ! Contains the error handler to stpo movement.
    PROC UpdatePos()
        VAR robtarget robTarg;
        VAR jointtarget jointTarg;
        OutOfRange := FALSE;
        ! If the robot is set to linear mode, we want to use MoveL, depending on XYZMode or not, we go to XYZGoal or JGoal.
        IF PosUpdateReady = TRUE THEN
            IF LinearMode = TRUE THEN
                IF XYZMode = TRUE THEN
                    IF VertTool = TRUE THEN ! If we want to keep the tool vertical
                        robTarg := Offs(pTableHome, XYZGoal.x, XYZGoal.y, XYZGoal.z);
                        robTarg := Offs(robTarg, -pTableHome.trans.x, -pTableHome.trans.y, -pTableHome.trans.z); ! Using pTableHome as reference for all but XYZ coords
                    ELSE ! if not, keep orientation defined in quaternions
                        robTarg := CRobT();         
                        robTarg.trans.x :=  XYZGoal.x; ! was originally with ptablehome.trans
                        robTarg.trans.y :=  XYZGoal.y; ! same
                        robTarg.trans.z :=  XYZGoal.z; ! same
                        robTarg.rot.q1 := Quaternions{1};
                        robTarg.rot.q2 := Quaternions{2};
                        robTarg.rot.q3 := Quaternions{3};
                        robTarg.rot.q4 := Quaternions{4};
                        
                    ENDIF
                    
                    MoveL robTarg, v100, fine, tSCup; 
                ELSE
                    robTarg := CalcRobT(JGoal, tSCup);
                    MoveL robTarg,speed,fine, tSCup;                    
                ENDIF
            ELSE ! We are not going to move linearly,
                IF XYZMode = TRUE THEN 
                    IF VertTool = TRUE THEN ! If we want to keep the tool vertical
                        robTarg := Offs(pTableHome, XYZGoal.x, XYZGoal.y, XYZGoal.z);
                        robTarg := Offs(robTarg, -pTableHome.trans.x, -pTableHome.trans.y, -pTableHome.trans.z); ! Using pTableHome as reference for all but XYZ coords
                    ELSE ! if not, keep the current orientation, this can cause issues.
                        robTarg := CRobT();         
                        robTarg.trans.x :=  XYZGoal.x; !
                        robTarg.trans.y :=  XYZGoal.y; !
                        robTarg.trans.z :=  XYZGoal.z; ! Was originally with ptableHome.trans
                        robTarg.rot.q1 := Quaternions{1};
                        robTarg.rot.q2 := Quaternions{2};
                        robTarg.rot.q3 := Quaternions{3};
                        robTarg.rot.q4 := Quaternions{4};
                    ENDIF
                    jointTarg := CalcJointT(robTarg,tSCup); ! building a joint target for MoveAbsJ
                    MoveAbsJ jointTarg, speed,fine,tSCup;
        !            MoveJ robTarg,speed,fine,tSCup;
                ELSE ! If we're not in XYZMode, just send the robot to the target.
                    MoveAbsJ JGoal, speed, fine, tSCup;
                ENDIF
            ENDIF         
        ENDIF
        
        ERROR ! Error handler, handles out of bonds and procErrRecovery 
            IF ERRNO=ERR_ROBLIMIT THEN ! If we're going out of bonds, stop the robot
                OutOfRange := TRUE;
                StopMove;
                ClearPath;
                StartMove;      
                PosUpdateReady := FALSE;
            ENDIF
                   
            IF ERRNO = ERR_PATH_STOP THEN ! If a procErrRecovery has been triggered, stop the robot.
                StopMove;
                ClearPath;
                StartMove;
            ENDIF
        EXITCycle;
    ENDPROC
     
ENDMODULE