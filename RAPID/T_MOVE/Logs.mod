MODULE Logs
    ! Logs: This is a generic module used to create a logfile outdirectory for any Program task
    ! Usage: simply include in your modules for a task, call initLogs(logfilename) here and then type log(message) to log a message
    ! Logs will appear in your robotstudio home directory... /SYSTEMS/MTRN4230/HOME/
    
    VAR string logName;
    VAR bool enableLogs:=FALSE;
    VAR iodev logFHandle;
    VAR num maxLogLength := 60;
    
    PROC initLogs(string logfile)
    ! initLogs: Call to start a log file for this task....    
        ! TODO - handle string errors...
        
        ! Initialises logs for this program...
        logName:=logfile;
        Open logfile , logFHandle\Write ;
        
        ! TODO - handle opening errors...
        enableLogs:=TRUE;
    ENDPROC
    
    PROC disableLogs()
        enableLogs := FALSE;
    ENDPROC
    
    PROC log(string message)
    ! Writes a message to our defined logHandle
        IF enableLogs THEN
            ! Write has an error when message is too long...
            IF StrLen (message) <= maxLogLength AND StrLen(message) > 0 THEN
                Write logFHandle, CTime() + " - " + message; ! writes a new message to the log...
            ELSE
                Write logFHandle, CTime() + " - " + "message unable to be logged";
            ENDIF
        ENDIF
    ENDPROC
    
    PROC closeLogs()
        ! TODO - call close on log file handle to release it...
    ENDPROC
    
ENDMODULE