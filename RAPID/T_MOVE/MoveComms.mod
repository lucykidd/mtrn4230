MODULE MoveComms
    
    ! Current State Globals - shared with the controller...
    PERS pos XYZGoal; ! coordinates for the goal
    PERS jointtarget JGoal; ! coordinates for the joint goal..
    PERS bool PosUpdateReady; ! MoveUpdate is to be set by the Communication, if set, this task actuates the robot to get to the position in XYZGoal or JGoal. Once updated, Controller sets to FALSE
    PERS bool Ready; ! Ready is to be set by the Controller, if set, the robot is ready to accept a new task. Once updated, Communication sets to TRUE.
    PERS bool IOUpdateReady;  ! IOUpdateReady is to be set by the Communication, if it's set, this task actuates the robots in/outputs. Once updated, the Controller sets to FALSE
    PERS string IOstate{4};
    
    PERS bool LinearMode;
    PERS bool XYZMode;
    PERS speeddata speed;
    PERS bool StopCommand;
    PERS bool VertTool;
    
    PERS bool ErrFlag;
    
    ! Shared Module Variables
    VAR num floatData {100}; ! this should be overkill... but good for storing our raw message
    VAR num dataLength;
    
    ! Comms Management variables
    VAR num timeCommsLost; ! Time that the comms has been lost for
    CONST num timeThresh := 8; ! Wait for 8 seconds without a message before closing any connection...
    CONST num maxWait := 1.6; ! wait 1.6s before stopping the robot...
    VAR bool commsLost:=FALSE;
    PERS bool SIOCommsError:=TRUE; ! this will signal to siocomms that comms has been lost...
    VAR bool stopMotion:=FALSE;
    
    PERS num Quaternions{4};
    
    ! Main Task...
    PROC Main() 
                      
       ! 1. Try to initialise communications...if first time...
       IF commsState = FALSE THEN
           initLogs("movecomms.log"); !NB: This might do funny shit on the flexpendant/real robot....but good for debugging at home...
           log("Try to initialise the movecommssocket");
           initComms;
           log("Socket initialised successfully!!!");
       ENDIF
       
       ! 2. Start the main send and recieve processing loop
       WHILE TRUE DO
            recieveAndProcess;
            IF commsLost=TRUE THEN
                log("Main : begin recovery of the comms...");
                SIOCommsError := TRUE; ! Tell SIOComms to also be true...
                recoverComms; ! this wil block until comms are reestablished...
                commsLost:=FALSE;
                stopMotion:=FALSE;
            ELSEIF stopMotion=TRUE THEN
                ! robot should temporarily stop motion
                StopCommand := TRUE;     ! Telling controller we've stopped
                IF ErrFlag = FALSE THEN
                    ProcerrRecovery \SyncOrgMoveInst; ! sending an error message to the routine responsible for motion
                ENDIF
            ELSE
                log("No problem with comms..");
            ENDIF
            
       ENDWHILE
       
        ERROR
            log("Error: problem with main task...retry");
            RETRY;

    ENDPROC
    
    PROC recieveAndProcess()
       VAR rawbytes rawData; ! variable to store the raw data... 
       VAR string message; ! var to store the output message..
       VAR num startPos :=-1; ! position
       VAR byte singleByte;
       
       VAR byte header1:=126;
       VAR byte header2:=228;
       VAR byte header3:=64;
       VAR byte header4:=70;
       VAR num j;
       VAR num i;
       VAR bool headerNotFound:=TRUE;
           
       !1. Read any Messages from matlab
       SocketReceive client_socket \RawData:=rawData \Time:=maxWait;!\Time:=WAIT_MAX;! Wait for 1.6 seconds
       log ("Message(s) from matlab recieved...process them ...");
       
       ! Let's quickly update our comms state since we have recieved a message...
       IF timeCommsLost > 0 THEN
            ! we have been previously stopped and waiting for comms..lets resume motion...and make comms found..
            timeCommsLost:=0;
            log("Comms regained without socket loss...restart any motion");
            StopCommand := FALSE;
            StopMotion:=FALSE;
       ENDIF
       
       !2. Lets try to find our message header...hopefully it is at the start of the message...
       ! If it is not at the start then we have some issues to solve...
       i:=1;
       WHILE headerNotFound AND i <= RawBytesLen(rawData) DO
           
           UnpackRawBytes rawData,i, singleByte \Hex1;
           !log("Header byte:" + ByteToStr(singleByte));
           IF singleByte = header1 THEN
               !log("Found the first header byte...");
                UnpackRawBytes rawData,i+1, singleByte \Hex1;   
                IF singleByte = header2 THEN
                    UnpackRawBytes rawData,i+2, singleByte \Hex1;  
                    IF singleByte = header3 THEN
                        UnpackRawBytes rawData,i+3, singleByte \Hex1; 
                        IF singleByte = header4 THEN
                            !log("Successfully found the entire correct header!");
                            startPos:=i+4; ! This is where the message ID will be found...
                            headerNotFound:=FALSE;
                        ENDIF
                    ENDIF
                ENDIF
           ENDIF
           
           i:=i+1;
       ENDWHILE
       
       ! TODO - maybe some form of incorrect message error handling here
       ! Also, not sure if we need some sort of ACK happening here... will see if TCP is robust enough by itself...
       
       ! 3. Convert the rest of message to an array of floats...
       IF startPos > 0 THEN
            dataLength := (RawBytesLen(rawData) - (startPos-1))/4; ! Length of the data that we have recieved...maybe make this non variable...
            log("data length is : " + NumToStr(dataLength,1));
            j:=startPos;
            i:=1;
            WHILE j <= RawBytesLen(rawData) AND i <= 100 DO
                UnpackRawBytes rawData,j,floatData{i} \Float4; 
                log("Next Float is" + NumToStr(floatData{i},4));
                j:=j+4;
                i:=i+1;
            ENDWHILE 
            
            ! 4. Parse the float array --> call our message processor...  
            ! This will then change our robot controller state variables as needed...     
            parseCommand;
       
            startPos:=-1; ! reset the startPos...
            headerNotFound:=TRUE; ! header is not found again
            ClearRawBytes rawData; ! clear our current raw bytes...
       ELSE
           log("Error : message does not contain correct header --> fix me");
       ENDIF
              
       ERROR
            IF ERRNO=ERR_SOCK_TIMEOUT THEN
                log("Error: we have not recieved any messages within the 1.6 second time");
                timeCommsLost:=timeCommsLost+maxWait; ! how long have we been waiting....
                IF timeCommsLost > timeThresh THEN
                    timeCommsLost:=0;
                    log("Error: waited 8s with no message, this socket must be closed...");
                    commsLost:=TRUE; ! Comms have been lost as a result of heartbeat failure
                ELSE
                    ! Comms cannot be assumed lost ... but we need to stop the motion
                    log("Error: Comms is temporarily lost... stop the motion ");
                    stopMotion:=TRUE;
                ENDIF
                
                RETURN;
            ELSEIF ERRNO=ERR_SOCK_CLOSED THEN
                log("Error: Socket has been closed and connection lost!");
                commsLost:=TRUE; ! set our comms lost flag...
                RETURN;
            ELSE
                log("Error: Uknown socket error...retry....");
                RETURN;
            ENDIF
    ENDPROC
    
ENDMODULE