MODULE Message_Processor
    PROC parseCommand()
        VAR num cartLength :=5;
        VAR num jointLength :=7;
        VAR num ioLength :=5;
        VAR num LinearModeLength :=2;
        VAR num XYZModeLength :=2;
        VAR num speedLength :=5;
        
        VAR num temp; ! temp variable.
        VAR num i;
        ! Will attempt to parse the array of floats and from that control the robot...
        
        ! Fix...lets loop ...processing each command...as many commands can come at once...
        i:=1;
        WHILE i <= dataLength DO
            log("Process the next command...");
        ! 1. Determine the command type and take appropriate action...
        IF floatData{i} = 3.0 THEN ! Stop Command
            StopCommand := TRUE;     ! Telling controller we've stopped
            IF ErrFlag = FALSE THEN
                ProcerrRecovery \SyncOrgMoveInst; ! sending an error message to the routine responsible for motion
            ENDIF
            
            ! Writing to the logs, using if/else to convert bool to num for the logs
            IF StopCommand = TRUE THEN 
                temp := 1;
            ELSE
                temp := 0;
            ENDIF
            log("Action 3: Stop motion of the robot...StopCommad: " + NumToStr(temp,0));
            i:=i+1;
            
        ELSEIF floatData{i} = 12.0 THEN ! heartbeat message recieved..don't do anything really...
            log("heartbeat message recieved...do nothing..");
            i:=i+1;
            
        ELSEIF floatData{i} = 1.0 THEN ! MoveJointMode
            log("Action 1: Move towards a new joint..");
            IF dataLength-(i-1) < jointLength THEN ! Deny if we don't have a message of the correct length...
                log("Insufficient data to process joint message - Error!");
                RETURN;
            ELSE
                JGoal.robax.rax_1:=floatData{i+1}; ! update each joints
                JGoal.robax.rax_2:=floatData{i+2};
                JGoal.robax.rax_3:=floatData{i+3};
                JGoal.robax.rax_4:=floatData{i+4};
                JGoal.robax.rax_5:=floatData{i+5};
                JGoal.robax.rax_6:=floatData{i+6};
                XYZMode := FALSE;
                PosUpdateReady :=TRUE;
            ENDIF
            i:=i+7; ! update our i value...
        
        ELSEIF floatData{i} = 2.0 THEN ! MoveCartesian mode...
            log("Action 2: Change robots motion goal to cartesian mode");
            IF dataLength-(i-1) < cartLength THEN ! Deny if we don't have a message of the correct length...
                log("Insufficient data to process cartesian message - Error!");
                RETURN;
            ELSE
                XYZGoal.x := floatData{i+1}; ! update XYZ
                XYZGoal.y := floatData{i+2};
                XYZGoal.z := floatData{i+3};
                IF floatData{i+4} > 0.1 THEN
                    VertTool := TRUE; ! vertical tool or not, temp for logs
                    temp := 1;
                ELSE
                    VertTool := FALSE;
                    temp := 0;
                ENDIF
                PosUpdateReady:=TRUE; ! ready to move
                log("vertical = " + NumToStr(temp,0));
            ENDIF
            i:=i+5;
            
        ELSEIF floatData{i} = 4.0 THEN ! Resume motion command
            StopCommand := FALSE;
            log("Action 3: Resume motion of the robot");
            i:=i+1;    
        
        ELSEIF floatData{i} = 5.0 THEN ! We have an iostate update command
            log("Action 5 : IOUpdate command");
            IF dataLength-(i-1) < ioLength THEN ! Deny if we don't have a message of the correct length...
                log("Insufficient data to process IO message - Error!");
                RETURN;
            ELSE
                IOstate{1} := NumToStr(floatData{i+1},0); ! write the IOs to IOstate
                IOstate{2} := NumToStr(floatData{i+2},0);
                IOstate{3} := NumToStr(floatData{i+3},0);
                IOstate{4} := NumToStr(floatData{i+4},0);
                Ready:=FALSE;
                IOUpdateReady:=TRUE;
                log("Update our IOState to: [" + IOstate{1} +"," + IOstate{2} + "," + IOstate{3} + "," + IOstate{4}+"]");
            ENDIF
            i:=i+5;
            
        ELSEIF floatData{i} = 6.0 THEN ! We have a LinearMode update command
            log("Action 6: Change our linear mode");
            IF dataLength-(i-1) < LinearModeLength THEN ! Deny if we don't have a message of the correct length
                log("Insufficient data to process LinearMode message - Error!");
                RETURN;
            ELSE
                IF floatData{i+1} > 0.1 THEN ! if/else to convert from float to bool
                    LinearMode := TRUE;
                ELSE
                    LinearMode := FALSE;
                ENDIF
                
                IF LinearMode = TRUE THEN
                    temp := 1;
                ELSE
                    temp := 0; 
                ENDIF
                log("Update LinearMode to: " + NumToStr(temp,0));
            ENDIF
            i:=i+2;
            
        ELSEIF floatData{i} = 7.0 THEN ! We have an XYZMode update command
            log("Action 7: Set our XYZMode update");
            IF dataLength-(i-1) < XYZModeLength THEN ! Deny if we don't have a message of the correct length
                log("Insufficient data to process XYZMode message - Error!");
                RETURN;
            ELSE
                IF floatData{i+1} > 0.1 THEN ! if/else to convert from float to bool
                    XYZMode := TRUE;
                ELSE
                    XYZMode := FALSE;
                ENDIF
                IF XYZMode = TRUE THEN
                    temp :=1;
                ELSE
                    temp :=0;
                ENDIF
                log("Update XYZMode to: " + NumToStr(temp,0));
            ENDIF
            i:=i+2;
            
        ELSEIF floatData{i} = 8.0 THEN ! We have a speed update command
            log("Action 8: updaet our speed");
            IF dataLength-(i-1) < speedLength THEN ! Deny if we don't have a message of the correct length
                log("Insufficient data to process speed message - Error!");
                RETURN;
            ELSE
                speed.v_tcp := floatData{i+1}; ! various components for speed
                speed.v_ori := floatData{i+2};
                speed.v_leax := floatData{i+3};
                speed.v_reax := floatData{i+4};
                
                log("Update speed" + NumToStr(speed.v_tcp,0) + " " + NumToStr(speed.v_ori,0) + " " +NumToStr(speed.v_leax,0) + " " + NumToStr(speed.v_reax,0));
            ENDIF
            i:=i+5;
        
        ELSEIF floatData{i} = 9.0 THEN ! MoveCartesian mode...
            log("Action 2: Change robots motion goal to cartesian mode");
            IF dataLength-(i-1) < cartLength THEN ! Deny if we don't have a message of the correct length...
                log("Insufficient data to process cartesian message - Error!");
                RETURN;
            ELSE
                XYZGoal.x := floatData{i+1}; ! update XYZ
                XYZGoal.y := floatData{i+2};
                XYZGoal.z := floatData{i+3};
                Quaternions{1} := floatData{i+4};
                Quaternions{2} := floatData{i+5};
                Quaternions{3} := floatData{i+6};
                Quaternions{4} := floatData{i+7};
                
                PosUpdateReady:=TRUE; ! ready to move
                log("Quaternions set to " + NumToStr(Quaternions{1},4) + " " + NumToStr(Quaternions{2},4) + " "  + NumToStr(Quaternions{3},4) + " " + NumToStr(Quaternions{4},4) + " " );
            ENDIF
            i:=i+8;
            
            
            
        ELSE ! Shit...nothing makes sense here...
            log("Shit..command id makes no sense");
            !RETURN;
            i:=i+1;
        ENDIF
        
        ENDWHILE
        ! 2. Set the robot to not being ready
        ! I should ignore it's actual state as we are changing it as we go...
        Ready:=FALSE;
        
        ERROR
            RETURN;
        
    ENDPROC
ENDMODULE