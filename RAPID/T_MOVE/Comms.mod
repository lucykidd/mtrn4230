MODULE Comms
    ! Global Variables
    VAR socketdev client_socket;
    
    ! The host and port that we will be listening for a connection on.
    !CONST string host := "192.168.2.1";  ! Robot host ip...
    CONST string host := "127.0.0.1";
    VAR num port := 1026;
    VAR bool commsState := FALSE;
    
   PROC initComms()
        
        ! Create the socket to listen for a connection on.
        VAR socketdev welcome_socket;
        SocketCreate welcome_socket;
        
        ! Bind the socket to the host and port.
        SocketBind welcome_socket, host, port;
        
        ! Listen on the welcome socket.
        SocketListen welcome_socket;
        
        ! Accept a connection on the host and port.
        SocketAccept welcome_socket, client_socket \Time:=WAIT_MAX; ! set to wait indefineately...
        
        ! Close the welcome socket, as it is no longer needed.
        SocketClose welcome_socket;
        
        commsState:=TRUE; ! Say that the comms is active....
        
        ERROR
            
            IF ERRNO=ERR_SOCK_CLOSED THEN
                log("Error: trying to intialise a socket that is already closed..");
                RETRY;
            ELSE       
                RETRY; !just retry for any errors we encoutner...
                    log("Error:unknown error trying to connect");
            ENDIF
    ENDPROC
    
    ! Close the connection to the client.
    PROC CloseComms()
        SocketClose client_socket;
        
        ERROR
            log("Error when trying to close the socket..");
    ENDPROC
    
    PROC recoverComms()
        ! call this function when comms is lost and we want to try and reconnect
        
        ! 1. Make sure the current socket is closed...
        log("Try to recover the comms...");
        CloseComms; ! Hopefully this does not throw...
        commsState:=FALSE;
        
        ! 2. Try to reestablish comms....
        initComms;
        log("Comms re-established successfully!!!!");
        
    ENDPROC
    
    
ENDMODULE